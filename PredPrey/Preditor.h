/*
 * File:   Preditor.h
 * Author: Adam
 *
 * Created on 08 April 2016, 15:41
 */

#include <string>
#include <vector>
#include <utility>
#include <memory>
#include "PreditorAgent.h"
#ifndef PREDITOR_H
#define PREDITOR_H

class Preditor {
public:
    Preditor(std::string localNameIn, int width, int heigth, int visionRangeIn);
    Preditor(std::string localNameIn, int width, int heigth, int visionRangeIn, bool isRandom);
    virtual ~Preditor();
    void SetStamina(int stamina);
    int GetStamina() const;
    void SetMoveResult(int moveResult);
    int GetMoveResult() const;
    void SetSprintSpeed(int sprintSpeed);
    int GetSprintSpeed() const;
    void SetSpeed(int speed);
    int GetSpeed() const;
    void SetVisionRange(int visionRange);
    int GetVisionRange() const;
    void SetPosition(std::pair<int, int> position);
    std::pair<int, int> GetPosition() const;
    void SetLocalName(std::string name);
    std::string GetLocalName() const;
    void SetRlName(std::string rlName);
    std::string GetRlName() const;
    std::string serialisePreditor();
    std::string getRLAction();
    std::string getRandomAction();
    void updateRLLocal(std::string localState, PP_MOVE_RESULTS moveResult);
    void updateNonRL(std::string localState, PP_MOVE_RESULTS moveResult);
    static std::string PairToCoord(std::pair<int, int> input);
    void writeRLPolicies(std::string name, std::string tag);
    std::string printTrace();
    void printReward(std::string fileName, std::string tag);
    std::string printCatches();
    void setExplotation();
    void publishRLStateSpace(std::string path, std::string tag);
    void addPTLMapping(std::string sourceName, std::string targetName, std::shared_ptr<QTable> source, std::shared_ptr<QTable> target);
    void printPTLMappings(std::string filename, std::string tag);
    std::string getPTLTransfers();
    void addPTLTransfers(std::string ptlIn);
    void changeTemperature(int newOne);
    std::vector<std::string> getLocalPolicyNames();
    void readPolicies(std::string name);
    void setTestTime(int input);
private:
    std::shared_ptr<PreditorAgent> agent;
    int visionRange;
    int speed;
    int sprintSpeed;
    int stamina;
    int moveResult;
    std::pair<int, int> position;
    std::string localName;
    std::string rlName;
    bool moore;
    bool vanN;
    std::vector<std::pair<int, int>> trace;
    std::vector<int> catches;
    int testTime;

};

#endif /* PREDITOR_H */

