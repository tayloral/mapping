/*
 * File:   PreditorReward.cpp
 * Author: Adam
 *
 * Created on 31 May 2016, 15:27
 */

#include "PreditorVisionReward.h"
#include "Constants.h"
#include <string>
#include <iostream>
#include <cstdlib>

PreditorVisionReward::PreditorVisionReward() { }

PreditorVisionReward::~PreditorVisionReward() { }

void PreditorVisionReward::calcReward()
{
  std::cout << "calc reward 0 arg\n";
  reward = 1;
}

/**
 *not used anymore but need for an interface
 */
void PreditorVisionReward::calcReward(std::string in)
{
  std::cout << "calc reward 1 arg\n";
  reward = 2; //as exciting as the non-passed version
}

/**
 *this is the version called in the normal RL update cycle.
 * It sets the reward variable to be used.  In this case the maze calculates it for us, so it does nothing.
 */
void PreditorVisionReward::calcReward(std::string oldState, std::string worldState)
{
  /*worldState = worldState.substr(worldState.find("~") + 1, worldState.length() - worldState.find("~"));
  int u = -'0' + (worldState.at(worldState.find("U") + 1));
  int d = -'0' + (worldState.at(worldState.find("D") + 1));
  int l = -'0' + (worldState.at(worldState.find("L") + 1));
  int r = -'0' + (worldState.at(worldState.find("R") + 1));
  if ((u + d + l + r) == 1)
  {//prey in a moore neighbourhood
      reward = 100;
  }
  else if ((u + d + l + r) == 2)
  {//prey in a von nu but not a moore neighbourhood
      reward = 50;
  }
  else if ((u + d + l + r) > 0)
  {//must be multi preys
      reward = 50;
  }
  else
  {//no prey
      reward = 0;
  }*/
  //reward 1 -1 1000 -1
  //reward 2 -1 1000 -100
  //reward 3 0 1000 0
  if (moveResult == PP_MOVE_RESULTS::SUCCESS)
    {//not caught
      reward = -1; //simulate hunger
    }
  else if (moveResult == PP_MOVE_RESULTS::PRED_INTO_PREY || moveResult == PP_MOVE_RESULTS::PREY_INTO_PRED || moveResult == PP_MOVE_RESULTS::ANOTHER_CAUGHT)
    {//not caught
      reward = 1000; //simulate eating prey
      //std::cout << "PreditorReward::calcReward caught prey\n";
    }
  else if (moveResult == PP_MOVE_RESULTS::INTO_IMPASSABLE)
    {
      reward = -1; //simulate hunger and running into a wall pain
      //-2 for this rl wins
    }
  else
    {
      std::cout << "PreditorReward::calcReward unknown move outcome\n";
      std::exit(54242);
    }
  //std::cout << worldState << " and reward = " << reward << "\n";
}

void PreditorVisionReward::setMoveResult(PP_MOVE_RESULTS moveResultIn)
{
  moveResult = moveResultIn;
}