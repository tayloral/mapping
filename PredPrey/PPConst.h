/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   Constants.h
 * Author: Adam
 *
 * Created on 08 July 2016, 10:11
 */

#ifndef PPCONST_H
#define PPCONST_H

#define PP_START_NUMBER_OF_EXPERIMENT 0
#define PP_NUMBER_OF_RUNS 200
#define PP_EXPERIMENT_TEST_LENGTH 3000
#define PP_EXPERIMENT_LENGTH PP_EXPERIMENT_TEST_LENGTH
#define PP_EXPLORE_LENGTH PP_EXPERIMENT_TEST_LENGTH
#define PP_NUMBER_OF_PREDITORS 4

#define PP_WORLD_WIDTH 10
#define PP_WORLD_HIGHT 10
#define PP_NUMBER_OF_MOORE_ACTIONS 5
#define PP_PREDITOR_VISION_RANGE 2
#define PP_USING_CROSS_VISION false
#define PP_PREY_VISION_RANGE 1
#define PP_PREDITOR_STEPS_PER_TIMESTEP 1
#define PP_PREY_STEPS_TO_PREDITOR .3
#define PP_EVASION_CONSTANT_TIME 1
#define PP_USING_EVASION_COOLDOWN true
#define PP_WORLD_SPHERICAL false
#define PP_ALL_AGENTS_SHARE_REWARD false
#define PP_USE_OWN_COORD_POLICY !true
#define PP_USE_VECTOR_POLICY !true
#define PP_USE_VISION_POLICY true
#define PP_MULTI_FEATURE_VISION false
#define PP_BIN_SIZE_DEGREES 45
#define PP_DEGREES_PER_RADIAN 57.2958
#define PP_usingPTL !true//false//

enum PP_MOVE_RESULTS {
    SUCCESS,
    PRED_INTO_PREY,
    PREY_INTO_PRED,
    INTO_IMPASSABLE,
    ANOTHER_CAUGHT
};

static const std::string PP_TIMESTEP_TYPE_RL() {
    return "RL";
};

static const std::string PP_TIMESTEP_TYPE_RANDOM() {
    return "Random";
};
static const int TEST_TIMES[] = {
    1,
    10,
    20,
    30,
    40,
    50,
    60,
    70,
    80,
    90,
    100,
    200,
    300,
    400,
    500,
    1000,
    1500,
    2000,
    2500,
    3000,
    4000,
    5000,
    7500,
    10000

};

static const std::string PP_ACTIONS_STRINGS[9] = {
    "North",
    "East",
    "South",
    "West",
    "Pause",
    "North East",
    "South East",
    "North West",
    "South West"
};

enum PP_ACTIONS {
    N,
    E,
    S,
    W,
    P,
    NE,
    SE,
    SW,
    NW,
    END_OF_ENUM
};

enum PP_SQUARE_TYPE {
    EMPTY,
    PREDITOR_ONLY,
    PREY_ONLY,
    IMPASSABLE,
    WALL,
    last
};
#endif /* PPCONST_H */

