/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   PPMain.cpp
 * Author: Work-mine
 *
 * Created on 19 January 2017, 12:02
 */

#include <cstdlib>
#include <memory>
#include <iostream>
#include "World.h"
#include <time.h>
#include <sstream>

/**
 * a run with random actions untill the pole falls over
 * @return the numebr of timesteps it took
 */
int PPrunRandomlyUntilCatch(bool printToTerminal)
{
  //make some things
  int numberOfSteps = 0;
  std::shared_ptr<World> world(std::make_shared<World>());
  std::stringstream ss;
  std::shared_ptr<Prey> prey(std::make_shared<Prey>());
  prey->SetLocalName("A");
  prey->SetPosition(std::pair<int, int>(7, 7));
  ss.str("");
  ss << "a";
  std::shared_ptr<Preditor> pred(std::make_shared<Preditor>(ss.str(), PP_WORLD_WIDTH, PP_WORLD_HIGHT, PP_PREDITOR_VISION_RANGE, false));
  pred->SetPosition(std::pair<int, int>(3, 3));
  //add them
  world->addPreditor(pred);
  world->addPrey(prey);
  prey->SetUsingRandomMove(true);
  prey->SetVisionRange(0);
  if (printToTerminal)
    {
      std::cout << "made a World\n";
    }
  int worldStatus = 0; //
  while (worldStatus == 0)
    {//while not caught anything
      numberOfSteps++;
      if (printToTerminal)
        {
          world->printToConsole();
        }
      worldStatus = world->timeStep("Random", false);
    }
  if (printToTerminal)
    {
      world->printToConsole();
    }
  if (printToTerminal)
    {
    }
  std::cout << "Lasted for " << numberOfSteps << " steps\n";

  return numberOfSteps;
}

