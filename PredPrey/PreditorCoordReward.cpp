/*
 * File:   PreditorReward.cpp
 * Author: Adam
 *
 * Created on 31 May 2016, 15:27
 */

#include "PreditorCoordReward.h"
#include "Constants.h"
#include <string>
#include <iostream>
#include <cstdlib>

PreditorCoordReward::PreditorCoordReward() { }

PreditorCoordReward::~PreditorCoordReward() { }

void PreditorCoordReward::calcReward()
{
  std::cout << "calc reward 0 arg\n";
  reward = 1;
}

/**
 *not used anymore but need for an interface
 */
void PreditorCoordReward::calcReward(std::string in)
{
  std::cout << "calc reward 1 arg\n";
  reward = 2; //as exciting as the non-passed version
}

/**
 *reward for time in maze and catching prey
 */
void PreditorCoordReward::calcReward(std::string oldState, std::string worldState)
{
  if (moveResult == PP_MOVE_RESULTS::PRED_INTO_PREY || moveResult == PP_MOVE_RESULTS::PREY_INTO_PRED || moveResult == PP_MOVE_RESULTS::ANOTHER_CAUGHT)
    {//not caught
      reward = 100; //simulate eating prey
      //std::cout << "PreditorReward::calcReward caught prey\n";
    }
  else if (moveResult == PP_MOVE_RESULTS::INTO_IMPASSABLE || moveResult == PP_MOVE_RESULTS::SUCCESS)
    {
      reward = 0; //simulate hunger and running into a wall pain
    }
  else
    {
      std::cout << "PreditorReward::calcReward unknown move outcome\n";
      std::exit(54242);
    }





}

void PreditorCoordReward::setMoveResult(PP_MOVE_RESULTS moveResultIn)
{
  moveResult = moveResultIn;
}