/*
 * File:   Prey.cpp
 * Author: Adam
 *
 * Created on 08 April 2016, 15:41
 */

#include "Prey.h"
#include "Constants.h"
#include <sstream>
#include <cstdlib>
#include <iostream>
#include <algorithm>
#include <vector>
#include <numeric>

Prey::Prey()
{
  evasionCoolDown = 0;
  moore = false;
  vanN = true;
  this->usingLines = false;
  this->usingRandomMove = false;
  this->usingStopMove = true;
  stepsUntillMovement = PP_PREDITOR_STEPS_PER_TIMESTEP / PP_PREY_STEPS_TO_PREDITOR;
  usingRL = false;
  if (moore)
    {
      currentLineDirection = (PP_ACTIONS) (rand() % PP_NUMBER_OF_MOORE_ACTIONS);
    }
  else if (vanN)
    {
      currentLineDirection = (PP_ACTIONS) (rand() % PP_ACTIONS::END_OF_ENUM);
    }
}

Prey::Prey(const Prey& orig) { }

Prey::~Prey() { }

PP_ACTIONS Prey::randomMove()
{
  if (usingRandomMove)
    {
      if (moore)
        {
          return (PP_ACTIONS) (rand() % PP_NUMBER_OF_MOORE_ACTIONS);
        }
      else if (vanN)
        {
          return (PP_ACTIONS) (rand() % PP_ACTIONS::END_OF_ENUM);
        }
      else
        {
          std::cout << "Prey::randomMove not moore of vanN\n";
          std::exit(363);
          return PP_ACTIONS::END_OF_ENUM;
        }
    }
  else
    {
      std::cout << "Prey::randomMove not using random move\n";
      std::exit(363);
      return PP_ACTIONS::END_OF_ENUM;

    }
}

PP_ACTIONS Prey::moveInLines()
{
  if (usingLines)
    {
      //std::cout << "Prey::moveInLines " << currentLineDirection << "\n";
      return this->currentLineDirection;
    }
  else
    {
      std::cout << "Prey::randomMove not moving in lines\n";
      std::exit(363);
      return PP_ACTIONS::END_OF_ENUM;
    }

}

void Prey::printReward(std::string fileName, std::string tag)
{
  if (usingRL == false)
    {
      //std::cout << "Prey::printReward No rl, so cant print reward\n";
    }
  else
    {
      std::cout << "Prey::printReward need to impliment this, so cant print reward\n";
      //agent->printReward(fileName, tag);
    }

}

void Prey::SetStamina(int stamina)
{
  this->stamina = stamina;
}

int Prey::GetStamina() const
{
  return stamina;
}

void Prey::SetSprintSpeed(int sprintSpeed)
{
  this->sprintSpeed = sprintSpeed;
}

int Prey::GetSprintSpeed() const
{
  return sprintSpeed;
}

void Prey::SetSpeed(int speed)
{
  this->speed = speed;
}

int Prey::GetSpeed() const
{
  return speed;
}

void Prey::SetVisionRange(int visionRange)
{
  this->visionRange = visionRange;
}

int Prey::GetVisionRange() const
{
  return visionRange;
}

void Prey::SetPosition(std::pair<int, int> position)
{
  this->position = position;
  trace.push_back(position);
}

std::pair<int, int> Prey::GetPosition() const
{
  return position;
}

void Prey::SetLocalName(std::string name)
{
  this->localName = name;
}

std::string Prey::GetLocalName() const
{
  return localName;
}

void Prey::SetRlName(std::string rlName)
{
  this->rlName = rlName;
}

std::string Prey::GetRlName() const
{
  return rlName;
}

std::string Prey::serialisePrey()
{
  std::stringstream ss;
  ss << "<Prey><posX><" << position.first << "><posY><" << position.second << "><spee><" << speed << "><sSpe><" << sprintSpeed << "><stam><" << stamina << "><visi><" << visionRange << "><locN><" << localName << "><rlNa><" << rlName << ">\n";
  return ss.str();
}

PP_ACTIONS Prey::getAction()
{
  stepsUntillMovement--;
  if (stepsUntillMovement <= 0)
    {//move
      stepsUntillMovement = PP_PREDITOR_STEPS_PER_TIMESTEP / PP_PREY_STEPS_TO_PREDITOR; //reset
      if (usingRandomMove)
        {
          return this->randomMove();
        }
      else if (usingLines)
        {
          return this->moveInLines();
        }
      else if (usingStopMove)
        {
          return PP_ACTIONS::P;
        }
      else if (usingEvasionMove)
        {
          return currentLineDirection; //will have been calculated when we were notified of results
        }
      else if (usingRL)
        {
          std::cout << "Prey::getAction no RL yet\n";
          return PP_ACTIONS::P;
        }
      else
        {
          std::cout << "Prey::getAction no action bool set\n";
          std::exit(424);
          return PP_ACTIONS::END_OF_ENUM;
        }
    }
  else
    {//don't move
      return PP_ACTIONS::P;
    }
}

/**
 * get a moore move that is safe (if possible)
 * @return
 */
PP_ACTIONS Prey::getEvasionMoveAction(bool hitWall)
{
  evasionCoolDown++;
  if (PP_USING_EVASION_COOLDOWN && evasionCoolDown < PP_EVASION_CONSTANT_TIME)
    {

    }
  else
    {
      PP_ACTIONS oldDirection = this->currentLineDirection;
      std::vector<int> possibles;
      for (int a = 0; a < PP_NUMBER_OF_MOORE_ACTIONS - 1; a++)
        {//-1 as never take stay as a possibility unless nothing else
          if (a<this->preditorsSeen.size() && this->preditorsSeen[a] == false)
            {//if safe to go this way
              //std::cout << "     addeded " << a << "\n";
              possibles.push_back(a);
            }
          else if (a >= this->preditorsSeen.size())
            {//if is stay
              possibles.push_back(a);
              //std::cout << "     addeded " << a << "\n";
            }
          else
            {
              //std::cout << "Prey::getEvasionMoveAction a dangourous direction:" << a << "\n";
            }
        }
      if (possibles.size() == 0)
        {//nothing to do but stay
          currentLineDirection = PP_ACTIONS::P;
        }
      else if (possibles.size() == 1)
        {
          currentLineDirection = (PP_ACTIONS) possibles[0];
        }
      else if (possibles.size() == 2)
        {
          //int diff=std::abs(possibles[0]-possibles[1]);
          //if(diff==2)
          //{//two oposite directions to choose from
          currentLineDirection = (PP_ACTIONS) possibles[rand() % possibles.size()];
          // }
        }
      else if (possibles.size() == 3)
        {
          int sumPossibles = std::accumulate(possibles.begin(), possibles.end(), 0);
          if (sumPossibles == 3)
            {
              currentLineDirection = (PP_ACTIONS) 1;
            }
          else if (sumPossibles == 4)
            {
              currentLineDirection = (PP_ACTIONS) 0;
            }
          else if (sumPossibles == 5)
            {
              currentLineDirection = (PP_ACTIONS) 3;
            }
          else if (sumPossibles == 6)
            {
              currentLineDirection = (PP_ACTIONS) 2;
            }
          else
            {
              std::cout << "Prey::getEvasionMoveAction 1 default action\n";
              currentLineDirection = (PP_ACTIONS) possibles[rand() % possibles.size()];
            }

        }
      else if (possibles.size() == 4)
        {//do nothing if no need to change

        }
      else
        {
          std::cout << "poss.size= " << possibles.size() << "\n";
          std::cout << "Prey::getEvasionMoveAction default action\n";
          return (PP_ACTIONS) possibles[rand() % possibles.size()];
        }
      if (hitWall)
        {//if cant go the same way we were check we are not
          while (currentLineDirection == oldDirection)
            {//if still same way
              if (possibles.size() > 1)
                {//if we have an option
                  currentLineDirection = (PP_ACTIONS) possibles[rand() % possibles.size()];
                }
              else
                {//if we dont
                  std::cout << "trapped \n";
                  currentLineDirection = PP_ACTIONS::P; //stay
                  break; //leave loop
                }
            }
        }
      evasionCoolDown = 0;
    }
  return currentLineDirection;
}

void Prey::SetPreditorsSeen(std::vector<bool> preditorsSeen)
{
  this->preditorsSeen = preditorsSeen;
}

/***should only be used to set true
 */
void Prey::SetUsingLines(bool usingLines)
{
  this->usingRandomMove = false;
  this->usingLines = usingLines;
  this->usingStopMove = false;
  this->usingRL = false;
  usingEvasionMove = false;
}

bool Prey::IsUsingLines() const
{
  return usingLines;
}

/***should only be used to set true
 */
void Prey::SetUsingEvasionMove(bool usingEvasionMoveIn)
{
  this->usingLines = false;
  this->usingStopMove = false;
  this->usingRandomMove = false;
  this->usingRL = false;
  usingEvasionMove = usingEvasionMoveIn;
}

bool Prey::IsUsingEvasionMove() const
{
  return usingEvasionMove;
}

void Prey::SetUsingRandomMove(bool usingRandomMove)
{
  this->usingLines = false;
  this->usingStopMove = false;
  this->usingRandomMove = usingRandomMove;
  this->usingRL = false;
  usingEvasionMove = false;
}

bool Prey::IsUsingRandomMove() const
{
  return usingRandomMove;
}

void Prey::SetUsingStopMove(bool usingStopMove)
{
  this->usingStopMove = usingStopMove;
  this->usingRL = false;
  usingRandomMove = false;
  usingLines = false;
  usingEvasionMove = false;
}

bool Prey::IsUsingStopMove() const
{
  return usingStopMove;
}

void Prey::notifyOfResults(PP_MOVE_RESULTS result, std::pair<int, int> location)
{
  if (this->usingRandomMove || this->usingStopMove)
    {
      //nothing to do
    }
  else if (this->usingRL)
    {
      std::cout << "Prey::notifyOfResults No RL yet\n";
    }
  else if (this->usingLines)
    {
      if (result == PP_MOVE_RESULTS::INTO_IMPASSABLE)
        {//if hit a wall turn round
          if (moore)
            {
              int nextDirection = ((int) this->currentLineDirection);
              this->currentLineDirection = (PP_ACTIONS) ((nextDirection + 1) % PP_NUMBER_OF_MOORE_ACTIONS);
            }
          else if (vanN)
            {
              int nextDirection = ((int) this->currentLineDirection);
              this->currentLineDirection = (PP_ACTIONS) ((nextDirection + 1) % PP_ACTIONS::END_OF_ENUM);
            }
          else
            {
              std::cout << "Prey::randomMove not moore of vanN\n";
              std::exit(363);
            }
        }
    }
  else if (this->usingEvasionMove)
    {

      if (result == PP_MOVE_RESULTS::INTO_IMPASSABLE)
        {//if hit a wall indicate we need to turn round
          this->currentLineDirection = this->getEvasionMoveAction(true);
        }
      else
        {//otherwise keep going unless we cant
          this->currentLineDirection = this->getEvasionMoveAction(false);
        }

    }
  else
    {
      std::cout << "Prey::notifyOfResults notify of type i don't know\n";
      std::exit(42433);
    }
  this->SetPosition(location); //set location in all action choise types
}

std::string Prey::printTrace()
{
  std::stringstream ss;
  std::vector < std::pair<int, int>>::iterator traceIt = trace.begin();
  while (traceIt != trace.end())
    {
      ss << traceIt->first << "," << traceIt->second << ",";
      traceIt++;
    }
  return ss.str();
}

void Prey::setExplotation() {
  //std::cout << "Prey::setExplotation No RL yet so can't exploit\n";
}