/*
 * File:   World.cpp
 * Author: Adam
 *
 * Created on 08 April 2016, 11:23
 */

#include "World.h"
#include <iostream>
#include "Constants.h"
#include <string>
#include <sstream>
#include <math.h>
#include<iostream>
//#include<arpa/inet.h>
#include<unistd.h>
//#include<sys/socket.h>
//#include<sys/types.h>
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include <memory>

World::World()
{
  autoPauseToSend = false;
  if (__cplusplus < 201103L)
    {//warn if things won't work
      std::cout << "Warning---C++11 is required for STD::ARRAY, some features of vectors of stl containers and colors\n";
    }
  for (int b = 0; b < PP_WORLD_HIGHT; b++)
    {//for the board hight

      for (int a = 0; a < PP_WORLD_WIDTH; a++)
        {//for the board width
          if (PP_WORLD_SPHERICAL == false && ((a == 0) || (a == PP_WORLD_HIGHT - 1)) || (b == 0) || (b == PP_WORLD_WIDTH - 1))
            {//make walls then
              world[a][b] = PP_SQUARE_TYPE::WALL;
            }
          else
            {

              world[a][b] = randomSquareType(false, false);
              //std::cout << "Square (" << a << " ," << b << ") is " << World::squareTypeToString(world[a][b]) << "\n";
            }
        }

    }
}

void World::addPreditor(std::shared_ptr<Preditor> input)
{
  preditors.push_back(input);
}

void World::printTracesToFile(std::string filename, std::string tag)
{
  std::vector<std::shared_ptr < Prey>>::iterator preyIterator = preys.begin();
  for (; preyIterator < preys.end(); preyIterator++)
    {//for all preys
      std::shared_ptr<Prey> toTest = *preyIterator;
      std::ofstream preyFile;
      std::stringstream ss;
      ss << filename << "-prey-" << toTest->GetLocalName() << "-" << tag << ".csv.stats";
      preyFile.open(ss.str());
      preyFile << toTest->printTrace();
      preyFile.close();
      toTest->printReward(filename, tag);
    }
  std::vector<std::shared_ptr < Preditor>>::iterator preditorIterator = preditors.begin();
  for (; preditorIterator < preditors.end(); preditorIterator++)
    {//for all preds
      std::shared_ptr<Preditor> toTest = *preditorIterator;
      std::ofstream predFile;
      std::stringstream ss;
      ss << filename << "-preditorTrace-" << toTest->GetLocalName() << "-" << tag << ".csv.stats";
      predFile.open(ss.str());
      predFile << toTest->printTrace();
      predFile.close();
      ss.str("");
      ss << filename << "-preditorCatches-" << toTest->GetLocalName() << "-" << tag << ".csv.stats";
      predFile.open(ss.str());
      predFile << toTest->printCatches();
      predFile.close();

      ss.str("");
      ss << filename << "-" << toTest->GetLocalName();
      toTest->printReward(ss.str(), tag);
    }
}

void World::addPrey(std::shared_ptr<Prey> input)
{
  preys.push_back(input);
}

void World::setExploitaion()
{
  std::vector<std::shared_ptr < Prey>>::iterator preyIterator = preys.begin();
  for (; preyIterator < preys.end(); preyIterator++)
    {//for all preys
      std::shared_ptr<Prey> toTest = *preyIterator;
      toTest->setExplotation();
    }
  std::vector<std::shared_ptr < Preditor>>::iterator preditorIterator = preditors.begin();
  for (; preditorIterator < preditors.end(); preditorIterator++)
    {//for all preds
      std::shared_ptr<Preditor> toTest = *preditorIterator;
      toTest->setExplotation();
    }

}

bool World::isPreditorOccupied(std::pair<int, int> input)
{
  return isPreditorOccupied(input.first, input.second);
}

bool World::isPreditorOccupied(int x, int y)
{
  bool output = false;
  std::vector<std::shared_ptr < Preditor>>::iterator preditorIterator = preditors.begin();
  for (; preditorIterator < preditors.end(); preditorIterator++)
    {//for all preds
      std::shared_ptr<Preditor> toTest = *preditorIterator;
      if (toTest->GetPosition().first == x && toTest->GetPosition().second == y)
        {
          output = true;
          return output;
        }
    }
  return output;
}

void World::printToFile(std::string message, bool close, std::string filename)
{
  if (filename.compare("") != 0)
    {//if we have a file name open the outstream
      std::cout << "opening file\n";
      fileOut.open(filename);
    }
  else
    {
      fileOut << "<timeStep>" << message;
      if (autoPauseToSend == true)
        {//if we are writing to file send a pause as it happens
          std::stringstream ss;
          ss << "<pause><posX><" << autoPauseCoord.first << "><posY><" << autoPauseCoord.second << ">\n";
          fileOut << ss.str();
          autoPauseToSend = false;
        }
    }
  if (close)
    {
      //std::cout << "closing file\n";
      fileOut.close();
    }
}

void World::printToUDP(int port, std::string message)
{
  std::cout << "Warning---UDP disabled\n";
  /* int sockfd;
   sockfd = socket(AF_INET, SOCK_DGRAM, 0);
   struct sockaddr_in serv, client;

   serv.sin_family = AF_INET;
   serv.sin_port = htons(port);
   serv.sin_addr.s_addr = inet_addr("127.0.0.1");

   char buffer[message.length()];
   socklen_t l = sizeof (client);
   socklen_t m = sizeof (serv);
   //socklen_t m = client;
   strncpy(buffer, message.c_str(), sizeof (buffer));
   sendto(sockfd, buffer, sizeof (buffer), 0, (struct sockaddr *) &serv, m);*/
}

bool World::isPreyOccupied(int x, int y)
{

  bool output = false;
  std::vector<std::shared_ptr < Prey>>::iterator preyIterator = preys.begin();
  for (; preyIterator < preys.end(); preyIterator++)
    {//for all preys
      std::shared_ptr<Prey> toTest = *preyIterator;
      if (toTest->GetPosition().first == x && toTest->GetPosition().second == y)
        {
          //std::cout << "is preyocc=true " << x << " " << y << "\n";
          output = true;
          return output;
        }
    }

  return output;
}

bool World::isOccupied(int x, int y)
{

  return isPreyOccupied(x, y) || isPreditorOccupied(x, y);
}

PP_SQUARE_TYPE World::randomSquareType(bool allowWall, bool allowObsticles)
{
  if (allowObsticles == true)
    {
      if (allowWall == true)
        {
          return static_cast<PP_SQUARE_TYPE> (rand() % PP_SQUARE_TYPE::last);
        }
      else
        {
          return static_cast<PP_SQUARE_TYPE> (rand() % (PP_SQUARE_TYPE::last - 1));
        }
    }
  else
    {
      return PP_SQUARE_TYPE::EMPTY;
    }
}

std::string World::squareTypeToString(PP_SQUARE_TYPE input)
{
  std::string output = "";
  switch (input)
    {
      case PP_SQUARE_TYPE::EMPTY:
        output = "EMPTY";
        break;
      case PP_SQUARE_TYPE::IMPASSABLE:
        output = "IMPASS";
        break;
      case PP_SQUARE_TYPE::PREDITOR_ONLY:
        output = "PREDIT";
        break;
      case PP_SQUARE_TYPE::PREY_ONLY:
        output = "PREY";
        break;
      case PP_SQUARE_TYPE::WALL:
        output = "WALL";
        break;
      default:
        output = "UNKNOWN";
        break;
    }
  return output;
}

std::string World::getOccupierName(int x, int y)
{
  std::string output = "?whoops";
  if (isPreyOccupied(x, y))
    {
      std::vector<std::shared_ptr < Prey>>::iterator preyIterator = preys.begin();
      for (; preyIterator < preys.end(); preyIterator++)
        {//for all preys
          std::shared_ptr<Prey> toTest = *preyIterator;
          if (toTest->GetPosition().first == x && toTest->GetPosition().second == y)
            {
              output = toTest->GetLocalName();
              return output;
            }
        }
    }
  else
    {
      std::vector<std::shared_ptr < Preditor>>::iterator preditorIterator = preditors.begin();
      for (; preditorIterator < preditors.end(); preditorIterator++)
        {//for all preds
          std::shared_ptr<Preditor> toTest = *preditorIterator;
          if (toTest->GetPosition().first == x && toTest->GetPosition().second == y)
            {
              output = toTest->GetLocalName();
              return output;
            }
        }
    }
  return output;
}

void World::printToConsole()
{/*more on consol codes and how they display here https://en.wikipedia.org/wiki/ANSI_escape_code#graphics*/
  for (int b = PP_WORLD_HIGHT - 1; b >= 0; b--)
    {//for the board hight
      //for (int b = WORLD_WIDTH - 1; b >= 0; b--)
      for (int a = 0; a < PP_WORLD_WIDTH; a++)
        {//for the board width
          if (isPreditorOccupied(a, b))
            {
              std::string name = getOccupierName(a, b);
              std::cout << "\033[30;45m" << name.at(0) << "\033[0m";
            }
          else if (isPreyOccupied(a, b))
            {
              std::string name = getOccupierName(a, b);
              std::cout << "\033[30;42m" << name.at(0) << "\033[0m";
            }
          else
            {
              switch (world[a][b])
                {
                  case PP_SQUARE_TYPE::EMPTY:
                    std::cout << "\033[36;46mE\033[0m";
                    break;
                  case PP_SQUARE_TYPE::IMPASSABLE:
                    std::cout << "\033[30;40mI\033[0m";
                    break;
                  case PP_SQUARE_TYPE::PREDITOR_ONLY:
                    std::cout << "\033[31;41mP\033[0m";
                    break;
                  case PP_SQUARE_TYPE::PREY_ONLY:
                    std::cout << "\033[33;43mF\033[0m";
                    break;
                  case PP_SQUARE_TYPE::WALL:
                    if (PP_WORLD_SPHERICAL == false && (((a == 0) && (b == 0)) || ((a == 0) && (b == PP_WORLD_WIDTH - 1)) || ((a == PP_WORLD_HIGHT - 1) && (b == PP_WORLD_WIDTH - 1)) || ((a == PP_WORLD_HIGHT - 1) && (b == 0))))
                      {//if corner wall
                        std::cout << "\033[37;40m+\033[0m";
                      }
                    else if (PP_WORLD_SPHERICAL == false && ((a == 0) || (a == PP_WORLD_HIGHT - 1)))
                      {//vert wall
                        std::cout << "\033[37;40m-\033[0m";
                      }
                    else if (PP_WORLD_SPHERICAL == false && ((b == 0) || (b == PP_WORLD_WIDTH - 1)))
                      {//horiz wall
                        std::cout << "\033[37;40m|\033[0m";
                      }
                    else
                      {
                        std::cout << "\033[37;40m?\033[0m";
                      }

                    break;
                  default:
                    std::cout << "\033[37;45m?\033[0m";

                    break;
                }
            }
        }
      std::cout << "\n";
    }

}

/***
 differs from print to consol as avoids prinint prey name*/
std::string World::printToString()
{
  std::stringstream ss;
  ss << "<worX><" << PP_WORLD_WIDTH << "><worY><" << PP_WORLD_HIGHT << ">\n<Worl><";
  for (int b = PP_WORLD_HIGHT - 1; b >= 0; b--)
    {//for the board hight
      //for (int b = WORLD_WIDTH - 1; b >= 0; b--)
      for (int a = 0; a < PP_WORLD_WIDTH; a++)
        {//for the board width
          if (false && isPreditorOccupied(a, b))
            {
              std::string name = getOccupierName(a, b);
              ss << name.at(0);
            }
          else if (false && isPreyOccupied(a, b))
            {
              std::string name = getOccupierName(a, b);
              ss << name.at(0);
            }
          else
            {
              switch (world[a][b])
                {
                  case PP_SQUARE_TYPE::EMPTY:
                    ss << "E";
                    break;
                  case PP_SQUARE_TYPE::IMPASSABLE:
                    ss << "I";
                    break;
                  case PP_SQUARE_TYPE::PREDITOR_ONLY:
                    ss << "P";
                    break;
                  case PP_SQUARE_TYPE::PREY_ONLY:
                    ss << "F";
                    break;
                  case PP_SQUARE_TYPE::WALL:
                    if (PP_WORLD_SPHERICAL == false && (((a == 0) && (b == 0)) || ((a == 0) && (b == PP_WORLD_WIDTH - 1)) || ((a == PP_WORLD_HIGHT - 1) && (b == PP_WORLD_WIDTH - 1)) || ((a == PP_WORLD_HIGHT - 1) && (b == 0))))
                      {//if corner wall
                        ss << "+";
                      }
                    else if (PP_WORLD_SPHERICAL == false && ((a == 0) || (a == PP_WORLD_HIGHT - 1)))
                      {//vert wall
                        ss << "-";
                      }
                    else if (PP_WORLD_SPHERICAL == false && ((b == 0) || (b == PP_WORLD_WIDTH - 1)))
                      {//horiz wall
                        ss << "|";
                      }
                    else
                      {
                        ss << "?";
                      }

                    break;
                  default:
                    ss << "?";

                    break;
                }
            }
        }
      ss << "\n";
    }
  ss << ">";
  return ss.str();
}

World::World(const World& orig) { }

World::~World() { }

std::string World::serialiseWorld()
{
  std::stringstream ss;
  ss << printToString();
  ss << "<noPrey><" << preys.size() << ">\n";
  std::vector<std::shared_ptr < Prey>>::iterator preyIterator = preys.begin();
  while (preyIterator != preys.end())
    {
      ss << (*preyIterator)->serialisePrey();
      preyIterator++;
    }
  ss << "<noPred><" << preditors.size() << ">\n";
  std::vector<std::shared_ptr < Preditor>>::iterator preditorsIterator = preditors.begin();
  while (preditorsIterator != preditors.end())
    {
      ss << (*preditorsIterator)->serialisePreditor();
      preditorsIterator++;
    }

  return ss.str();
}

int World::timeStep(std::string timeStepType, bool sequential = false)
{
  if (timeStepType.compare(PP_TIMESTEP_TYPE_RL()) == 0)
    {//is rl
      if (sequential)
        {//go through each and compleat a timestep
          sequentialRLTimeStep();
        }
      else
        {
          nonsequentialRLTimeStep();
        }
    }
  else
    if (timeStepType.compare(PP_TIMESTEP_TYPE_RANDOM()) == 0)
    {//is random
      return nonsequentialRandomTimeStep();
    }
  else
    {
      std::cout << "World::timeStep unknown timesteptype\n";
    }
  return 111222; //cos they return ints for some reason that must have seemed good at the time
  //now calculate the return value
}

int World::sequentialRLTimeStep()
{
  std::cout << "nothing yet\n";
  return 0;
}

int World::nonsequentialRLTimeStep()
{//move prey then preds, then rescan prey and respawn
  std::vector<std::shared_ptr < Prey>>::iterator preyIterator = preys.begin();
  while (preyIterator != preys.end())
    {
      //if the prey can run, see where to run to
      (*preyIterator)->SetPreditorsSeen(this->makePreyVision((*preyIterator)->GetPosition(), (*preyIterator)->GetVisionRange()));
      PP_MOVE_RESULTS moveResult;
      PP_ACTIONS move = (*preyIterator)->getAction();
      //std::cout << "Prey used " << move << "\n";
      std::pair<int, int> newPlace = applyMove((*preyIterator)->GetLocalName(), move, moveResult);
      (*preyIterator)->notifyOfResults(moveResult, newPlace);
      preyIterator++;
    }
  std::vector<std::shared_ptr < Preditor>>::iterator preditorsIterator = preditors.begin();
  bool anyPreditorCaughtPrey = false;
  std::string catcher = "";
  while (preditorsIterator != preditors.end())
    {//get and apply moves//decoupled from update so we can know if anyone caught
      std::string action = (*preditorsIterator)->getRLAction();
      PP_MOVE_RESULTS moveResult;
      std::pair<int, int> newPlace = applyMove((*preditorsIterator)->GetLocalName(), World::ActionStringToEnum(action), moveResult);
      (*preditorsIterator)->SetPosition(newPlace);
      if (PP_ALL_AGENTS_SHARE_REWARD)
        {//if i need to wait to see if anyone cauget do this later
          // (*preditorsIterator)->updateRLLocal(makeStateForRLPreditor((*preditorsIterator)->GetPosition(), (*preditorsIterator)->GetVisionRange()), moveResult);
        }
      else
        {//update what happened based on own reward
          (*preditorsIterator)->updateRLLocal(makeStateForRLPreditor((*preditorsIterator)->GetPosition(), (*preditorsIterator)->GetVisionRange()), moveResult);
        }
      if (moveResult == PP_MOVE_RESULTS::PRED_INTO_PREY || moveResult == PP_MOVE_RESULTS::PREY_INTO_PRED)
        {//if caught set flag
          anyPreditorCaughtPrey = true;
          catcher = (*preditorsIterator)->GetLocalName();
          if (autoPauseToSend == false)
            {
              //std::cout << "marking a catch\n";
              autoPauseToSend = true;
              autoPauseCoord = newPlace;
            }
        }

      preditorsIterator++;
    }
  //std::cout << "postPred 1 \n";
  preditorsIterator = preditors.begin();
  while (preditorsIterator != preditors.end())
    {//now update as we know if we caught the prey
      if (PP_ALL_AGENTS_SHARE_REWARD)
        {//update with if we won colaberativlyn
          if (anyPreditorCaughtPrey)
            {
              //std::cout << "a\n";
              if ((*preditorsIterator)->GetLocalName() == catcher)
                {//if i caught reall win
                  // std::cout << "b\n";
                  //std::cout << "updating caught prey " << (*preditorsIterator)->GetLocalName() << "\n";
                  (*preditorsIterator)->updateRLLocal(makeStateForRLPreditor((*preditorsIterator)->GetPosition(), (*preditorsIterator)->GetVisionRange()), PP_MOVE_RESULTS::PRED_INTO_PREY);
                }
              else
                {//someone else caught give me reward though
                  // std::cout << "c\n";
                  (*preditorsIterator)->updateRLLocal(makeStateForRLPreditor((*preditorsIterator)->GetPosition(), (*preditorsIterator)->GetVisionRange()), PP_MOVE_RESULTS::ANOTHER_CAUGHT);
                }
            }
          else
            {//no one caught so failed
              //std::cout << "d\n";
              (*preditorsIterator)->updateRLLocal(makeStateForRLPreditor((*preditorsIterator)->GetPosition(), (*preditorsIterator)->GetVisionRange()), PP_MOVE_RESULTS::INTO_IMPASSABLE);
            }
        }
      preditorsIterator++;
    }
  //std::cout << "postPred 2\n";
  preyIterator = preys.begin();
  while (preyIterator != preys.end())
    {
      if (isPreditorOccupied((*preyIterator)->GetPosition()))
        {//if there is a pred in the quare where i am
          bool repawened = false;
          std::pair<int, int> newPlace;
          newPlace.first = rand() % (PP_WORLD_WIDTH);
          newPlace.second = rand() % (PP_WORLD_HIGHT);
          while (isPreditorOccupied(newPlace) == true || world[newPlace.first][newPlace.second] == PP_SQUARE_TYPE::IMPASSABLE || world[newPlace.first][newPlace.second] == PP_SQUARE_TYPE::PREDITOR_ONLY || world[newPlace.first][newPlace.second] == PP_SQUARE_TYPE::WALL)
            {//keep trying locations until a safe draw
              newPlace.first = rand() % (PP_WORLD_WIDTH);
              newPlace.second = rand() % (PP_WORLD_HIGHT);
            }
          (*preyIterator)->SetPosition(newPlace);
        }
      preyIterator++;
    }
}

/**
 * a test time step
 * @return 0 is nothing caught otherwise 1
 */
int World::nonsequentialRandomTimeStep()
{//move prey then preds, then rescan prey and respawn
  int outStatus = 0;
  std::vector<std::shared_ptr < Prey>>::iterator preyIterator = preys.begin();
  while (preyIterator != preys.end())
    {
      //if the prey can run, see where to run to
      (*preyIterator)->SetPreditorsSeen(this->makePreyVision((*preyIterator)->GetPosition(), (*preyIterator)->GetVisionRange()));
      PP_MOVE_RESULTS moveResult;
      PP_ACTIONS move = (*preyIterator)->getAction();
      //std::cout << "Prey used " << move << "\n";
      std::pair<int, int> newPlace = applyMove((*preyIterator)->GetLocalName(), move, moveResult);
      (*preyIterator)->notifyOfResults(moveResult, newPlace);
      preyIterator++;
    }
  std::vector<std::shared_ptr < Preditor>>::iterator preditorsIterator = preditors.begin();
  bool anyPreditorCaughtPrey = false;
  std::string catcher = "";
  while (preditorsIterator != preditors.end())
    {//get and apply moves//decoupled from update so we can know if anyone caught
      std::string action = (*preditorsIterator)->getRandomAction();
      PP_MOVE_RESULTS moveResult;
      std::pair<int, int> newPlace = applyMove((*preditorsIterator)->GetLocalName(), World::ActionStringToEnum(action), moveResult);
      (*preditorsIterator)->SetPosition(newPlace);
      if (PP_ALL_AGENTS_SHARE_REWARD)
        {//if i need to wait to see if anyone cauget do this later
          // (*preditorsIterator)->updateRLLocal(makeStateForRLPreditor((*preditorsIterator)->GetPosition(), (*preditorsIterator)->GetVisionRange()), moveResult);
        }
      else
        {//update what happened based on own reward
          (*preditorsIterator)->updateNonRL(makeStateForRLPreditor((*preditorsIterator)->GetPosition(), (*preditorsIterator)->GetVisionRange()), moveResult);
        }
      if (moveResult == PP_MOVE_RESULTS::PRED_INTO_PREY || moveResult == PP_MOVE_RESULTS::PREY_INTO_PRED)
        {//if caught set flag
          anyPreditorCaughtPrey = true;
          catcher = (*preditorsIterator)->GetLocalName();
          if (autoPauseToSend == false)
            {
              outStatus = 1;
              //std::cout << "marking a catch\n";
              autoPauseToSend = true;
              autoPauseCoord = newPlace;
            }
        }

      preditorsIterator++;
    }
  //std::cout << "postPred 1 \n";
  preditorsIterator = preditors.begin();
  while (preditorsIterator != preditors.end())
    {//now update as we know if we caught the prey
      if (PP_ALL_AGENTS_SHARE_REWARD)
        {//update with if we won colaberativlyn
          if (anyPreditorCaughtPrey)
            {
              //std::cout << "a\n";
              if ((*preditorsIterator)->GetLocalName() == catcher)
                {//if i caught reall win
                  // std::cout << "b\n";
                  //std::cout << "updating caught prey " << (*preditorsIterator)->GetLocalName() << "\n";
                  (*preditorsIterator)->updateNonRL(makeStateForRLPreditor((*preditorsIterator)->GetPosition(), (*preditorsIterator)->GetVisionRange()), PP_MOVE_RESULTS::PRED_INTO_PREY);
                }
              else
                {//someone else caught give me reward though
                  // std::cout << "c\n";
                  (*preditorsIterator)->updateNonRL(makeStateForRLPreditor((*preditorsIterator)->GetPosition(), (*preditorsIterator)->GetVisionRange()), PP_MOVE_RESULTS::ANOTHER_CAUGHT);
                }
            }
          else
            {//no one caught so failed
              //std::cout << "d\n";
              (*preditorsIterator)->updateNonRL(makeStateForRLPreditor((*preditorsIterator)->GetPosition(), (*preditorsIterator)->GetVisionRange()), PP_MOVE_RESULTS::INTO_IMPASSABLE);
            }
        }
      preditorsIterator++;
    }
  //std::cout << "postPred 2\n";
  preyIterator = preys.begin();
  while (preyIterator != preys.end())
    {
      if (isPreditorOccupied((*preyIterator)->GetPosition()))
        {//if there is a pred in the quare where i am
          bool repawened = false;
          std::pair<int, int> newPlace;
          newPlace.first = rand() % (PP_WORLD_WIDTH);
          newPlace.second = rand() % (PP_WORLD_HIGHT);
          while (isPreditorOccupied(newPlace) == true || world[newPlace.first][newPlace.second] == PP_SQUARE_TYPE::IMPASSABLE || world[newPlace.first][newPlace.second] == PP_SQUARE_TYPE::PREDITOR_ONLY || world[newPlace.first][newPlace.second] == PP_SQUARE_TYPE::WALL)
            {//keep trying locations until a safe draw
              newPlace.first = rand() % (PP_WORLD_WIDTH);
              newPlace.second = rand() % (PP_WORLD_HIGHT);
            }
          (*preyIterator)->SetPosition(newPlace);
        }
      preyIterator++;
    }
  return outStatus;
}

std::string World::makeStateForRLPreditor(std::pair<int, int> coord, int visionRange)
{
  std::stringstream output;
  output << Preditor::PairToCoord(coord);
  output << "~"; //add the break
  output << World::MakePreditorVisionState(coord, visionRange);
  output << "~"; //add the break
  output << World::MakePreditorVectorState(coord, visionRange);
  return output.str();
}

std::pair<int, int> World::applyMove(std::string name, PP_ACTIONS action, PP_MOVE_RESULTS& result)
{
  bool done = false;
  std::pair<int, int> next;
  std::vector<std::shared_ptr < Prey>>::iterator preyIterator = preys.begin();
  while (done == false && preyIterator != preys.end())
    {
      if ((*preyIterator)->GetLocalName().compare(name) == 0)
        {//found it
          //do movement
          done = true;
          std::pair<int, int> current = (*preyIterator)->GetPosition();
          next = current;
          if (action == PP_ACTIONS::S)
            {
              next.second--;
            }
          else if (action == PP_ACTIONS::N)
            {
              next.second++;
            }
          else if (action == PP_ACTIONS::E)
            {
              next.first++;
            }
          else if (action == PP_ACTIONS::W)
            {
              next.first--;
            }
          else if (action == PP_ACTIONS::SW)
            {
              next.first--;
              next.second--;
            }
          else if (action == PP_ACTIONS::SE)
            {
              next.first++;
              next.second--;
            }
          else if (action == PP_ACTIONS::NW)
            {
              next.first--;
              next.second++;
            }
          else if (action == PP_ACTIONS::NE)
            {
              next.first++;
              next.second++;
            }
          else if (action == PP_ACTIONS::P)
            {

            }
          else
            {
              std::cout << "World::applyMove Unknown Direction\n";
            }
          if (isPreyOccupied(next.first, next.second) == false && ((world[next.first][next.second] == PP_SQUARE_TYPE::EMPTY) || (world[next.first][next.second] == PP_SQUARE_TYPE::PREY_ONLY)))
            {//if prey can enter
              (*preyIterator)->SetPosition(next);
              result = PP_MOVE_RESULTS::SUCCESS;
            }
          else
            {//cant enter  safely pred, prey or wall
              //do result
              if (isPreditorOccupied(next.first, next.second))
                {//prey moved into pred
                  result = PP_MOVE_RESULTS::PREY_INTO_PRED;
                }
              else
                {//failed to move
                  next = current;
                  result = PP_MOVE_RESULTS::INTO_IMPASSABLE;
                }
            }
        }
      preyIterator++;
    }
  std::vector<std::shared_ptr < Preditor>>::iterator preditorsIterator = preditors.begin();
  while (done == false && preditorsIterator != preditors.end())
    {
      if ((*preditorsIterator)->GetLocalName().compare(name) == 0)
        {//found it
          //do movement
          done = true;
          std::pair<int, int> current = (*preditorsIterator)->GetPosition();
          next = current;
          if (action == PP_ACTIONS::S)
            {
              next.second--;
            }
          else if (action == PP_ACTIONS::N)
            {
              next.second++;
            }
          else if (action == PP_ACTIONS::E)
            {
              next.first++;
            }
          else if (action == PP_ACTIONS::W)
            {
              next.first--;
            }
          else if (action == PP_ACTIONS::SW)
            {
              next.first--;
              next.second--;
            }
          else if (action == PP_ACTIONS::SE)
            {
              next.first++;
              next.second--;
            }
          else if (action == PP_ACTIONS::NW)
            {
              next.first--;
              next.second++;
            }
          else if (action == PP_ACTIONS::NE)
            {
              next.first++;
              next.second++;
            }
          else if (action == PP_ACTIONS::P)
            {

            }
          else
            {
              std::cout << "World::applyMove Unknown Direction\n";
            }
          //check for valid move
          if (isPreditorOccupied(next.first, next.second) == true || (world[next.first][next.second] == PP_SQUARE_TYPE::IMPASSABLE) || (world[next.first][next.second] == PP_SQUARE_TYPE::PREY_ONLY) || (world[next.first][next.second] == PP_SQUARE_TYPE::WALL))
            {//if pred cant enter
              next = current; //undo action
            }
          //see what move did
          if (isPreyOccupied(next.first, next.second))//check prey first as itself may prevent it catching prey
            {//prey moved into pred
              result = PP_MOVE_RESULTS::PRED_INTO_PREY;
              (*preditorsIterator)->SetPosition(next);
            }
          else if ((world[next.first][next.second] == PP_SQUARE_TYPE::EMPTY) || (world[next.first][next.second] == PP_SQUARE_TYPE::PREDITOR_ONLY))
            {//moved into ok square
              (*preditorsIterator)->SetPosition(next);
              result = PP_MOVE_RESULTS::SUCCESS;
            }
          else
            {//moved into an invalid square not corrected earlier
              std::cout << "World::applyMove executed action badly\n";
              std::exit(53522);
              (*preditorsIterator)->SetPosition(next);
              result = PP_MOVE_RESULTS::INTO_IMPASSABLE;
            }
        }
      preditorsIterator++;
    }
  return next;
}

PP_ACTIONS World::ActionStringToEnum(std::string input)
{
  for (int a = 0; a < PP_ACTIONS::END_OF_ENUM; a++)
    {
      if (input == PP_ACTIONS_STRINGS[a])
        {
          return (PP_ACTIONS) a;
        }
    }
  std::cout << "World::ActionStringToEnum Unknown action " << input << "\n";
  std::exit(4324);
  return PP_ACTIONS::END_OF_ENUM;
}

/**
 * 0 nothing 1 prey 2 pred 3 both
 * @param location
 * @param visionRange
 * @return
 */
std::string World::MakePreditorVisionState(std::pair<int, int> location, int visionRange)
{
  std::stringstream output;
  int isVisableUp = 0;
  int isVisableDown = 0;
  int isVisableRight = 0;
  int isVisableLeft = 0;
  if (PP_MULTI_FEATURE_VISION)
    {
      //std::cout << "Location= " << location.first << "," << location.second << ") " << visionRange << " \n";
      for (int a = visionRange; a > 0; a--)
        {//if can be seen up or right
          if (isPreyOccupied(location.first, location.second + a))
            {
              if (isVisableUp == 0)
                {//if nothing seen yet
                  isVisableUp = 1;
                }
              else
                {
                  isVisableUp = 3;
                }
            }
          if (isPreditorOccupied(location.first, location.second + a))
            {
              if (isVisableUp == 0)
                {//if nothing seen yet
                  isVisableUp = 2;
                }
              else
                {
                  isVisableUp = 3;
                }
            }
          if (isPreyOccupied(location.first + a, location.second))
            {
              if (isVisableRight == 0)
                {//if nothing seen yet
                  isVisableRight = 1;
                }
              else
                {
                  isVisableRight = 3;
                }
            }
          if (isPreditorOccupied(location.first + a, location.second))
            {
              if (isVisableRight == 0)
                {//if nothing seen yet
                  isVisableRight = 2;
                }
              else
                {
                  isVisableRight = 3;
                }
            }
        }
      for (int a = -visionRange; a < 0; a++)
        {//if can be seen down or left
          if (isPreyOccupied(location.first, location.second + a))
            {
              if (isVisableDown == 0)
                {//if nothing seen yet
                  isVisableDown = 1;
                }
              else
                {
                  isVisableDown = 3;
                }
            }
          if (isPreditorOccupied(location.first, location.second + a))
            {
              if (isVisableDown == 0)
                {//if nothing seen yet
                  isVisableDown = 2;
                }
              else
                {
                  isVisableDown = 3;
                }
            }
          if (isPreyOccupied(location.first + a, location.second))
            {
              if (isVisableLeft == 0)
                {//if nothing seen yet
                  isVisableLeft = 1;
                }
              else
                {
                  isVisableLeft = 3;
                }
            }
          if (isPreditorOccupied(location.first + a, location.second))
            {
              if (isVisableLeft == 0)
                {//if nothing seen yet
                  isVisableLeft = 2;
                }
              else
                {
                  isVisableLeft = 3;
                }
            }
        }
      if (location.first < PP_WORLD_WIDTH - 1 && world[location.first + 1][location.second] == PP_SQUARE_TYPE::WALL || world[location.first + 1][location.second] == PP_SQUARE_TYPE::IMPASSABLE)
        {
          isVisableLeft = 4;
        }
      if (location.first > 1 && world[location.first - 1][location.second] == PP_SQUARE_TYPE::WALL || world[location.first - 1][location.second] == PP_SQUARE_TYPE::IMPASSABLE)
        {
          isVisableRight = 4;
        }
      if (location.first < PP_WORLD_HIGHT - 1 && world[location.first][location.second + 1] == PP_SQUARE_TYPE::WALL || world[location.first][location.second + 1] == PP_SQUARE_TYPE::IMPASSABLE)
        {
          isVisableUp = 4;
        }
      if (location.first > 1 && world[location.first][location.second - 1] == PP_SQUARE_TYPE::WALL || world[location.first][location.second - 1] == PP_SQUARE_TYPE::IMPASSABLE)
        {
          isVisableDown = 4;
        }
    }
  else
    {
      if (PP_USING_CROSS_VISION)
        {
          for (int a = visionRange; a > 0; a--)
            {//if can be seen up or right
              if (isPreyOccupied(location.first, location.second + a))
                {
                  isVisableUp = 1;
                }

              if (isPreyOccupied(location.first + a, location.second))
                {
                  isVisableRight = 1;
                }

            }
          for (int a = -visionRange; a < 0; a++)
            {//if can be seen down or left
              if (isPreyOccupied(location.first, location.second + a))
                {
                  isVisableDown = 1;
                }

              if (isPreyOccupied(location.first + a, location.second))
                {
                  isVisableLeft = 1;

                }

            }
        }
      else
        {
          for (int a = -visionRange; a <= visionRange; a++)
            {//if can be seen up or down
              for (int b = -visionRange; b <= visionRange; b++)
                {
                  if (isPreyOccupied(location.first + a, location.second + b))
                    {//right or left
                      if (b > 0)
                        {
                          isVisableUp = 1;
                        }
                      else if (b < 0)
                        {
                          isVisableDown = 1;

                        }
                      if (a > 0)
                        {
                          isVisableRight = 1;
                        }
                      else if (a < 0)
                        {
                          isVisableLeft = 1;

                        }
                    }

                }
            }
        }

    }
  output << "U" << isVisableUp << "-D" << isVisableDown << "-L" << isVisableLeft << "-R" << isVisableRight;
  return output.str();
}

/**
 * 0 nothing 1 prey 2 pred 3 both
 * @param location
 * @param visionRange
 * @return
 */
std::string World::MakePreditorVectorState(std::pair<int, int> location, int visionRange)
{
  if (PP_USE_VECTOR_POLICY == true && (this->preditors.size() != 2 || this->preys.size() != 1))
    {
      std::cout << "World::MakePreditorVectorState something is the wrong size \n";
      std::exit(235);
    }
  std::string output;
  std::pair<int, int> predLocation(-1000, -1000);
  std::pair<int, int> preyLocation(-1000, -1000);
  bool preyFound = false;
  bool predFound = false;
  for (int a = -visionRange; a <= visionRange; a++)
    {//find both other points
      for (int b = -visionRange; b <= visionRange; b++)
        {
          if (0 == b && a == 0)
            {//skip own location

            }
          else
            {
              if (preyFound != true && this->isPreyOccupied(a + location.first, b + location.second))
                {
                  preyLocation.first = a + location.first;
                  preyLocation.second = b + location.second;
                  preyFound = true;
                }
              if (predFound != true && this->isPreditorOccupied(a + location.first, b + location.second))
                {
                  predLocation.first = a + location.first;
                  predLocation.second = b + location.second;
                  predFound = true;
                }
              if (predFound && preyFound)
                {
                  break;
                }
            }
        }
    }
  //std::cout << "me= (" << location.first << ", " << location.second << ") pred= (" << predLocation.first << ", " << predLocation.second << ") prey= (" << preyLocation.first << ", " << preyLocation.second << ")\n";
  if ((predFound && preyFound) == false)
    {//if not found enough
      return "0_0_0";
    }
  double predDist = std::sqrt(std::pow(location.first - predLocation.first, 2) + std::pow(location.second - predLocation.second, 2));
  double preyDist = std::sqrt(std::pow(location.first - preyLocation.first, 2) + std::pow(location.second - preyLocation.second, 2));
  double p2pDist = std::sqrt(std::pow(predLocation.first - preyLocation.first, 2) + std::pow(predLocation.second - preyLocation.second, 2));
  //cos ang = -opp^2+pred^2+prey^2/2pred*prey
  double angle = ((std::pow(predDist, 2) + std::pow(preyDist, 2)) - (std::pow(p2pDist, 2))) / (2 * preyDist * predDist);
  //std::cout << "pp dist= " << p2pDist << " prey dist= " << preyDist << " pred dist= " << predDist << " ang= " << angle << "\n";
  //make arccos safe
  if (angle >= 1)
    {
      angle = 1.0;
    }
  if (angle <= -1)
    {
      angle = -1.0;
    }
  angle = std::acos(angle);
  //std::cout << "pp dist= " << p2pDist << " prey dist= " << preyDist << " pred dist= " << predDist << " ang= " << angle << "\n";
  angle = std::abs(angle * PP_DEGREES_PER_RADIAN);
  //std::cout << "pp dist= " << p2pDist << " prey dist= " << preyDist << " pred dist= " << predDist << " ang= " << angle << "\n";
  std::stringstream ss;
  ss << round2(predDist) << "_" << round2(preyDist) << "_" << round2(angle / PP_BIN_SIZE_DEGREES);
  output = ss.str();
  // std::cout << "state= " << output << "\n";
  return output;
}

void World::downTemperaturePreditor(double newOne)
{
  std::vector<std::shared_ptr < Preditor>>::iterator preditorsIterator = preditors.begin();
  while (preditorsIterator != preditors.end())
    {

      (*preditorsIterator)->changeTemperature((int) newOne);
      preditorsIterator++;
    }

}

/**
 * return if the past rey can see bools
 * @param location
 * @param visionRange
 * @return the bools in clockwise order from (inclusive) north
 */
std::vector<bool> World::makePreyVision(std::pair<int, int> location, int visionRange)
{
  std::vector<bool> output;
  bool isVisableUp = false;
  bool isVisableDown = false;
  bool isVisableRight = false;
  bool isVisableLeft = false;
  for (int a = visionRange; a > 0; a--)
    {//if can be seen up or right
      if (isPreditorOccupied(location.first, location.second + a))
        {
          isVisableUp = true;
        }
      if (isPreditorOccupied(location.first + a, location.second))
        {
          isVisableRight = true;
        }
    }
  for (int a = -visionRange; a < 0; a++)
    {//if can be seen down or left
      if (isPreditorOccupied(location.first, location.second + a))
        {
          isVisableDown = true;
        }
      if (isPreditorOccupied(location.first + a, location.second))
        {
          isVisableLeft = true;
        }
    }
  output.push_back(isVisableUp);
  output.push_back(isVisableRight);
  output.push_back(isVisableDown);
  output.push_back(isVisableLeft);
  return output;
}

/**
 * required because g++ doesn't correctly use cmath headers for round
 * @param input
 * @return
 */
int World::round2(double input)
{
  if (input >= floor(input) + 0.5)
    {
      return ceil(input);
    }
  else
    {
      return floor(input);
    }

}