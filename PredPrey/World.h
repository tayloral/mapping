/*
 * File:   World.h
 * Author: Adam
 *
 * Created on 08 April 2016, 11:23
 */
#include <array>
#include <algorithm>
#include <vector>
#include "Constants.h"
#include "Preditor.h"
#include "Prey.h"

#include <sys/types.h>
#include <unistd.h>
#include <sys/types.h>
//#include <sys/socket.h>
//#include <netinet/in.h>
#include <unistd.h>
//#include <arpa/inet.h>
//#include <netdb.h>
#include <memory.h>
//#include <ifaddrs.h>
//#include <net/if.h>
#include <errno.h>
#include <fstream>
#include <stdlib.h>
#include <memory>

#ifndef WORLD_H
#define WORLD_H

class World {
public:

    World();
    World(const World& orig);
    virtual ~World();
    void printToConsole();
    void addPreditor(std::shared_ptr<Preditor> input);
    void addPrey(std::shared_ptr<Prey> input);
    static std::string squareTypeToString(PP_SQUARE_TYPE input);
    static PP_SQUARE_TYPE randomSquareType(bool allowWall, bool allowObsticles);
    bool isPreyOccupied(int x, int y);
    bool isPreditorOccupied(int x, int y);
    bool isPreditorOccupied(std::pair<int, int> input);
    bool isOccupied(int x, int y);
    std::string getOccupierName(int x, int y);
    void printToUDP(int port, std::string message);
    std::string serialiseWorld();
    std::string printToString();
    int timeStep(std::string timeStepType, bool sequential);
    int sequentialRLTimeStep();
    int nonsequentialRLTimeStep();
    int nonsequentialRandomTimeStep();
    std::pair<int, int> applyMove(std::string name, PP_ACTIONS action, PP_MOVE_RESULTS& result);
    static PP_ACTIONS ActionStringToEnum(std::string input);
    void printToFile(std::string message, bool close = false, std::string filename = "");
    std::string makeStateForRLPreditor(std::pair<int, int> coord, int visionRange);
    std::string MakePreditorVisionState(std::pair<int, int> location, int visionRange);
    std::string MakePreditorVectorState(std::pair<int, int> location, int visionRange);
    std::vector<bool> makePreyVision(std::pair<int, int> location, int visionRange);
    void printTracesToFile(std::string filename, std::string tag);
    void setExploitaion();
    static int round2(double input);
    void downTemperaturePreditor(double newOne);
private:
    //these are the world based represnations whatever algoritm drives them can do so by name
    std::vector<std::shared_ptr<Preditor>> preditors;
    std::vector<std::shared_ptr<Prey>> preys;
    std::array<std::array<PP_SQUARE_TYPE, PP_WORLD_HIGHT>, PP_WORLD_WIDTH> world;
    std::ofstream fileOut;
    bool autoPauseToSend;
    std::pair<int, int> autoPauseCoord;
};

#endif /* WORLD_H */

