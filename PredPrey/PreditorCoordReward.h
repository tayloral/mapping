/*
 * File:   PreditorReward.h
 * Author: Adam
 *
 * Created on 31 May 2016, 15:27
 */
#ifndef PREDITORCOORDREWARD_H
#define PREDITORCOORDREWARD_H

#include "Reward.h"
#include "PPConst.h"
#pragma once

class PreditorCoordReward : public Reward {
public:
    PreditorCoordReward();
    void calcReward();
    void calcReward(std::string in);
    void calcReward(std::string oldState, std::string worldState);
    void setReward(double rewardIn);
    void setMoveResult(PP_MOVE_RESULTS moveResultIn);
    virtual ~PreditorCoordReward();
private:
    PP_MOVE_RESULTS moveResult;
};



#endif // PREDITORCOORDREWARD_H

