/*
 * File:   CartPoleAgent.cpp
 * Author: Adam
 *
 * Created on 17 June 2014, 14:17
 */

#include "CartPoleAgent.h"
#include "Constants.h"
#include "CPConst.h"
#include "RewardCartPole.h"
#include <string>
#include <sstream>

CartPoleAgent::CartPoleAgent(std::string name) : DWLAgent(name)
{
  this->setUsingTransferLearning(!true);

  //std::cerr << "Creating Agent " << this->getName() << std::endl;

  // instantiate the specific Reward for the policy which you have already defined
  rewardCartPole = std::make_shared<RewardCartPole>();

  // add the local policy to the agent and return a handle to it for creating the state space
  std::string policyName;
  if (name.find("Source") == std::string::npos)
    {//if not soucre
      policyName = std::string(this->getName() + "+CartPole");
    }
  else
    {
      policyName = std::string(this->getName() + "+CartPoleSource");
    }
  std::shared_ptr<Policy>cartPolePolicy = this->addLocalPolicy(policyName, rewardCartPole);


  std::stringstream ss;
  std::string cartPos[CPConst::NUMBER_OF_CART_POSSITIONS]; //left right centre
  for (int a = 0; a < CPConst::NUMBER_OF_CART_POSSITIONS; a++)
    {
      ss << "CPpos_" << a;
      cartPos[a] = ss.str();
      ss.str("");
    }
  std::string cartSpeed[CPConst::NUMBER_OF_CART_SPEEDS]; //fast left, slow left
  for (int a = 0; a < CPConst::NUMBER_OF_CART_SPEEDS; a++)
    {
      ss << "spd_" << a;
      cartSpeed[a] = ss.str();
      ss.str("");
    }
  std::string polePos[CPConst::NUMBER_OF_POLE_POSSITIONS]; //large lest, medium l, small l
  for (int a = 0; a < CPConst::NUMBER_OF_POLE_POSSITIONS; a++)
    {
      ss << "posP_" << a;
      polePos[a] = ss.str();
      ss.str("");
    }
  std::string poleSpeed[CPConst::NUMBER_OF_POLE_SPEEDS]; //fast left, slow left
  for (int a = 0; a < CPConst::NUMBER_OF_POLE_SPEEDS; a++)
    {
      ss << "spdP_" << a;
      poleSpeed[a] = ss.str();
      ss.str("");
    }
  std::string actions[2] = {"0", "1"};

  /*
   std::string cartPos[3] = {"L", "R", "C"}; //left right centre
  std::string cartSpeed[4] = {"FL", "FR", "SL", "SR"}; //fast left, slow left
  std::string polePos[6] = {"LL", "ML", "SL", "LR", "MR", "SR",}; //large lest, medium l, small l
  std::string poleSpeed[4] = {"FL", "FR", "SL", "SR"}; //fast left, slow left
  std::string actions[2] = {"0", "1"};*/

  //for boxes
  /* for (int a = 0; a < Constants::N_BOXES; a++)
   {//for all the boxes
       for (int b = 0; b < 2; b++)
       {//for all the actions
           std::stringstream ss;
           ss << a;
           ((WLearningProcess*) cartPolePolicy)->addStateAction(ss.str(), actions[b]);
       }
   }
   //add fail states
   ((WLearningProcess*) cartPolePolicy)->addStateAction("-1", actions[0]);
   ((WLearningProcess*) cartPolePolicy)->addStateAction("-1", actions[1]);*/
  //for state
  for (int a = 0; a < CPConst::NUMBER_OF_CART_POSSITIONS; a++)
    {
      for (int b = 0; b < CPConst::NUMBER_OF_CART_SPEEDS; b++)
        {
          for (int c = 0; c < CPConst::NUMBER_OF_POLE_POSSITIONS; c++)
            {
              for (int d = 0; d < CPConst::NUMBER_OF_POLE_SPEEDS; d++)
                {
                  for (int e = 0; e < 2; e++)
                    {//for all the actions
                      std::stringstream ss;
                      ss.str("");
                      ss << cartPos[a] << "&" << cartSpeed[b] << "&" << polePos[c] << "&" << poleSpeed[d];
                      std::static_pointer_cast<WLearningProcess>(cartPolePolicy)->addStateAction(ss.str(), actions[e]);
                    }
                }
            }
        }
    }
  //add fail states
  std::static_pointer_cast<WLearningProcess>(cartPolePolicy)->addStateAction("-1", actions[0]);
  std::static_pointer_cast<WLearningProcess>(cartPolePolicy)->addStateAction("-1", actions[1]);
  int boltzTemp = 1000; //BOTLZTEMP

  std::static_pointer_cast<WLearningProcess>(cartPolePolicy)->setBoltzmannTemperature(boltzTemp);
  std::static_pointer_cast<WLearningProcess>(cartPolePolicy)->setUseTaylor(true);

  std::static_pointer_cast<WLearningProcess>(cartPolePolicy)->setQAlpha(.4);
  std::static_pointer_cast<WLearningProcess>(cartPolePolicy)->setQGamma(0.9);

  this->manageLearning(true, true);
}

CartPoleAgent::~CartPoleAgent() { }

