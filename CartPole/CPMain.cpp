/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   CPMain.cpp
 * Author: Work-mine
 *
 * Created on 17 January 2017, 10:42
 */

#include <cstdlib>
#include <memory>
#include <iostream>
#include "CartPole.h"
#include "CartPoleAgent.h"
#include "DWLAgent.h"
#include "Constants.h"
#include <time.h>

void GENseedRand(int input)
{
  if (input >= 0)
    {
      srand(input);
    }
  else
    {
      srand(clock());
    }

}

/*
 *
 */
int CPtestCP()
{
  std::shared_ptr<CartPole> cp;
  std::cout << "made a CP\n";
  return 0;
}

/**
 * 146 max time 9 min with param a
 * 161 max time 13 min with param b
 * a run with random actions untill the pole falls over
 * @return the numebr of timesteps it took
 */
int CPrunRandomlyUntilFall(bool printToTerminal)
{
  int numberOfSteps = 0;
  std::shared_ptr<CartPole> cp(std::make_shared<CartPole>());
  if (printToTerminal)
    {
      std::cout << "made a CP\n";
    }
  while (cp->isFallen() == 0)
    {//while not fallen over
      numberOfSteps++;
      if (printToTerminal)
        {
          cp->printState();
        }
      int action = rand() % 2;
      if (printToTerminal)
        {
          std::cout << "action = " << action << "\n";
        }
      cp->executeAction(action);
    }
  if (printToTerminal)
    {
      cp->printState();
    }
  if (printToTerminal)
    {
      std::cout << "Lasted for " << numberOfSteps << " steps\n";
    }
  return numberOfSteps;
}

int CPrunRLUntilFall(bool printToTerminal, int numberOfFalls)
{
  int currentTemperature = numberOfFalls;
  int numberOfStepsTotal = 0;
  int fallCount = 0;
  std::shared_ptr<CartPole> cp(std::make_shared<CartPole>());
  std::shared_ptr<CartPoleAgent> agent(std::make_shared<CartPoleAgent>("TestRLAgent"));
  if (printToTerminal)
    {
      std::cout << "made a CP\n";
    }

  while (fallCount <= numberOfFalls)
    {
      currentTemperature--;
      if (fallCount < 500)
        {
          agent->changeActionSelectionTemperature(currentTemperature);
        }
      else if (fallCount > 830)
        {
          agent->changeActionSelectionTemperature(30);
        }
      else
        {
          agent->changeActionSelectionTemperature(1);
        }

      int numberOfStepsThisTime = 0;
      while (cp->isFallen() == 0)
        {//while not fallen over
          numberOfStepsThisTime++;
          if (printToTerminal)
            {
              cp->printState();
            }
          int action = atoi(agent->nominate().c_str());
          if (printToTerminal)
            {
              std::cout << "action = " << action << "\n";
            }
          cp->executeAction(action);
          agent->updateLocal(cp->getCurrentState());
        }
      cp->reset(); //stand it up again cos it's fallen over by now
      agent->updateLocal(cp->getCurrentState());
      fallCount++;
      std::cout << numberOfStepsThisTime << ",";
      numberOfStepsTotal += numberOfStepsThisTime;
    }
  agent->publishLocalPolicies(Constants::OUT_PATH(), Constants::EXP_NAME());
  if (printToTerminal)
    {
      cp->printState();
    }

  return numberOfStepsTotal;
}