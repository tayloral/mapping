/*
 * File:   Constants.h as https://webdocs.cs.ualberta.ca/~sutton/book/code/pole.c
 * Author: Adam
 *
 * Created on 17 June 2014, 14:30
 */

#ifndef CPCONST_H
#define CPCONST_H

class CPConst {
public:
    //run stuff
    const static int NUMBER_OF_ALPHAS = 10; //one less than is actually wanted
    const static int NUMBER_OF_GAMMAS = 10; //11
    const static int NUMBER_OF_EXPS = 1; //10
    const static int NUMBER_OF_MT_CAR_EXPOLITE_RUNS = 1;
    const static int NUMBER_OF_CART_POLE_EXPLOITE_RUNS = 3;

    //RL stuff
    const static int NUMBER_OF_CART_POSSITIONS = 3;
    const static int NUMBER_OF_CART_SPEEDS = 5;
    const static int NUMBER_OF_POLE_SPEEDS = 5;
    const static int NUMBER_OF_POLE_POSSITIONS = 5;
    //CART POLE
    const static int CART_POLE_TRAINING_FALLS = 1000;
    const static int MAX_HEARTBEAT = 100000;
    /*** Parameters for RL ***/
    const static int N_BOXES = 162; /* Number of disjoint boxes of state space. */

    const static float left_edge() {
        return -2.4;
    }

    const static float right_edge() {
        return 2.4;
    }

    const static float max_theta_dot() {
        return ninety_degrees();
    }

    const static float cart_pos_bin() {
        return (left_edge()*-1 + right_edge()) / NUMBER_OF_CART_POSSITIONS;
    }

    const static float cart_speed_bin() {
        return 2.2 / NUMBER_OF_CART_SPEEDS; //use a likely speed range
    }

    const static float pole_speed_bin() {
        return max_theta_dot() / NUMBER_OF_POLE_SPEEDS; //use a likely speed range
    }

    const static float pole_pos_bin() {
        return twelve_degrees()*2 / NUMBER_OF_POLE_POSSITIONS; //use a likely speed range
    }

    const static float one_degree() {
        return 0.0174532;
    } /* 2pi/360 */

    const static float six_degrees() {
        return 0.1047192;
    }

    const static float twelve_degrees() {
        return 0.2094384;
    }

    const static float fifty_degrees() {
        return 0.87266; //rad/s
    }

    const static float ninety_degrees() {
        return 1.57079; //rad/s
    }

    const static float fifteen_degrees() {
        return 0.26179938779;
    }

    /*** Parameters for simulation ***/
    const static float GRAVITY() {
        return 9.8;
    }

    const static float MASSCART() {
        return 1.0;
    }

    const static float MASSPOLE() {
        return 0.1;
    }

    const static float TOTAL_MASS() {
        return (CPConst::MASSPOLE() + CPConst::MASSCART());
    }

    const static float LENGTH() {
        return 0.5;
    } /* actually half the pole's length */

    const static float POLEMASS_LENGTH() {
        return (CPConst::MASSPOLE() * CPConst::LENGTH());
    }

    const static float FORCE_MAG() {
        return 10; //param a
        //return 5; //param b
    }

    const static float TAU() {
        return 0.02;
    } /* seconds between state updates */

    const static float FOURTHIRDS() {
        return 1.3333333333333;
    };

    const static float DEG_IN_RAD() {
        return 57.2957795;
    }
};
#endif /* CPCONST_H */

