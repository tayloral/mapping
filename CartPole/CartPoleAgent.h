/*
 * File:   CartPoleAgent.h
 * Author: Adam
 *
 * Created on 17 June 2014, 14:17
 */
#include <string>
#include "RewardCartPole.h"
#include "DWLAgent.h"

#ifndef CARTPOLEAGENT_H
#define CARTPOLEAGENT_H

class CartPoleAgent : public DWLAgent {
public:
    CartPoleAgent(std::string name);
    ~CartPoleAgent();
private:
    std::shared_ptr<RewardCartPole> rewardCartPole;
};

#endif /* CARTPOLEAGENT_H */

