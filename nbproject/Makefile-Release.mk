#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=MinGW_64-Windows
CND_DLIB_EXT=dll
CND_CONF=Release
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/_ext/684b6d05/CPMain.o \
	${OBJECTDIR}/_ext/684b6d05/CartPole.o \
	${OBJECTDIR}/_ext/684b6d05/CartPoleAgent.o \
	${OBJECTDIR}/_ext/684b6d05/RewardCartPole.o \
	${OBJECTDIR}/_ext/c1e3ed61/ActionSelection.o \
	${OBJECTDIR}/_ext/c1e3ed61/Boltzmann.o \
	${OBJECTDIR}/_ext/c1e3ed61/CollaborationProcess.o \
	${OBJECTDIR}/_ext/c1e3ed61/Cusum.o \
	${OBJECTDIR}/_ext/c1e3ed61/DWLAgent.o \
	${OBJECTDIR}/_ext/c1e3ed61/EGready.o \
	${OBJECTDIR}/_ext/c1e3ed61/NeighbourReward.o \
	${OBJECTDIR}/_ext/c1e3ed61/Policy.o \
	${OBJECTDIR}/_ext/c1e3ed61/QTable.o \
	${OBJECTDIR}/_ext/c1e3ed61/Reward.o \
	${OBJECTDIR}/_ext/c1e3ed61/TaylorSeriesSelection.o \
	${OBJECTDIR}/_ext/c1e3ed61/TransferMapping.o \
	${OBJECTDIR}/_ext/c1e3ed61/WLearningProcess.o \
	${OBJECTDIR}/_ext/c1e3ed61/WTable.o \
	${OBJECTDIR}/_ext/b43478da/MTMain.o \
	${OBJECTDIR}/_ext/b43478da/MtCar.o \
	${OBJECTDIR}/_ext/1af43604/PPMain.o \
	${OBJECTDIR}/_ext/1af43604/Preditor.o \
	${OBJECTDIR}/_ext/1af43604/PreditorAgent.o \
	${OBJECTDIR}/_ext/1af43604/PreditorCoordReward.o \
	${OBJECTDIR}/_ext/1af43604/PreditorVectorReward.o \
	${OBJECTDIR}/_ext/1af43604/PreditorVisionReward.o \
	${OBJECTDIR}/_ext/1af43604/Prey.o \
	${OBJECTDIR}/_ext/1af43604/World.o \
	${OBJECTDIR}/main.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=
CXXFLAGS=

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/mapping.exe

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/mapping.exe: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/mapping ${OBJECTFILES} ${LDLIBSOPTIONS}

${OBJECTDIR}/_ext/684b6d05/CPMain.o: /C/Users/Work-mine/Documents/Code/mapping/CartPole/CPMain.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/684b6d05
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/684b6d05/CPMain.o /C/Users/Work-mine/Documents/Code/mapping/CartPole/CPMain.cpp

${OBJECTDIR}/_ext/684b6d05/CartPole.o: /C/Users/Work-mine/Documents/Code/mapping/CartPole/CartPole.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/684b6d05
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/684b6d05/CartPole.o /C/Users/Work-mine/Documents/Code/mapping/CartPole/CartPole.cpp

${OBJECTDIR}/_ext/684b6d05/CartPoleAgent.o: /C/Users/Work-mine/Documents/Code/mapping/CartPole/CartPoleAgent.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/684b6d05
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/684b6d05/CartPoleAgent.o /C/Users/Work-mine/Documents/Code/mapping/CartPole/CartPoleAgent.cpp

${OBJECTDIR}/_ext/684b6d05/RewardCartPole.o: /C/Users/Work-mine/Documents/Code/mapping/CartPole/RewardCartPole.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/684b6d05
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/684b6d05/RewardCartPole.o /C/Users/Work-mine/Documents/Code/mapping/CartPole/RewardCartPole.cpp

${OBJECTDIR}/_ext/c1e3ed61/ActionSelection.o: /C/Users/Work-mine/Documents/Code/mapping/DWL/src/ActionSelection.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/c1e3ed61
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/c1e3ed61/ActionSelection.o /C/Users/Work-mine/Documents/Code/mapping/DWL/src/ActionSelection.cpp

${OBJECTDIR}/_ext/c1e3ed61/Boltzmann.o: /C/Users/Work-mine/Documents/Code/mapping/DWL/src/Boltzmann.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/c1e3ed61
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/c1e3ed61/Boltzmann.o /C/Users/Work-mine/Documents/Code/mapping/DWL/src/Boltzmann.cpp

${OBJECTDIR}/_ext/c1e3ed61/CollaborationProcess.o: /C/Users/Work-mine/Documents/Code/mapping/DWL/src/CollaborationProcess.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/c1e3ed61
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/c1e3ed61/CollaborationProcess.o /C/Users/Work-mine/Documents/Code/mapping/DWL/src/CollaborationProcess.cpp

${OBJECTDIR}/_ext/c1e3ed61/Cusum.o: /C/Users/Work-mine/Documents/Code/mapping/DWL/src/Cusum.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/c1e3ed61
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/c1e3ed61/Cusum.o /C/Users/Work-mine/Documents/Code/mapping/DWL/src/Cusum.cpp

${OBJECTDIR}/_ext/c1e3ed61/DWLAgent.o: /C/Users/Work-mine/Documents/Code/mapping/DWL/src/DWLAgent.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/c1e3ed61
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/c1e3ed61/DWLAgent.o /C/Users/Work-mine/Documents/Code/mapping/DWL/src/DWLAgent.cpp

${OBJECTDIR}/_ext/c1e3ed61/EGready.o: /C/Users/Work-mine/Documents/Code/mapping/DWL/src/EGready.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/c1e3ed61
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/c1e3ed61/EGready.o /C/Users/Work-mine/Documents/Code/mapping/DWL/src/EGready.cpp

${OBJECTDIR}/_ext/c1e3ed61/NeighbourReward.o: /C/Users/Work-mine/Documents/Code/mapping/DWL/src/NeighbourReward.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/c1e3ed61
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/c1e3ed61/NeighbourReward.o /C/Users/Work-mine/Documents/Code/mapping/DWL/src/NeighbourReward.cpp

${OBJECTDIR}/_ext/c1e3ed61/Policy.o: /C/Users/Work-mine/Documents/Code/mapping/DWL/src/Policy.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/c1e3ed61
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/c1e3ed61/Policy.o /C/Users/Work-mine/Documents/Code/mapping/DWL/src/Policy.cpp

${OBJECTDIR}/_ext/c1e3ed61/QTable.o: /C/Users/Work-mine/Documents/Code/mapping/DWL/src/QTable.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/c1e3ed61
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/c1e3ed61/QTable.o /C/Users/Work-mine/Documents/Code/mapping/DWL/src/QTable.cpp

${OBJECTDIR}/_ext/c1e3ed61/Reward.o: /C/Users/Work-mine/Documents/Code/mapping/DWL/src/Reward.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/c1e3ed61
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/c1e3ed61/Reward.o /C/Users/Work-mine/Documents/Code/mapping/DWL/src/Reward.cpp

${OBJECTDIR}/_ext/c1e3ed61/TaylorSeriesSelection.o: /C/Users/Work-mine/Documents/Code/mapping/DWL/src/TaylorSeriesSelection.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/c1e3ed61
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/c1e3ed61/TaylorSeriesSelection.o /C/Users/Work-mine/Documents/Code/mapping/DWL/src/TaylorSeriesSelection.cpp

${OBJECTDIR}/_ext/c1e3ed61/TransferMapping.o: /C/Users/Work-mine/Documents/Code/mapping/DWL/src/TransferMapping.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/c1e3ed61
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/c1e3ed61/TransferMapping.o /C/Users/Work-mine/Documents/Code/mapping/DWL/src/TransferMapping.cpp

${OBJECTDIR}/_ext/c1e3ed61/WLearningProcess.o: /C/Users/Work-mine/Documents/Code/mapping/DWL/src/WLearningProcess.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/c1e3ed61
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/c1e3ed61/WLearningProcess.o /C/Users/Work-mine/Documents/Code/mapping/DWL/src/WLearningProcess.cpp

${OBJECTDIR}/_ext/c1e3ed61/WTable.o: /C/Users/Work-mine/Documents/Code/mapping/DWL/src/WTable.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/c1e3ed61
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/c1e3ed61/WTable.o /C/Users/Work-mine/Documents/Code/mapping/DWL/src/WTable.cpp

${OBJECTDIR}/_ext/b43478da/MTMain.o: /C/Users/Work-mine/Documents/Code/mapping/MountainCar/MTMain.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/b43478da
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/b43478da/MTMain.o /C/Users/Work-mine/Documents/Code/mapping/MountainCar/MTMain.cpp

${OBJECTDIR}/_ext/b43478da/MtCar.o: /C/Users/Work-mine/Documents/Code/mapping/MountainCar/MtCar.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/b43478da
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/b43478da/MtCar.o /C/Users/Work-mine/Documents/Code/mapping/MountainCar/MtCar.cpp

${OBJECTDIR}/_ext/1af43604/PPMain.o: /C/Users/Work-mine/Documents/Code/mapping/PredPrey/PPMain.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/1af43604
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1af43604/PPMain.o /C/Users/Work-mine/Documents/Code/mapping/PredPrey/PPMain.cpp

${OBJECTDIR}/_ext/1af43604/Preditor.o: /C/Users/Work-mine/Documents/Code/mapping/PredPrey/Preditor.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/1af43604
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1af43604/Preditor.o /C/Users/Work-mine/Documents/Code/mapping/PredPrey/Preditor.cpp

${OBJECTDIR}/_ext/1af43604/PreditorAgent.o: /C/Users/Work-mine/Documents/Code/mapping/PredPrey/PreditorAgent.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/1af43604
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1af43604/PreditorAgent.o /C/Users/Work-mine/Documents/Code/mapping/PredPrey/PreditorAgent.cpp

${OBJECTDIR}/_ext/1af43604/PreditorCoordReward.o: /C/Users/Work-mine/Documents/Code/mapping/PredPrey/PreditorCoordReward.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/1af43604
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1af43604/PreditorCoordReward.o /C/Users/Work-mine/Documents/Code/mapping/PredPrey/PreditorCoordReward.cpp

${OBJECTDIR}/_ext/1af43604/PreditorVectorReward.o: /C/Users/Work-mine/Documents/Code/mapping/PredPrey/PreditorVectorReward.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/1af43604
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1af43604/PreditorVectorReward.o /C/Users/Work-mine/Documents/Code/mapping/PredPrey/PreditorVectorReward.cpp

${OBJECTDIR}/_ext/1af43604/PreditorVisionReward.o: /C/Users/Work-mine/Documents/Code/mapping/PredPrey/PreditorVisionReward.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/1af43604
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1af43604/PreditorVisionReward.o /C/Users/Work-mine/Documents/Code/mapping/PredPrey/PreditorVisionReward.cpp

${OBJECTDIR}/_ext/1af43604/Prey.o: /C/Users/Work-mine/Documents/Code/mapping/PredPrey/Prey.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/1af43604
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1af43604/Prey.o /C/Users/Work-mine/Documents/Code/mapping/PredPrey/Prey.cpp

${OBJECTDIR}/_ext/1af43604/World.o: /C/Users/Work-mine/Documents/Code/mapping/PredPrey/World.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/1af43604
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1af43604/World.o /C/Users/Work-mine/Documents/Code/mapping/PredPrey/World.cpp

${OBJECTDIR}/main.o: main.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/main.o main.cpp

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
