/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   main.cpp
 * Author: Work-mine
 *
 * Created on 17 January 2017, 09:50
 */

#include <cstdlib>
#include<iostream>
#include <memory>
#include "Constants.h"
#include "limits.h"
#include "CartPole.h"
#include <chrono>
#include <ctime>
#include <sstream>

//general
void GENseedRand(int input);
//CP functions
int CPtestCP();
int CPrunRandomlyUntilFall(bool printToTerminal);
int CPrunRLUntilFall(bool printToTerminal, int numberOfFalls);
//MT functions
int MTtestMT();
int MTrunRandomlyUntilFinished(bool printToTerminal);
//PP functions
int PPrunRandomlyUntilCatch(bool printToTerminal);

/*
 *
 */
int main(int argc, char** argv)
{
  std::cout << "Project " << Constants::EXP_NAME() << "\n";
  std::chrono::time_point<std::chrono::system_clock> start, end;
  start = std::chrono::system_clock::now();
  std::stringstream ss;
  ss << "mkdir \"" << Constants::OUT_PATH();
  system(ss.str().c_str());
  int min = INT_MAX;
  int max = INT_MIN;
  for (int a = 0; a < 1; a++)
    {
      //GENseedRand(a);
      int number = CPrunRLUntilFall(!true, 1200);
      if (number < min)
        {
          min = number;
          std::cout << "temp run min= " << min << " a " << a << "\n";
        }
      if (number > max)
        {
          max = number;
          std::cout << "temp run max= " << max << " a " << a << "\n";
        }
    }
  std::cout << "run max= " << max << "run min= " << min << "\n";
  end = std::chrono::system_clock::now();
  std::chrono::duration<double> elapsed_seconds = end - start;
  std::time_t end_time = std::chrono::system_clock::to_time_t(end);
  std::cout << "finished computation at " << std::ctime(&end_time) << "elapsed time: " << elapsed_seconds.count() << "s\n";
  std::cout << "Ended " << Constants::EXP_NAME() << "\n";
  std::cin.get();
  return 0;
}

/**
 * //test random actiopn simulator infrastructure
 int min = INT_MAX;
  int max = INT_MIN;
  for (int a = 0; a < 10; a++)
    {
      std::cout << "b" << std::endl;
      //GENseedRand(a);
      int number = PPrunRandomlyUntilCatch(!true);
      if (number < min)
        {
          min = number;
          std::cout << "temp min= " << min << " a " << a << "\n";
        }
      if (number > max)
        {
          max = number;
          std::cout << "temp max= " << max << " a " << a << "\n";
        }
    }
  std::cout << "max= " << max << " min= " << min << "\n";*/