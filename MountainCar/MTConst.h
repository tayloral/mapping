/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   MTConst.h
 * Author: Work-mine
 *
 * Created on 19 January 2017, 10:04
 */

#ifndef MTCONST_H
#define MTCONST_H

class MTConst {
public:

    //mountain car
    //learn too fast ? ? ? ? ?
    const static int CAR_TRAINING = 10;
    const static int CAR_TRAINING_STEPS = (200); //now used
    const static int TEST_FREQ = 50000;
    const static int CAR_EXPLOITATION_TIME = 100000; //who cares how long results discarded
    const static int CAR_MAX = 100000;

    const static int MAX_HEARTBEAT = 100000;

    const static double LEFT_EDGE() {
        return -1.2;
    }

    const static double RIGHT_EDGE() {
        return 0.7;
    }

    const static double MAX_LEFT_X_DOT() {
        return -0.07;
    }

    const static double MAX_RIGHT_X_DOT() {
        return 0.07;
    }

    const static double TARGET_X() {
        return 0.6;
    }
};
#endif /* MTCONST_H */

