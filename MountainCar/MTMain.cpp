/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   MTMain.cpp
 * Author: Work-mine
 *
 * Created on 19 January 2017, 10:16
 */

#include <cstdlib>
#include "MtCar.h"
#include<iostream>
#include <memory>

/*
 *
 */
int MTtestMT()
{
  std::shared_ptr<MtCar> mt;
  std::cout << "made a MT\n";
  return 0;
}

/**
 *
 * a run with random actions untill the cer is done
 * @return the numebr of timesteps it took
 */
int MTrunRandomlyUntilFinished(bool printToTerminal)
{
  int numberOfSteps = 0;
  std::shared_ptr<MtCar> mt(std::make_shared<MtCar>());
  if (printToTerminal)
    {
      std::cout << "made a MT\n";
    }
  while (mt->isFinished() == 0)
    {//while not done
      numberOfSteps++;
      if (printToTerminal)
        {
          mt->printState();
        }
      int action = (rand() % 3) - 1; //-1,0,1
      if (printToTerminal)
        {
          std::cout << "action = " << action << "\n";
        }
      mt->executeAction(action);
    }
  if (printToTerminal)
    {
      mt->printState();
    }
  if (printToTerminal)
    {

      std::cout << "Lasted for " << numberOfSteps << " steps\n";
    }
  return numberOfSteps;
}


