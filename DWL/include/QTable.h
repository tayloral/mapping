/* 
 * File:   QTable.h
 * Author: Adam
 *
 * Created on October 8, 2012, 10:11 AM
 */
#include <vector>
#include <string>
#include <map>
#include <iostream>
#include "Cusum.h"

#ifndef QTABLE_H
#define QTABLE_H

struct compareClass {

    bool operator()(const std::string, std::pair<std::string, double> &left, const std::string, std::pair<std::string, double> &right) const {
        std::cerr << "comparing" << "\n";
        if (left.first.compare(right.first) == 0) {
            return true;
        } else {
            return false;
        }
    }
};

class QTable {
public:
    QTable();
    virtual ~QTable();

    void addStateAction(std::string stateName, std::string actionName, double value);
    double getQValue(std::string stateName, std::string actionName);
    void setQValue(std::pair<std::string, std::pair<std::string, double> > input);
    void setQValue(std::string stateName, std::string actionName, double value);
    std::vector<std::pair<std::string, double> > getActionsFromState(std::string stateName);
    void writeStateActionToFile(std::string filenameIn, std::string tag);
    void readStateActionFromFile(std::string filenameIn);
    void qLearningUpdate(std::string previousStateName, std::string actionName, std::string currentStateName, double reward);
    std::pair<std::string, double> getBestAction(std::string stateName);
    void setGamma(double gamma);
    double getGamma() const;
    void setAlpha(double alpha);
    double getAlpha() const;
    std::multimap<std::string, std::pair<std::string, double> > getQTable() const;
    std::pair<std::string, std::pair<std::string, double> > getQTableEntry(std::string state, std::string action);
    std::string getRandomState();
    std::string randomState; //hold a state to hack something to work
    std::vector<std::pair<std::string, std::string> > getStateOfGreatestChang(int amountToReturn); //community???//yes
    std::vector<std::pair<std::string, std::string> > getStatesOfTargetedBatchBased(int amountToReturn); //community???//yes
    void addFromQTable(std::multimap<std::string, std::pair<std::string, double> > in);
    void changeRandomState();
    std::vector<std::pair<std::string, std::string> > getStateOfMostVisitConverged(int amountToReturn);
    std::vector<std::pair<std::string, std::string> > getStateOfManyVisit(int amountToReturn);
    std::vector<std::pair<std::string, std::string> > getBestStates(int amountToReturn);
    //ADDED FOR EXP
    std::vector<std::pair<std::string, std::string> > getStateOfMostVisit(int amountToReturn);
    int getVisitCount(std::string stateName, std::string actionName);
    std::vector<std::pair<std::string, std::string> > getStateOfRandom(int amountToReturn);
    void setMasterCusum(std::shared_ptr<Cusum> masterCusum);
    std::shared_ptr<Cusum> getMasterCusum() const;
    void setStateDiverges(std::multimap<std::string, std::pair<std::string, std::shared_ptr<Cusum>> > stateDiverges);
    std::multimap<std::string, std::pair<std::string, std::shared_ptr<Cusum>> > getStateDiverges() const;
    void newMasterCusum(std::shared_ptr<Cusum> input);
    std::pair<int, int> stateActionsReportingChangeNumber();
    std::pair<double, double>stateActionsReportingChangeAverageMagnatude();
    int numberOfStateActionsConverged();
    int numberOfStateActions();
    void resetVisitCount(bool onlyChangedStates);
    int approximateNumberOfRegularVisitedStates();
    std::vector< std::pair<std::pair<std::string, std::string>, double> > sort(std::vector< std::pair<std::pair<std::string, std::string>, double> > input);
    void createAndSetShadowCopy();
    void destroyShadowCopy(bool acceptShadowAsCorrect);
    bool isCurrentCopyBetterThanShadow();
    //end
private:
    double alpha;
    double gamma;

    std::multimap<std::string, std::pair<std::string, double> > qTable; //map of statename as key to an action and value
    //ADDED FOR EXP
    std::multimap<std::string, std::pair<std::string, std::shared_ptr<Cusum>> > stateDiverges; //map of statename as key to an action and value
    std::multimap<std::string, std::pair<std::string, int> > visitCount; //map of statename as key to an action and value
    std::multimap<std::string, std::pair<std::string, bool> > converged; //map of statename as key to an action and value
    std::multimap<std::string, std::pair<std::string, double> > qTablePrevious; //map of statename as key to an action and value
    std::vector<std::pair<std::string, std::string>> mostRecentStatesTrace; //a trace of where we've been so that it can be sent
    bool mostRecentStatesTraceTransferable; //to indicate if we are good to send
    double rollingAverageReward;
    double rollingVarienceReward;
    int numberOfRewardSamples;
    std::multimap<std::string, std::pair<std::string, double> > shadowQTable; //mused to store a thing to try
    bool usingShadowTable;
    double shadowAverageReward;
    double shadowVarienceReward;
    int shadowNumberOfRewardSamples;

    std::shared_ptr<Cusum> masterCusum; //used to initalise states
    //END
};

#endif /* QTABLE_H */

