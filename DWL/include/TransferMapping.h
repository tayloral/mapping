/* 
 * File:   TransferMapping.h
 * Author: Adam
 *
 * Created on November 5, 2012, 3:42 PM
 */
#include "QTable.h"
#include "WTable.h"
#include "Reward.h"
#include <map>
#include <memory>
#include <string>

#ifndef TRANSFERMAPPING_H
#define TRANSFERMAPPING_H

class TransferMapping {
public:
    TransferMapping();
    virtual ~TransferMapping();
    void setTarget(std::shared_ptr<QTable> target);
    std::shared_ptr<QTable> getTarget() const;
    void setSource(std::shared_ptr<QTable> source);
    std::shared_ptr<QTable> getSource() const;
    void setWTarget(std::shared_ptr<WTable> target);
    std::shared_ptr<WTable> getWTarget() const;
    void setWSource(std::shared_ptr<WTable> source);
    std::shared_ptr<WTable> getWSource() const;
    void populateMappingRandomly();
    void populateMappingBySearch();
    void populateLearningMapping();
    void populateWMappingBySearch();
    std::pair<std::string, std::pair<std::string, double> > mapFromStateToTarget(std::pair<std::string, double> input);
    std::vector<std::pair<std::string, std::pair<std::string, double> > > mapFromStateToTarget(std::vector<std::pair<std::string, double> >input);
    std::pair<std::string, double> mapWFromStateToTarget(std::pair<std::string, double> input);
    std::vector<std::pair<std::string, double> > mapWFromStateToTarget(std::vector<std::pair<std::string, double> >input);
    static std::pair<std::string, double> makePairForMapper(std::pair<std::string, std::pair<std::string, double> > input);
    static std::vector<std::pair<std::string, double> > makePairForMapper(std::vector<std::pair<std::string, std::pair<std::string, double> > >input);
    bool getSourceSet();
    bool getTargetSet();
    bool getWSourceSet();
    bool getWTargetSet();
    void setTargetName(std::string TargetName);
    std::string getTargetName() const;
    void setSourceName(std::string sourceName);
    std::string getSourceName() const;
    void printMappingSourceFirst(std::string input, std::string tag);
    void printMappingTargetFirst(std::string input, std::string tag);
    void updateLearnedMappingFromTarget(std::string targetStateToUpdate, bool good);
    void updateLearnedMappingFromSource(std::string stateToUpdate);
    std::vector<std::pair<std::string, double> > choosePairsBasedOnVotes(int numberOfPairs, double greadiness, std::shared_ptr<QTable> qIn);
    void loadMapping(std::string filename);
    std::vector<std::string> split(std::string s, char delim);
    void runAnts(int antRuns, double threshold);
    void setTargetReward(std::shared_ptr<Reward> targetReward);
    std::shared_ptr<Reward> getTargetReward() const;
    void setSourceReward(std::shared_ptr<Reward> sourceReward);
    std::shared_ptr<Reward> getSourceReward() const;


private:
    std::string sourceName;
    std::string TargetName;
    bool sourceSet;
    bool targetSet;
    bool wSourceSet;
    bool wTargetSet;
    bool sourceRewardSet;
    bool targetRewardSet;
    std::shared_ptr<QTable> source;
    std::shared_ptr<QTable> target;
    std::shared_ptr<WTable> wSource;
    std::shared_ptr<WTable> wTarget;
    std::shared_ptr<Reward> sourceReward;
    std::shared_ptr<Reward> targetReward;
    std::multimap<std::string, std::pair<std::string, double> > mappings; //this is source state+action, target state+action, scaling factor for new info
    int deletedCount; //
    std::map<std::pair<std::string, std::string>, int> votes; //if it should change a mapping
    std::vector<std::string> sourceUnallocatedPool;
    std::vector<std::string> matchless;
    std::multimap<std::string, std::string> deleted;
    std::vector<std::string> sourceAllocatedPool;
    //std::vector<std::pair<std::string, std::string > > outBuffer; //memory of what has been sent from what
};

#endif /* TRANSFERMAPPING_H */

