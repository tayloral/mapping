/*
 * File:   DWLAgent.cpp
 * Author: Adam
 *
 * Created on July 11, 2012, 9:10 AM
 */

#include "DWLAgent.h"
#include "Policy.h"
#include "NeighbourReward.h"
#include "WLearningProcess.h"
#include <string>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <set>
#include <vector>
#include <locale>
#include <sstream>

/**
 *
 * @param ID
 */
DWLAgent::DWLAgent(std::string ID)
{
  shadowTranferCount = -1;
  this->collaborationPolicy = NULL; //do this so it doesn't get used by accident
  name = ID;
  totalReward = 0;
  historicReward = 0;
  this->previousWinner = ID;
  usingTransferLearning = false; //default to no transfer
}

/**
 * add in to the neighbour set what neighbours are passed
 * @param name what to add
 */
void DWLAgent::addNeighbours(std::string name)
{
  neighbours.insert(name);
}

std::string DWLAgent::getName()
{
  return name;
}

DWLAgent::~DWLAgent()
{//TODO make sure the elements are deleter properly. clear only empties the vector, it does not call ~
  //std::cerr << "1" << std::endl;
  std::vector<std::shared_ptr < Policy>>::iterator localIterator = localPolicies.begin();
  while (localIterator != localPolicies.end())
    {
      //        delete (*localIterator);//shared pointer now
      localIterator++;
    }
  localPolicies.clear();
  //std::cerr << "2" << std::endl;
  std::vector<std::shared_ptr < Policy>>::iterator remoteIterator = remotePolicies.begin();
  while (remoteIterator != remotePolicies.end())
    {
      //delete (*remoteIterator);//shared pointer now
      remoteIterator++;
    }
  remotePolicies.clear();
  neighbours.clear();
  communications.clear();
  sugestedActions.clear();
  //std::cerr << "3" << std::endl;
  if (collaborationPolicy != NULL)
    {
      //delete collaborationPolicy;//shared pointer now
    }
  //std::cerr << "4" << std::endl;
  //shared pointer now
  /*
  for (std::vector < std::shared_ptr<TransferMapping>>::iterator i = mappings.begin(); i != mappings.end(); ++i)
      delete *i;
  for (std::vector<std::shared_ptr < TransferMapping>>::iterator i = wMappings.begin(); i != wMappings.end(); ++i)
      delete *i;*/
  rewardLog.clear();
  //std::cerr << "5" << std::endl;
  //free memory
  std::vector<std::string> emptyVectorD;
  std::vector<std::shared_ptr < Policy>> emptyVectorP;
  std::vector<std::shared_ptr < TransferMapping>> emptyVectorT;
  std::vector< std::pair< std::string, std::pair<std::string, double> > > emptyVectorSSD;
  std::set<std::string, Compare> emptySetS;
  remotePolicies.swap(emptyVectorP);
  localPolicies.swap(emptyVectorP);
  neighbours.swap(emptySetS);
  communications.swap(emptyVectorSSD);
  sugestedActions.swap(emptyVectorSSD);
  mappings.swap(emptyVectorT);
  wMappings.swap(emptyVectorT);
  rewardLog.swap(emptyVectorD);
}

/** charris - taken out DOP
 * add a policy to this agent
 * @param name what to call it should be unique
 * @param rewardIn how to tell it what is good
 * @param the transfer set up
 */
std::shared_ptr<Policy> DWLAgent::addLocalPolicy(std::string name, std::shared_ptr<Reward> rewardIn, TransferConfiguration config, int learnedThold, int enviroThold, int startSharp, int startFast)
{
  std::shared_ptr<Policy> toAdd = this->addLocalPolicy(name, rewardIn);
  toAdd->setConfig(config);
  toAdd->setLearnedThreshold(learnedThold);
  toAdd->setEnvironmentChangeThreshold(enviroThold);
  toAdd->setSharpChangeStart(startSharp);
  toAdd->setFastChangeStart(startFast);
  return toAdd;
}

/**
 * change ptl parameters based on the environment
 */
void DWLAgent::adaptAndReconfigure()
{
  //std::cerr << "adapting +++++++++++++++++++++++++++++++++++++++\n";
  //for all locals
  std::vector<std::shared_ptr < Policy>>::iterator localIterator = localPolicies.begin();
  while (localIterator != localPolicies.end())
    {

      //get things
      std::shared_ptr<Policy> policy = *localIterator;
      //std::cerr << "policy " << policy->getPolicyName() << " =============================\n";
      TransferConfiguration currentConfig = policy->getConfig();
      std::pair<int, int> numberChanged = (std::static_pointer_cast<WLearningProcess>(policy))->stateActionsReportingChangeNumber();
      std::pair<double, double> amountChanged = (std::static_pointer_cast<WLearningProcess>(policy))->stateActionsReportingChangeAverageMagnatude();
      int numberLearned = (std::static_pointer_cast<WLearningProcess>(policy))->numberOfStateActionsConverged();
      int numberOfStateActions = (std::static_pointer_cast<WLearningProcess>(policy))->numberOfStateActions();
      policy->setEnvironmentChangeThreshold((std::static_pointer_cast<WLearningProcess>(policy))->getApproximatedNumberOfRegularlyVisitedStaes()); //this will set number of states that should change based on how frequently they are visited
      double changeTreshold = ((double) policy->getEnvironmentChangeThreshold()) / numberOfStateActions; //turn number of states to a %
      int learnedThreshold = policy->getLearnedThreshold();

      //now figure out the environemnt based on them
      double percentHigh = ((double) numberChanged.first) / numberOfStateActions;
      double percentLow = ((double) numberChanged.second) / numberOfStateActions;

      if ((percentHigh + percentLow) > changeTreshold)
        {//if is changing
          std::cerr << "Prercentage high= " << percentHigh << " Prercentage low= " << percentLow << " and " << changeTreshold << " changeTreshold" << "\n";
          std::cerr << "+change= " << amountChanged.first << " -change= " << amountChanged.second << "\n";
          if (amountChanged.first + amountChanged.second < policy->getFastChangeStart())
            {//if less than fast must be slow
              std::cerr << "adapting to slow change: setting true\n";
              currentConfig.environment.slowChange = true;
              rewardLog.push_back("slow");
            }
          else if (amountChanged.first + amountChanged.second < policy->getSharpChangeStart())
            {//if less than sharp must be fast
              std::cerr << "adapting to fast change: setting true\n";
              currentConfig.environment.fastChange = true;
              rewardLog.push_back("fast");
            }
          else if (amountChanged.first + amountChanged.second >= policy->getSharpChangeStart())
            {//if less than fast must be slow
              std::cerr << "adapting to sharp change: setting true\n";
              rewardLog.push_back("sharp");
              currentConfig.environment.sharpChnageHappened = true;
            }
          else
            {
              std::cerr << "DWLAGetne adapt and change, is changing but dont know in to waht\n";
              exit(5762);
            }
        }
      else
        {
          //std::cerr << "adapting to nothing: setting default\n";
          //not changing based on states so reset
          currentConfig.environment.fastChange = false;
          currentConfig.environment.processedSharpChnageHappened = false;
          currentConfig.environment.sharpChnageHappened = false;
          currentConfig.environment.slowChange = false;

        }

      if (numberLearned / numberOfStateActions > learnedThreshold)
        {//we've learned
          std::cerr << "adapting to finised learnig: setting true\n";
          rewardLog.push_back("learned");
          currentConfig.environment.iLearned = true;
          currentConfig.environment.processedILearned = false;
        }


      //now apply the change
      if (currentConfig.environment.iLearned == true && currentConfig.environment.processedILearned == false)
        {//if learning finished reduce threshold as less natural variation
          changeTreshold = changeTreshold * .5;
          currentConfig.environment.processedILearned = true;
          std::cerr << "adapting to i learned: changing thold\n";
        }
      if (currentConfig.environment.fastChange == false && currentConfig.environment.iChanged == false && currentConfig.environment.neighbourChange == false && currentConfig.environment.sharpChnageHappened == false && currentConfig.environment.slowChange == false)
        {//if no change from external factiors
          //todo use best transfer for steady state
          //std::cerr << "adapting to nothing: changing amount and selection\n";
          currentConfig.amountToTransfer = 10;
          currentConfig.selection = SelectionType::greatestChange;
        }
      else
        {
          //now react to the environment
          if (currentConfig.environment.slowChange == true)
            {//if slow
              //todo slow transfer unkown states  avoid sharing them
              std::cerr << "adapting to slow: changing amount and selection\n";
              currentConfig.selection = SelectionType::mostVisit; //this is the most visited after the reset we do
              currentConfig.amountToTransfer = 5;
              currentConfig.mergeParam = 1;
              (std::static_pointer_cast<WLearningProcess>(policy))->resetVisitCount(false); //reset on changing states and remodel
            }
          else if (currentConfig.environment.fastChange == true)
            {//if fast
              //todo fast change transfer only share the most recently visited or recieved
              std::cerr << "adapting to fast: changing amount and selection\n";
              currentConfig.selection = SelectionType::mostVisit; //this is the most visited after the reset we do
              currentConfig.amountToTransfer = 5;
              currentConfig.mergeParam = 4;
              (std::static_pointer_cast<WLearningProcess>(policy))->resetVisitCount(false); //reset visit count on all states so we can look for help again and remodel
            }
          else if (currentConfig.environment.sharpChnageHappened == true && currentConfig.environment.processedSharpChnageHappened == false)
            {//if sharp
              //todo for a while transefer nothing
              std::cerr << "adapting to sharp: changing amount to 0 and selection\n";
              currentConfig.selection = SelectionType::mostVisit; //this is the most visited after the reset we do
              currentConfig.amountToTransfer = 20;
              currentConfig.mergeParam = 4;
              (std::static_pointer_cast<WLearningProcess>(policy))->resetVisitCount(false); //reset visit count on all states so we can look for help again and remodel
              currentConfig.environment.processedSharpChnageHappened = true;
            }
          else
            {
              std::cerr << "adapting to something??\n";
            }
          if (currentConfig.environment.neighbourChange == true)
            {//if neighbout

            }
          if (currentConfig.environment.iChanged == true)
            {//if i changed

            }
        }
      if (currentConfig.environment.iChanged || currentConfig.environment.neighbourChange)
        {
          std::cerr << "DWLAgetn not sure how to hadel my own or nieghbour changes yet\n";
          //TODO check own changes and neighbour and time
        }
      //now move on with your life
      policy->setConfig(currentConfig);
      localIterator++;
      // std::cerr << "end " << policy->getPolicyName() << " =============================\n";
    }
  //std::cerr << "adapting --------------------------------------\n";
}

void DWLAgent::setConfigParams(TransferConfiguration tc, int learnedThold, int enviroThold, int startSharp, int startFast)
{
  std::vector<std::shared_ptr < Policy>>::iterator localIterator = localPolicies.begin();
  while (localIterator != localPolicies.end())
    {
      std::shared_ptr<Policy> policy = *localIterator;
      policy->setConfig(tc);
      policy->setLearnedThreshold(learnedThold);
      policy->setEnvironmentChangeThreshold(enviroThold);
      policy->setSharpChangeStart(startSharp);
      policy->setFastChangeStart(startFast);
      localIterator++;
    }
}

/**for debugging*/
void DWLAgent::printCurrentStates()
{
  std::vector<std::shared_ptr < Policy>>::iterator localIterator = localPolicies.begin();
  while (localIterator != localPolicies.end())
    {
      std::shared_ptr<Policy> policy = *localIterator;

      std::cerr << policy->getPolicyName() << "'s state is " << (std::static_pointer_cast<WLearningProcess>(policy))->getCurrentState() << "\n";
      localIterator++;
    }
}

/** charris - taken out DOP
 * add a policy to this agent
 * @param name what to call it should be unique
 * @param rewardIn how to tell it what is good
 */
std::shared_ptr<Policy> DWLAgent::addLocalPolicy(std::string name, std::shared_ptr<Reward> rewardIn)
{

  // create the Policy implementation
  WLearningProcess* wLearningProcess = new WLearningProcess(name);
  wLearningProcess->setReward(rewardIn);
  //wLearningProcess->configureLearningProcess();
  //rewardIn->calcReward();
  // cast into Policy container
  std::shared_ptr<Policy> toAdd = (std::shared_ptr<Policy>) wLearningProcess;

  // toAdd->setPolicyName(name);
  // toAdd->configureLearningProcess(rewardIn);
  localPolicies.push_back(toAdd);
  // return as a WLearningProcess for use by the sub-class

  return toAdd;
}

/**
 *add what comms gives
 */
void DWLAgent::addRemotePolicy(std::vector<std::pair<std::string, std::shared_ptr<WTable>> > policies)
{//std::cerr<<"dwl in add remote\n";
  std::vector<std::pair<std::string, std::shared_ptr < WTable>> >::iterator inputIterator = policies.begin();
  while (inputIterator != policies.end())
    {//for all the things to add
      std::shared_ptr<WTable> wTableToAdd = (*inputIterator).second;
      if (neighbours.find(wTableToAdd->getID()) != neighbours.end())
        {//if is in neighbours set
          bool isLocal = false;
          //prevent adding of its own local policies if they are read from comms
          std::vector<std::shared_ptr < Policy>>::iterator localIterator = localPolicies.begin();
          while (localIterator != localPolicies.end())
            {
              std::shared_ptr<Policy> policy = *localIterator;
              if (policy->getPolicyName() == (*inputIterator).first)
                {
                  //std::cerr<<"DWLAgent addRemotePolicy got a local one\n";
                  isLocal = true;
                  localIterator = localPolicies.end(); //end loop
                }
              else
                {
                  localIterator++;
                }
            }
          if (!isLocal)
            {//if is a remote policy
              std::vector<std::shared_ptr < Policy>>::iterator remoteIterator;
              remoteIterator = remotePolicies.begin();
              bool isThereAlready = false;
              if (remotePolicies.begin() == remotePolicies.end())
                {
                  std::shared_ptr<Policy> policyToAdd = (std::shared_ptr<Policy>)new WLearningProcess((*inputIterator).first);
                  policyToAdd->setPolicyName((*inputIterator).first);
                  std::shared_ptr<NeighbourReward> reward = std::make_shared<NeighbourReward>(); //use this as it is allows a singe reward sample from a neighbour to be treated in the same way as 'normal'.
                  (std::static_pointer_cast<WLearningProcess>(policyToAdd))->addRemoteFromWTable(wTableToAdd, this->replaceActionsInRemoteStateSpace(wTableToAdd)); //this strips the actions and replaces them
                  (std::static_pointer_cast<WLearningProcess>(policyToAdd))->setReward(reward);
                  (std::static_pointer_cast<WLearningProcess>(policyToAdd))->setQAlpha(0.01); //TODO place holder//im not responsable for seting these in general//they need to be inited here
                  (std::static_pointer_cast<WLearningProcess>(policyToAdd))->setWAlpha(.9);
                  (std::static_pointer_cast<WLearningProcess>(policyToAdd))->setQGamma(.1);
                  (std::static_pointer_cast<WLearningProcess>(policyToAdd))->setWGamma(.1);
                  (std::static_pointer_cast<WLearningProcess>(policyToAdd))->initalise();
                  remotePolicies.push_back(policyToAdd);
                  remoteIterator = remotePolicies.end();
                  //std::cerr << "Added " << policyToAdd->getPolicyName() << " to " << this->getName() << "\n";
                  isThereAlready = true;
                }
              else
                {// a remote policy and we already have at least one
                  while (remoteIterator != remotePolicies.end())
                    {//for each see if it matches
                      if ((*remoteIterator)->getPolicyName() == (*inputIterator).first)
                        {
                          isThereAlready = true;
                          remoteIterator = remotePolicies.end();
                        }
                      else
                        {
                          remoteIterator++;
                        }
                    }
                  if (!isThereAlready)
                    {//only add if there was no same named one
                      std::shared_ptr<Policy> policyToAdd = std::make_shared<Policy>((*inputIterator).first);
                      policyToAdd->setPolicyName((*inputIterator).first);
                      std::shared_ptr<NeighbourReward> reward = std::make_shared<NeighbourReward>(); //use this as it is allows a singe reward sample from a neighbour to be treated in the same way as 'normal'.
                      (std::static_pointer_cast<WLearningProcess>(policyToAdd))->addRemoteFromWTable(wTableToAdd, this->replaceActionsInRemoteStateSpace(wTableToAdd)); //this strips the actions and replaces them
                      (std::static_pointer_cast<WLearningProcess>(policyToAdd))->setReward(reward);
                      (std::static_pointer_cast<WLearningProcess>(policyToAdd))->initalise();
                      remotePolicies.push_back(policyToAdd);
                    }
                }
            }
          else
            {

            }
        }

      inputIterator++;
    }
}

/**
 *return how many neighbours in set
 */
int DWLAgent::numberOfNeighbours()
{

  return neighbours.size();
}

void DWLAgent::setUsingTransferLearning(bool usingTransferLearning)
{

  this->usingTransferLearning = usingTransferLearning;
}

bool DWLAgent::getUsingTransferLearning() const
{

  return usingTransferLearning;
}

/**
 *add what comms gives <remotePol><path with no -w.txt>
 *checks neighbour based on last \\ in path and + for policy after agent
 */
void DWLAgent::addRemotePolicy(std::string path)
{//TODO add check for adding its self as a remote

  //drop the tag
  path = path.substr(11, (path.length() - 11));
  std::string test = "";
  //split sting by <>s
  while (path.find_first_of("<") != std::string::npos && path.find_first_of(">") != std::string::npos)
    {//while still adderesses that is properly formatted
      //std::cerr<<"path: "<<path<<"\n";
      test = path.substr(path.find_first_of("<") + 1, (path.find_first_of(">") - 1)); //get the path
      path = path.substr(path.find_first_of(">") + 1, (path.length() - ((path.find_first_of(">") - 1)))); //clear handeled path
      std::string policyName = test.substr(test.find_last_of("\\") + 1, test.length() - 1);
      int index = policyName.find("+");
      index = policyName.find("+", index + 1);
      index++; //it is now at start of agent+policy in agent+local+agetn+policy
      policyName = policyName.substr(index, policyName.size() - index);
      std::string agentName = policyName.substr(0, policyName.find("+"));
      //std::cerr<<"before if add remote\n";
      //added a better way now  can do this //find that looks for sub string, that being is h1 is neighbout h1+p1 will not match
      bool canBeAdded = false;
      std::set<std::string, Compare>::iterator neighbourIterator = neighbours.begin();
      while (canBeAdded == false && neighbourIterator != neighbours.end())
        {
          //std::cerr << "compare " << (*neighbourIterator) << " 2 " << policyName << "\n";
          if ((*neighbourIterator).find(agentName) != std::string::npos)
            {//if the policy/agent name is contained in this neighbour
              canBeAdded = true;
              //std::cerr << "Match to neighbour\n";
            }
          neighbourIterator++;
        }

      if (canBeAdded)//TODO turn neighbours back on//if this policy is in the neighbour set
        {
          test = test + "-w"; //add this so it finds the way it was printed to file
          std::shared_ptr<WTable> wToAdd = std::make_shared<WTable>(policyName);
          bool isLocal = false;
          //prevent adding of its own local policies if they are read from comms
          std::vector<std::shared_ptr < Policy>>::iterator localIterator = localPolicies.begin();
          while (localIterator != localPolicies.end())
            {
              std::shared_ptr<Policy> policy = *localIterator;
              if (policy->getPolicyName() == policyName)
                {
                  std::cerr << "DWLAgent addRemotePolicy got a local one\n";
                  isLocal = true;
                  localIterator = localPolicies.end(); //end loop
                }
              else
                {
                  localIterator++;
                }
            }
          if (!isLocal)
            {//if is a remote policy
              std::vector<std::shared_ptr < Policy>>::iterator remoteIterator;
              remoteIterator = remotePolicies.begin();
              bool isThereAlready = false;
              if (remotePolicies.begin() == remotePolicies.end())
                {
                  if (wToAdd->readStateActionFromFile(test))
                    {
                      std::shared_ptr<Policy> policyToAdd = (std::shared_ptr<Policy>)new WLearningProcess(name);
                      policyToAdd->setPolicyName(policyName);
                      std::shared_ptr<NeighbourReward> reward = std::make_shared<NeighbourReward>(); //use this as it is allows a singe reward sample from a neighbour to be treated in the same way as 'normal'.
                      (std::static_pointer_cast<WLearningProcess>(policyToAdd))->addRemoteFromWTable(wToAdd, this->replaceActionsInRemoteStateSpace(wToAdd)); //this strips the actions and replaces them
                      (std::static_pointer_cast<WLearningProcess>(policyToAdd))->setReward(reward);
                      (std::static_pointer_cast<WLearningProcess>(policyToAdd))->setQAlpha(0.01); //TODO place holder//im not responsable for seting these in general//they need to be inited here
                      (std::static_pointer_cast<WLearningProcess>(policyToAdd))->setWAlpha(.9);
                      (std::static_pointer_cast<WLearningProcess>(policyToAdd))->setQGamma(.1);
                      (std::static_pointer_cast<WLearningProcess>(policyToAdd))->setWGamma(.1);
                      (std::static_pointer_cast<WLearningProcess>(policyToAdd))->setUseTaylor(true); //don't use bolz naymore
                      (std::static_pointer_cast<WLearningProcess>(policyToAdd))->setTaylorTemperature(500);
                      (std::static_pointer_cast<WLearningProcess>(policyToAdd))->initalise();
                      remotePolicies.push_back(policyToAdd);
                      remoteIterator = remotePolicies.end();
                      //std::cerr<<"curretn state= "<<(std::static_pointer_cast<WLearningProcess>(policyToAdd))->getCurrentState() << "\n";
                      //std::cerr << "Added " << policyToAdd->getPolicyName() << " to " << this->getName() << "\n";
                      isThereAlready = true;
                      //std::cerr<<"\n added the Wtable in addremote end bit \n";
                    }
                  else
                    {
                      std::cerr << "\ncouldn't add the Wtable in addremote for some reason\n";
                      exit(36634);
                    }
                }
              else
                {// a remote policy and we already have at least one
                  while (remoteIterator != remotePolicies.end())
                    {//for each see if it matches
                      if ((*remoteIterator)->getPolicyName() == policyName)
                        {
                          isThereAlready = true;
                          remoteIterator = remotePolicies.end();
                        }
                      else
                        {
                          remoteIterator++;
                        }
                    }
                  if (!isThereAlready)
                    {//only add if there was no same named one
                      //if this policy is not a local one
                      //TODO add a check for if already in remote


                      if (wToAdd->readStateActionFromFile(test))
                        {
                          std::shared_ptr<Policy> policyToAdd = (std::shared_ptr<Policy>)new WLearningProcess(name);
                          policyToAdd->setPolicyName(policyName);
                          std::shared_ptr<NeighbourReward> reward = std::make_shared<NeighbourReward>(); //use this as it is allows a singe reward sample from a neighbour to be treated in the same way as 'normal'.
                          (std::static_pointer_cast<WLearningProcess>(policyToAdd))->addRemoteFromWTable(wToAdd, this->replaceActionsInRemoteStateSpace(wToAdd)); //this strips the actions and replaces them
                          (std::static_pointer_cast<WLearningProcess>(policyToAdd))->setReward(reward);
                          (std::static_pointer_cast<WLearningProcess>(policyToAdd))->setQAlpha(0.01); //TODO place holder//im not responsable for seting these in general//they need to be inited here
                          (std::static_pointer_cast<WLearningProcess>(policyToAdd))->setWAlpha(.9);
                          (std::static_pointer_cast<WLearningProcess>(policyToAdd))->setQGamma(.1);
                          (std::static_pointer_cast<WLearningProcess>(policyToAdd))->setWGamma(.1);
                          (std::static_pointer_cast<WLearningProcess>(policyToAdd))->initalise();
                          (std::static_pointer_cast<WLearningProcess>(policyToAdd))->setUseTaylor(true); //don't use bolz naymore
                          (std::static_pointer_cast<WLearningProcess>(policyToAdd))->setTaylorTemperature(500);
                          remotePolicies.push_back(policyToAdd);
                          remoteIterator = remotePolicies.end();
                          //std::cerr << "curretn state= " << (std::static_pointer_cast<WLearningProcess>(policyToAdd))->getCurrentState() << "\n";
                          //std::cerr << "Added " << policyToAdd->getPolicyName() << " to " << this->getName() << "\n";
                          isThereAlready = true;
                          //std::cerr<<"\n added the Wtable in addremote end bit \n";
                        }
                      else
                        {
                          std::cerr << "\ncouldn't add the Wtable in addremote for some reason\n";
                        }
                    }
                  else
                    {
                      //std::cerr << "already there\n";
                    }
                }
              //delete wToAdd;
            }
          else
            {

              std::cerr << policyName + " wasn't in the neighbourhood of " << this->name << "\n";
            }
        }
    }
  //read each path and see if we have it

  //std::cerr<<"end add remote\n";
}

/**
 * go through all mappings and update them based on ants
 */
void DWLAgent::updateLearnedMappingFromAnts()
{
  std::vector<std::shared_ptr < TransferMapping>>::iterator mappingIterator = mappings.begin();
  while (mappingIterator != mappings.end())
    {//fort all mappings
      //get this mappings target
      (
              *mappingIterator)->runAnts(100, 1);
      mappingIterator++;
    }
}

/**
 * place holder this should be called by nighbour in emulated comms or some automated way in real comms.  it will not add the same policy twice
 * @param name
 * @param the dop
 */
void DWLAgent::addRemotePolicy(std::string name, std::vector<std::shared_ptr<WTable>> wTableIn)
{
  std::vector<std::shared_ptr < WTable>>::iterator inputIterator = wTableIn.begin();
  while (inputIterator != wTableIn.end())
    {//for all the things to add
      std::shared_ptr<WTable> wTableToAdd = (*inputIterator);
      if (neighbours.find(wTableToAdd->getID()) != neighbours.end())
        {//if is in neighbours set
          bool isLocal = false;
          //prevent adding of its own local policies if they are read from comms
          std::vector<std::shared_ptr < Policy>>::iterator localIterator = localPolicies.begin();
          while (localIterator != localPolicies.end())
            {
              std::shared_ptr<Policy> policy = *localIterator;
              if (policy->getPolicyName() == name)
                {
                  //std::cerr<<"DWLAgent addRemotePolicy got a local one\n";
                  isLocal = true;
                  localIterator = localPolicies.end(); //end loop
                }
              else
                {
                  localIterator++;
                }
            }
          if (!isLocal)
            {//if is a remote policy
              std::vector<std::shared_ptr < Policy>>::iterator remoteIterator;
              remoteIterator = remotePolicies.begin();
              bool isThereAlready = false;
              if (remotePolicies.begin() == remotePolicies.end())
                {
                  std::shared_ptr<Policy> policyToAdd = (std::shared_ptr<Policy>)new WLearningProcess(name);
                  policyToAdd->setPolicyName(name);
                  std::shared_ptr<NeighbourReward> reward = std::make_shared<NeighbourReward>(); //use this as it is allows a singe reward sample from a neighbour to be treated in the same way as 'normal'.
                  (std::static_pointer_cast<WLearningProcess>(policyToAdd))->addRemoteFromWTable(wTableToAdd, this->replaceActionsInRemoteStateSpace(wTableToAdd)); //this strips the actions and replaces them
                  (std::static_pointer_cast<WLearningProcess>(policyToAdd))->setReward(reward);
                  (std::static_pointer_cast<WLearningProcess>(policyToAdd))->setQAlpha(0.01); //TODO place holder//im not responsable for seting these in general//they need to be inited here
                  (std::static_pointer_cast<WLearningProcess>(policyToAdd))->setWAlpha(.9);
                  (std::static_pointer_cast<WLearningProcess>(policyToAdd))->setQGamma(.1);
                  (std::static_pointer_cast<WLearningProcess>(policyToAdd))->setWGamma(.1);

                  remotePolicies.push_back(policyToAdd);
                  remoteIterator = remotePolicies.end();
                  //std::cerr << "Added " << policyToAdd->getPolicyName() << " to " << this->getName() << "\n";
                  isThereAlready = true;
                }
              else
                {// a remote policy and we already have at least one
                  while (remoteIterator != remotePolicies.end())
                    {//for each see if it matches
                      if ((*remoteIterator)->getPolicyName() == name)
                        {
                          isThereAlready = true;
                          remoteIterator = remotePolicies.end();
                        }
                      else
                        {
                          remoteIterator++;
                        }
                    }
                  if (!isThereAlready)
                    {//only add if there was no same named one
                      std::shared_ptr<Policy> policyToAdd = std::make_shared<Policy>(name);
                      policyToAdd->setPolicyName(name);
                      std::shared_ptr<NeighbourReward> reward = std::make_shared<NeighbourReward>(); //use this as it is allows a singe reward sample from a neighbour to be treated in the same way as 'normal'.
                      (std::static_pointer_cast<WLearningProcess>(policyToAdd))->addRemoteFromWTable(wTableToAdd, this->replaceActionsInRemoteStateSpace(wTableToAdd)); //this strips the actions and replaces them
                      (std::static_pointer_cast<WLearningProcess>(policyToAdd))->setReward(reward);
                      remotePolicies.push_back(policyToAdd);
                    }
                }
            }
          else
            {

            }
        }

      inputIterator++;
    }
}

/**
 * WARNING: seems to have  a bug in which it strips a state from the state space use an atlernative
 * write out a vector of all the dops this agent has for local policies.  This is used to make policies avaliable to other agents.
 * @return a vector of policy's dops
 */
std::vector<std::shared_ptr<WTable>> DWLAgent::publishLocalPolicies()
{
  std::cerr << "WARNING possible bug needs fixing.  Strips state from statespace intermitantly\n";
  std::vector<std::shared_ptr < WTable>> output;
  std::vector<std::shared_ptr < Policy>>::iterator localIterator = localPolicies.begin();
  while (localIterator != localPolicies.end())
    {

      std::shared_ptr<Policy> policy = *localIterator;
      output.push_back((std::static_pointer_cast<WLearningProcess>(policy))->getWTable());
      localIterator++;
    }
  return output;
}

/**
 * write out a vector of all the wtables to file ans return a string of all paths.  This is used to make policies avaliable to other agents.
 * @param where to put them
 * @return a string of form <path><ACTUAL PATH>
 */
std::string DWLAgent::publishLocalPolicies(std::string path, std::string tag)
{
  std::string output = "";
  std::vector<std::shared_ptr < Policy>>::iterator localIterator = localPolicies.begin();
  while (localIterator != localPolicies.end())
    {

      std::shared_ptr<Policy> policy = *localIterator;
      output += "<path><" + path + policy->getPolicyName() + ">\n";
      (std::static_pointer_cast<WLearningProcess>(policy))->getQTable()->writeStateActionToFile((std::static_pointer_cast<WLearningProcess>(policy))->getPolicyName(), tag);
      (std::static_pointer_cast<WLearningProcess>(policy))->getWTable()->writeStateActionToFile((std::static_pointer_cast<WLearningProcess>(policy))->getPolicyName(), tag);
      localIterator++;
    }
  return output;
}

/**
 * this takes a dop and strips the states and add this agents actions to its statespace
 * @param wIn the w table fro the neighbour
 * @return the nice q table with my actions and its states
 */
std::shared_ptr<QTable> DWLAgent::replaceActionsInRemoteStateSpace(std::shared_ptr<WTable> wIn)
{
  std::shared_ptr<QTable> remotePolicy = (std::make_shared<QTable>()); //make my representation
  std::map<std::string, double> localCopy = wIn->getWTable();
  std::map<std::string, double>::iterator wIterator = localCopy.begin();
  std::vector<std::string> actions = (std::static_pointer_cast<WLearningProcess>((*localPolicies.begin())))->getActions();
  while (wIterator != localCopy.end())
    {//add all actions to the states as we go through
      std::vector<std::string>::iterator actionsIterator = actions.begin();

      while (actionsIterator != actions.end())
        {//for all actions
          //std::cerr<<"adding an action\n";
          remotePolicy->addStateAction(wIterator->first, (*actionsIterator), 0);
          actionsIterator++;
        }
      remotePolicy->randomState = wIterator->first;
      wIterator++;
    };
  localCopy.clear();
  actions.clear();

  return remotePolicy; //now give it back
}

/**
 * emmulate reciving a communication over a network into a buffer
 * @param name the name of the agent and policy giving this update
 * @param messageIn the  state and action and reward recieved at t-1
 */
void DWLAgent::addMessage(std::string name, std::pair<std::string, double> messageIn)
{

  std::pair< std::string, std::pair<std::string, double> > toAdd;
  toAdd.first = name;
  toAdd.second = messageIn;
  communications.push_back(toAdd);
}

/**put a tagged mesage into the right vecto dwl/transfer/feedback
 */
void DWLAgent::recieveMixedComms(std::string input)
{
  //std::cerr << "input= " << input << "\n";
  while (input.size() > 0 && input.find(">") != std::string::npos)
    {//if anything left
      std::string type = input.substr(0, input.find(">"));
      if (type.find("dwlUpdate") != std::string::npos)
        {//if is a dwl
          int index = 0;
          for (int a = 0; a < 4; a++)
            {//dwl updates have 4 closing angle brackets including tag so count that many
              index = input.find(">", index + 1);
            }
          //now index is last >
          index++;
          std::string update = input.substr(0, index);
          input = input.substr(index, input.length() - index);
          addMessage(update); //now add the dwl msg
        }
      else if (type.find("transfer") != std::string::npos)
        {//if is a transfer
          int index = 0;
          for (int a = 0; a < 4; a++)
            {//transfers updates have 4 closing angle brackets including tag so count that many
              index = input.find(">", index + 1);
            }
          //now index is last >
          index++;
          std::string update = input.substr(0, index);
          input = input.substr(index, input.length() - index);
          readTransferedInfoIn(update); //now add the transfer msg
        }
      else if (type.find("feedback") != std::string::npos)
        {//if is a feedback
          int index = 0;
          for (int a = 0; a < 4; a++)
            {//feedback updates have 4 closing angle brackets including tag so count that many
              index = input.find(">", index + 1);
            }
          //now index is last >
          index++;
          std::string update = input.substr(0, index);
          input = input.substr(index, input.length() - index);
          recieveFeedback(update); //now add the feedbCK msg
        }
      else
        {

          std::cerr << "DWLAgent::recieveMixedComms unknown input type: " << type << "\n";
          exit(872985);
        }
    }
}

/**
 * emmulate reciving a communication over a network into a buffer
 * @param messageIn string from comms
 */
void DWLAgent::addMessage(std::string messageIn)
{

  if (messageIn.substr(0, messageIn.find_first_of(">")).find("dwlUpdate") == std::string::npos)
    {//if not a dwl update
      //std::cerr << "DWLAgent+AddMessage - no tag dwlUpdate " << messageIn.substr(0, messageIn.find_first_of(">")) << " instead\n";
    }
  else
    {
      messageIn = messageIn.substr(messageIn.find_first_of(">") + 1, (messageIn.length() - (messageIn.find_first_of(">") + 1))); //clip of message type
      while (std::count(messageIn.begin(), messageIn.end(), '<') >= 3)
        {//while a properly formed message left

          std::string policy = messageIn.substr(messageIn.find_first_of("<") + 1, (messageIn.find_first_of(">") - (messageIn.find_first_of("<") + 1))); //name
          messageIn = messageIn.substr(messageIn.find_first_of(">") + 1, (messageIn.length() - (messageIn.find_first_of(">")) + 1));
          std::string state = messageIn.substr(messageIn.find_first_of("<") + 1, (messageIn.find_first_of(">") - (messageIn.find_first_of("<") + 1))); //state
          messageIn = messageIn.substr(messageIn.find_first_of(">") + 1, (messageIn.length() - (messageIn.find_first_of(">")) + 1));
          std::string value = messageIn.substr(messageIn.find_first_of("<") + 1, (messageIn.find_first_of(">") - (messageIn.find_first_of("<") + 1))); //value
          messageIn = messageIn.substr(messageIn.find_first_of(">") + 1, (messageIn.length() - (messageIn.find_first_of(">")) + 1));
          //std::cerr << "add message reading as " << policy << "\nstate= " << state << "\nvalue =" << value << "\n";

          std::pair< std::string, std::pair<std::string, double> > toAdd; //add the message
          toAdd.first = policy;
          toAdd.second.first = state;
          toAdd.second.second = atof(value.c_str());
          communications.push_back(toAdd);

        }
    }
}

/**
 * add both parts of messages at the same time
 * @param input many messages
 */
void DWLAgent::addMessage(std::pair< std::string, std::pair<std::string, double> > input)
{

  communications.push_back(input);
}

/**
 * place holder for writing out a comms update.
 * @return the previous winners stuff
 */
std::pair< std::string, std::pair<std::string, double> > DWLAgent::writeMessage()
{
  //replaced as should be updating based on what state it is and how good that is
  /*std::vector< std::pair< std::string, KeyValuePair*> >::iterator sugestedIterator = sugestedActions.begin();
  while ((*sugestedIterator).first != this->previousWinner)
  {//while not the right pair
  sugestedIterator++;
  }
  (*sugestedIterator).second->second=this->calculatedRewardForPreviousAction;
  std::cerr<<"adding message "<<(*sugestedIterator).first<<" "<<(*sugestedIterator).second->first->getID()<<"  "<<(*sugestedIterator).second->second<<"\n";
  return (*sugestedIterator);*/
  std::pair< std::string, std::pair<std::string, double> > out;
  out.first = (*localPolicies.begin())->getPolicyName();
  out.second = std::pair<std::string, double>((std::static_pointer_cast<WLearningProcess>((*this->localPolicies.begin())))->getCurrentState(), (std::static_pointer_cast<WLearningProcess>((*this->localPolicies.begin())))->getMostRecentReward());

  return out; //TODO make this work for several locals
}

/**
 * place holder for writing out a comms update.
 * @param dummy argument to differentiate as i am lazy
 * @return the previous winners stuff
 */
std::string DWLAgent::writeMessage(int)
{
  std::vector< std::pair< std::string, std::pair<std::string, double> > > ::iterator sugestedIterator = sugestedActions.begin();
  while (sugestedIterator != sugestedActions.end() && (*sugestedIterator).first != this->previousWinner)
    {//while not the right pair]
      sugestedIterator++;
    }
  if (sugestedIterator == sugestedActions.end())
    {
      return ""; //if no message send something
    }
  std::string out = "<dwlUpdate><";
  out += ((*sugestedIterator)).first;
  out += "><";
  //get the state to tell neighbour not the action
  std::vector<std::shared_ptr < Policy>>::iterator localIterator = localPolicies.begin();

  out += (std::static_pointer_cast<WLearningProcess>((*localIterator)))->getCurrentState();

  out += "><";
  std::stringstream strs;
  strs << (*sugestedIterator).second.second;
  out += strs.str();
  out += ">\n";
  std::cerr << "messege =" << out << "\n";

  return out; //TODO make this work for several locals
}

std::set<std::string, Compare> DWLAgent::getNeighbours() const
{

  return neighbours;
}

void DWLAgent::purgeCommsBuffer()
{
  //TODO posible delete them here

  communications.clear();
}

void DWLAgent::purgeSugestedActionsBuffer()
{//TODO posible delete them here

  sugestedActions.clear();
}

std::string DWLAgent::getName() const
{

  return name;
}

/** write a file for each policy that lives in this agent
@param name what prefix to look for
@param an optional tag "" or "error" no tag "plain" no .stats
 */
void DWLAgent::writePolicies(std::string name, std::string tag)
{
  std::vector<std::shared_ptr < Policy>>::iterator localIterator = localPolicies.begin();
  while (localIterator != localPolicies.end())
    {
      std::stringstream ss;
      std::shared_ptr<Policy> policy = *localIterator;
      ss << name << "+Local+" << policy->getPolicyName();
      (std::static_pointer_cast<WLearningProcess>(policy))->printStateSpace(ss.str(), tag);
      localIterator++;
    }
  std::vector<std::shared_ptr < Policy>>::iterator remoteIterator = remotePolicies.begin();
  while (remoteIterator != remotePolicies.end())
    {

      std::stringstream ss;
      std::shared_ptr<Policy> policy = *remoteIterator;
      ss << name << "+Remote+" << policy->getPolicyName();
      (std::static_pointer_cast<WLearningProcess>(policy))->printStateSpace(ss.str(), tag);
      remoteIterator++;
    }
}

/** read a file for each policy that lives in this agent
@param name what to prefix the policies with*/
void DWLAgent::readPolicies(std::string name)
{
  std::vector<std::shared_ptr < Policy>>::iterator localIterator = localPolicies.begin();
  while (localIterator != localPolicies.end())
    {
      std::shared_ptr<Policy> policy = *localIterator;
      (std::static_pointer_cast<WLearningProcess>(policy))->readStateSpace(name);
      localIterator++;
    }
  std::vector<std::shared_ptr < Policy>>::iterator remoteIterator = remotePolicies.begin();
  while (remoteIterator != remotePolicies.end())
    {

      std::shared_ptr<Policy> policy = *remoteIterator;
      (std::static_pointer_cast<WLearningProcess>(policy))->readStateSpace(name);
      remoteIterator++;
    }
}

/**
 *for agents with ability to work out reward from state
 * inform remote policies what happened in the simulator and update the comms vector to reflect reward.
 * @param stateName the name of the state
 */
void DWLAgent::updateLocal(std::string stateName)
{
  if (shadowTranferCount >= 0)
    {//if shouold be counting
      shadowTranferCount++;
    }
  std::stringstream strs;
  strs << historicReward;
  rewardLog.push_back(strs.str());
  //
  if (stateName.length() <= 0)
    {
      //do nothing

      std::cerr << this->getName() << "'s statename is too short in update local  " << "\n";
      std::cerr << stateName << "=statename " << remotePolicies.size() << "\n";
    }
  else
    {
      std::vector<std::shared_ptr < Policy>>::iterator localIterator = localPolicies.begin();
      while (localIterator != localPolicies.end())
        {
          // charris - downcast to specific Policy
          // example downcast Programmer *pProg = (Programmer *)&employee; THIS IS C STYLE
          std::shared_ptr<Policy> toCast = *localIterator;
          std::shared_ptr<WLearningProcess> test = std::static_pointer_cast<WLearningProcess>(toCast);
          // TODO - safe C++ style dynamic cast not working for some reason - base needs to be virtual??
          // B* bp = dynamic_cast<B*>(arg); - example C++ style downcast - safe as involves a runtime type check
          //WLearningProcess* test = dynamic_cast<WLearningProcess*>(*localIterator);
          if (test->getPolicyName() == previousWinner)
            {//if this one won tell it to update and learn
              //std::cerr<<"about to call update 1 from "<<test->getPolicyName()<<"\n";


              test->update(stateName, 0, true); //doesnt matter what reward is passed as local policies can self-calculate
              if (sugestedActions.size() > 0)
                {//if there is a publiched action
                  std::vector< std::pair< std::string, std::pair<std::string, double> > >::iterator sugestedIterator = sugestedActions.begin();
                  while ((*sugestedIterator).first != this->previousWinner)
                    {//find the winner in comms vector
                      sugestedIterator++;
                    }
                  //now we have the pointer to its update put in the reward
                  (*sugestedIterator).second.second = test->getMostRecentReward();
                }
              historicReward += test->getMostRecentReward(); //as this won accumulate its reward
              if (usingTransferLearning)
                {
                  //std::cerr << "about to make feedback\n";
                  if (test->getMostRecentReward() > 0)
                    {//if good reward feedback good
                      /*
                       //old update for if pos reward good was nieve if (usingTransferLearning)
                       {
                       std::stringstream ss;
                       ss << test->getPreviousState() << ":" << test->getCurrentAction();

                       test->provideTransferFeedback(ss.str(), true);
                       }*/
                    }
                  else
                    {//bad feeback
                      /*if (usingTransferLearning)
                      {
                      std::stringstream ss;
                      ss << test->getPreviousState() << ":" << test->getCurrentAction();

                      test->provideTransferFeedback(ss.str(), false);
                      }*/
                    }
                }
            }
          else
            {//it lost learn not nothing


              //	std::cerr<<"about to call update 2 from "<<test->getPolicyName()<<"\n";
              //set the winning action as current so that the loosing agent can learn from it
              test->update(stateName, this->winningAction, 0, false); //doesnt matter what reward is passed as local policies can self-calculate
              //	std::cerr << "1 " << std::endl;
              if (usingTransferLearning)
                {
                  //std::cerr << "about to make feedback\n";
                  if (test->getMostRecentReward() > 0)
                    {//if good reward feedback good
                      std::stringstream ss;
                      ss << test->getPreviousState() << ":" << test->getCurrentAction();
                      test->provideTransferFeedback(ss.str(), true);
                    }
                  else
                    {//bad feeback

                      std::stringstream ss;
                      ss << test->getPreviousState() << ":" << test->getCurrentAction();
                      test->provideTransferFeedback(ss.str(), false);
                    }
                }
            }
          localIterator++;
        }
    }

}

/**
 *for agents with ability to work out reward from state
 * inform remote policies what happened in the simulator and update the comms vector to reflect reward.
 * @param stateName the name of the state
 *@param rewardValueIn an externally calculated reward
 */
void DWLAgent::updateLocal(std::string stateName, double rewardValueIn)
{
  std::cerr << "update local 0\n" << std::endl;
  std::stringstream strs;
  strs << historicReward;
  rewardLog.push_back(strs.str());
  this->calculatedRewardForPreviousAction = rewardValueIn;
  std::vector<std::shared_ptr < Policy>>::iterator localIterator = localPolicies.begin();
  while (localIterator != localPolicies.end())
    {//std::cerr<<"update local 1\n";
      std::shared_ptr<Policy> test = *localIterator;

      //std::cerr << this->getName() << " is local updating " << test->getPolicyName() << "  " << previousWinner;
      if (test->getPolicyName() == previousWinner)
        {//if this one won tell it to update and learn
          //std::cerr<<"update local 2\n";
          // std::cerr<<"about to call update 3 from "<<test->getPolicyName()<<"\n";
          (std::static_pointer_cast<WLearningProcess>(test))->update(stateName, rewardValueIn, true);
          //std::cerr << "local as winner with " << localPolicies.size() << " policies\n";
          std::vector< std::pair< std::string, std::pair<std::string, double> > >::iterator sugestedIterator = sugestedActions.begin();
          while ((*sugestedIterator).first != this->previousWinner)
            {//find the winner in comms vector
              sugestedIterator++;
              //std::cerr<<"update local 3\n";
            }
          //now we have the pointer to its update put in the reward
          (*sugestedIterator).second.second = (std::static_pointer_cast<WLearningProcess>(test))->getMostRecentReward();
          totalReward += (std::static_pointer_cast<WLearningProcess>(test))->getMostRecentReward();
          historicReward += (std::static_pointer_cast<WLearningProcess>(test))->getMostRecentReward(); //as this won accumulate its reward
        }
      else
        {//it lost learn nothing
          //std::cerr << " as loser with " << localPolicies.size() << " policies\n";
          //std::cerr<<"update local 4\n";
          //std::cerr<<"about to call update 4 from "<<test->getPolicyName()<<"\n";
          //set the winning action as current so that the loosing agent can learn from it
          (

                  std::static_pointer_cast<WLearningProcess>(test))->update(stateName, this->winningAction, rewardValueIn, false); //give reward anyway as it keeps the tie code simple
        }
      localIterator++;
    }

}

/**
 * load the passed vector as comms.  allows integration with an external comms vector most notably the one i wrote for gridlab-d.
 */
void DWLAgent::readInComms(std::vector< std::pair< std::string, std::pair<std::string, double> > > input)
{
  while (communications.size() > 0)
    {
      communications.erase(communications.begin());
    }//out with the old

  //old non-neighbourly version //communications.insert(communications.begin(), input.begin(), input.end()); //in with the nucleus
  std::vector< std::pair< std::string, std::pair<std::string, double> > >::iterator communicationsIterator = input.begin();
  while (communicationsIterator != input.end())
    {
      std::pair< std::string, std::pair<std::string, double> > toTest = *communicationsIterator; //get the message
      if (neighbours.find(toTest.first) != neighbours.end())
        {//if was found as a neighbour

          communications.push_back(toTest); //in with the nucleus
        }
      communicationsIterator++;
    }
}

/**
 * inform remote policies what was communicated.  They update by finding the relivent update in the comms vector
 *
 */
void DWLAgent::updateRemote()
{
  //std::cerr << "in remote update for " << this->getName() << " numPolicies= " << remotePolicies.size() <<" "<< communications.size() << " messages" << "\n";
  std::vector<std::shared_ptr < Policy>>::iterator remoteIterator = remotePolicies.begin();
  while (remoteIterator != remotePolicies.end())
    {
      //std::cerr << "policy " << (*remoteIterator)->getPolicyName() << "'s turn " << communications.size() << " messages\n";
      std::string stateName;
      bool matchCheck = false;
      double rewardValue;
      std::shared_ptr<Policy> test = *remoteIterator;
      std::pair< std::string, std::pair<std::string, double> > toSearch;
      // find update in comms vector
      std::vector< std::pair< std::string, std::pair<std::string, double> > >::iterator communicationsIterator = communications.begin();
      while (!matchCheck && communicationsIterator != communications.end())
        {//std::cerr<<"1"<<"\n";
          toSearch = *communicationsIterator;
          //std::cerr<<"message name= "<<toSearch.first<<" policy name= "<<test->getPolicyName()<<"\n";
          if (toSearch.first == test->getPolicyName())
            {//if this is the update we are looking for this policy

              stateName = toSearch.second.first; //save the name of the state
              rewardValue = toSearch.second.second; //the reward
              matchCheck = true;
              //	std::cerr << "state name is " << stateName << "\n";
            }
          else
            {//keep looking
              //std::cerr<<"3"<<"\n";
              communicationsIterator++;
            }
        }
      if (communicationsIterator == communications.end())
        {//if we went through all of the updates and didnt find the right one or we got all the ones for an agent
          //not nessicarily wrong that there is no update for a given remote policy agent may not have done anything out of coms range etc
          //std::cerr << "DWLAgent updateRemote() failed to find update" << std::endl;
          //std::cerr<<"4"<<"\n";
          remoteIterator++;
        }
      else
        {//if the was an update appliy it
          //std::cerr << this->getName() << " is remote updating " << test->getPolicyName()<<" with state= "<<stateName << "  winner= " << previousWinner<<"\n";
          if (test->getPolicyName() == previousWinner)
            {//if this one won tell it to update and learn
              //std::cerr <<this->getName()<< " remote as winner with " << remotePolicies.size() << " policies and " << communications.size() << " messages\n";
              (std::static_pointer_cast<WLearningProcess>(test))->update(stateName, rewardValue, true);
              std::vector< std::pair< std::string, std::pair<std::string, double> > >::iterator sugestedIterator = sugestedActions.begin();
              while ((*sugestedIterator).first != this->previousWinner)
                {//find the winner in comms vector
                  sugestedIterator++;
                }
              //now we have the pointer to its update put in the reward
              (*sugestedIterator).second.second = (std::static_pointer_cast<WLearningProcess>(test))->getMostRecentReward();
              totalReward += toSearch.second.second;
              historicReward += toSearch.second.second; //as this won accumulate its reward
            }
          else
            {//it lost learn nothing but move to new state

              (std::static_pointer_cast<WLearningProcess>(test))->update(stateName, this->winningAction, rewardValue, false);
            }
          communications.erase(communicationsIterator); //delete the msg so we dont try to process it again
        }
    }
  this->purgeCommsBuffer(); //need this here as we should have done all msgs and griglab causes problems if it is in finish
  //this->colaberationPolicy->update(colaberationPolicy->getDOP()->getCurrentState(),this->totalReward,true);
  //        this->colaberationPolicy->changeCValue(); //basicly an update only works on a policy set up a colab
  totalReward = 0; //clear this so it can accumlate again
}

/**
 * make a new colaberation policy
 * @param numberOfvalues how the range should be split
 * @param maxValue range is 0 to this
 */
void DWLAgent::createColaberationPolicy(std::string name, int numberOfValues, double maxValue)
{

  collaborationPolicy = std::make_shared<CollaborationProcess>(name, numberOfValues, maxValue);
  collaborationPolicy->setReward(std::make_shared<NeighbourReward>());
}

/**
 * convinience function that is very important
 */
void DWLAgent::finishRun()
{
  //this->purgeCommsBuffer();//cant do this in gridlab as it needs an extra break
  if (collaborationPolicy != NULL)
    {
      this->collaborationPolicy->update(collaborationPolicy->getCurrentState(), this->totalReward, true); //state for colaberation// give the total reward and make it learn
    }
  //TODO Make stats here
  totalReward = 0;
  this->purgeSugestedActionsBuffer();
  std::vector<std::shared_ptr < Policy>>::iterator localIterator = localPolicies.begin();
  while (localIterator != localPolicies.end())
    {//put locals into sugestion vector and see best
      std::shared_ptr<Policy> test = *localIterator;
      //std::cerr<<(std::static_pointer_cast<WLearningProcess>( (test))->getPolicyName()<<"  finish run\n";
      if (usingTransferLearning)
        {//no extra cleaning needed
        }
      else
        {
          (std::static_pointer_cast<WLearningProcess>(test))->clearTLFeadback();
        }
      localIterator++;
    }
  std::vector<std::shared_ptr < Policy>>::iterator remoteIterator = remotePolicies.begin();
  while (remoteIterator != remotePolicies.end())
    {
      std::shared_ptr<Policy> test = *remoteIterator;
      if (usingTransferLearning)
        {//no extra cleaning needed
        }
      else
        {
          (std::static_pointer_cast<WLearningProcess>(test))->clearTLFeadback();
        }

      remoteIterator++;
    }
#if __cplusplus <= 199711L
#pragma message("This library code will leak memory in very long runs.Compile for at least a C++11 to include shrink to fit, or ocasionally reinitalise agents")
#else
  remotePolicies.shrink_to_fit();
  localPolicies.shrink_to_fit();
  communications.shrink_to_fit();
  sugestedActions.shrink_to_fit();
  mappings.shrink_to_fit();
  wMappings.shrink_to_fit();
  rewardLog.shrink_to_fit();
#endif

}

/**
 * take in the required information to make one end to end mapping
 * @param sourceNameIn agent name and policy name
 * @param tergetNameIn agent name and policy name
 * @param sourceIn the q table
 * @param targetIn the q table
 * @param source reward
 * @param target reward
 */
void DWLAgent::addMapping(std::string sourceNameIn, std::string targetNameIn, std::shared_ptr<QTable> sourceIn, std::shared_ptr<QTable> targetIn, std::shared_ptr<Reward> sourceRewardIn, std::shared_ptr<Reward> targetRewardIn)
{

  if (this->usingTransferLearning)
    {
      bool isPresent = false;
      std::vector<std::shared_ptr < TransferMapping>>::iterator mappingIterator = mappings.begin();
      while (mappingIterator != mappings.end())
        {
          if (((*mappingIterator)->getSourceName() == sourceNameIn) && ((*mappingIterator)->getTargetName() == targetNameIn))
            {//is there
              std::cout << "attempting to re-add mapping\n";
              isPresent = true;
              mappingIterator = mappings.end();
            }
          else
            {
              mappingIterator++;
            }
        }
      if (!isPresent)
        {//if not there add it
          std::shared_ptr<TransferMapping> map = std::make_shared<TransferMapping>(); //make it and set the things
          map->setSource(sourceIn);
          map->setTarget(targetIn);
          map->setTargetName(targetNameIn);
          map->setSourceName(sourceNameIn);
          map->setSourceReward(sourceRewardIn);
          map->setTargetReward(targetRewardIn);
          map->populateLearningMapping(); //TODO replace this with a populateMappingBySearch
          this->mappings.push_back(map);
        }

    }
  else
    {

      std::cerr << "not using transfer learning in DWLAgent so don't try to\n";
      exit(9867);
    }
  //std::cerr<<"mappings = "<<mappings.size()<<"\n";
}

/**
 * ask all policies what to do and select the best
 * @return what to do
 */
std::string DWLAgent::nominate()
{ //std::cerr<<"in dwlagent Nominate 1\n";//TODO make it so no colaberation policy is required to work
  //std::cerr<<"local size = "<<localPolicies.size()<<"\n";
  //this->purgeSugestedActionsBuffer();
  double colaberationCoefficent = 1; //todo make learn c= atof(collaborationPolicy->getCurrentState().c_str()); //get the state of the colab dop and make that a double
  std::vector<std::shared_ptr < Policy>>::iterator localIterator = localPolicies.begin();
  //std::cerr<<"in dwlagent Nominate 1.25\n";
  //cerr << (*localIterator)->getPolicyName() << "  ";
  std::pair<std::string, std::pair<std::string, double> > bestAction = (std::static_pointer_cast<WLearningProcess>(*localIterator))->getSuggestion(); //initalise the winner to first action
  std::shared_ptr<Policy> test = *localIterator; //put the inial winner into vector
  //std::cerr<<"in dwlagent Nominate 1.5\n";
  sugestedActions.push_back(bestAction);
  std::pair<std::string, std::pair<std::string, double> > sugestion;
  //std::cerr<<"local Policy "<<test->getPolicyName()<<" gave "<<bestAction.second.first<<" with w of "<<bestAction.second.second<<"\n";
  //std::cerr<<"in dwlagent Nominate 1.75\n";
  localIterator++; //point to next one
  //std::cerr<<"in dwlagent Nominate 2\n";
  while (localIterator != localPolicies.end())
    {//put locals into sugestion vector and see best
      test = *localIterator;
      //cerr << test->getPolicyName() << "  ";
      sugestion = (std::static_pointer_cast<WLearningProcess>(test))->getSuggestion();
      //std::cerr<<"local Policy "<<test->getPolicyName()<<" gave "<<sugestion.second.first<<" with w of "<<sugestion.second.second<<"\n";
      if (bestAction.second.second < sugestion.second.second)
        {//if this one is better
          //this->purgeSugestedActionsBuffer();//hack as fails with two?
          bestAction = sugestion;
          sugestedActions.push_back(sugestion);
        }
      localIterator++;
    }
  //std::cerr<<"in dwlagent Nominate 3\n";
  std::vector<std::shared_ptr < Policy>>::iterator remoteIterator = remotePolicies.begin();
  while (remoteIterator != remotePolicies.end())
    {//put remotes into sugestion vector and see best
      test = *remoteIterator;
      //std::cerr << test->getPolicyName() << "  \n";
      sugestion = (std::static_pointer_cast<WLearningProcess>(test))->getSuggestion();
      sugestion.second.second = sugestion.second.second*colaberationCoefficent; //w for remote = w*c

      //std::cerr<<"remote Policy "<<test->getPolicyName()<<" gave "<<sugestion.second.first<<" with w of "<<sugestion.second.second<<"\n";
      if (bestAction.second.second < sugestion.second.second)
        {
          //this->purgeSugestedActionsBuffer();//hack as fails with two?
          bestAction = sugestion;
          sugestedActions.push_back(sugestion);
          //std::cerr<<this->getName()<<" best Action is from remote "<<bestAction.first<<"\n";
        }
      remoteIterator++;
    }
  previousWinner = bestAction.first;
  this->winningAction = bestAction.second.first;
  //std::cerr << "previous winner was " << previousWinner << "\n";
  //std::cerr << "At " << this->getName() << " Winner is " << bestAction.first << " with action " << bestAction.second.first << " C was " << colaberationCoefficent << std::endl;

  return bestAction.second.first; //send the winner back
}

/**return how many policies there ate*/
int DWLAgent::numberOfLocalPolicies()
{

  return this->localPolicies.size();
}

/**return how many policies there ate*/
int DWLAgent::numberOfRemotePolicies()
{

  return this->remotePolicies.size();
}

/**
 * take in the required information to make one end to end mapping
 * @param sourceNameIn agent name and policy name
 * @param tergetNameIn agent name and policy name
 * @param sourceIn the q table
 * @param targetIn the q table
 */
void DWLAgent::addSearchMapping(std::string sourceNameIn, std::string targetNameIn, std::shared_ptr<QTable> sourceIn, std::shared_ptr<QTable> targetIn)
{
  if (this->usingTransferLearning)
    {
      bool isPresent = false;
      std::vector<std::shared_ptr < TransferMapping>>::iterator mappingIterator = mappings.begin();
      while (mappingIterator != mappings.end())
        {
          if (((*mappingIterator)->getSourceName() == sourceNameIn) && ((*mappingIterator)->getTargetName() == targetNameIn))
            {//is there
              isPresent = true;
              mappingIterator = mappings.end();
            }
          else
            {
              mappingIterator++;
            }
        }
      if (!isPresent)
        {//if not there add it
          std::shared_ptr<TransferMapping> map = std::make_shared<TransferMapping>(); //make it and set the things
          map->setSource(sourceIn);
          map->setTarget(targetIn);
          map->setTargetName(targetNameIn);
          map->setSourceName(sourceNameIn);
          map->populateMappingBySearch(); //TODO replace this with a
          //map->populateMappingRandomly();
          this->mappings.push_back(map);
        }

    }
  else
    {

      std::cerr << "not using transfer learning in DWLAgent so don't try to\n";
      exit(9867);
    }
  //std::cerr << "mappings = " << mappings.size() << "\n";
}

/**
 * take in the required information to make one end to end mapping
 * @param sourceNameIn agent name and policy name
 * @param tergetNameIn agent name and policy name
 * @param sourceIn the q table
 * @param targetIn the q table
 */
void DWLAgent::addMapping(std::string sourceNameIn, std::string targetNameIn, std::shared_ptr<QTable> sourceIn, std::shared_ptr<QTable> targetIn, bool populateRandomly)
{
  if (this->usingTransferLearning)
    {
      bool isPresent = false;
      std::vector < std::shared_ptr < TransferMapping>>::iterator mappingIterator = mappings.begin();
      while (mappingIterator != mappings.end())
        {
          if (((*mappingIterator)->getSourceName() == sourceNameIn) && ((*mappingIterator)->getTargetName() == targetNameIn))
            {//is there
              isPresent = true;
              mappingIterator = mappings.end();
            }
          else
            {
              mappingIterator++;
            }
        }
      if (!isPresent)
        {//if not there add it
          std::shared_ptr<TransferMapping> map = std::make_shared<TransferMapping>(); //make it and set the things
          map->setSource(sourceIn);
          map->setTarget(targetIn);
          map->setTargetName(targetNameIn);
          map->setSourceName(sourceNameIn);
          if (populateRandomly == false)
            {
              // std::cout << "pop lean map\n ";
              map->populateLearningMapping(); //TODO replace this with a populateMappingBySearch
            }
          else
            {
              map->populateMappingRandomly();
            }
          this->mappings.push_back(map);
        }

    }
  else
    {

      std::cerr << "not using transfer learning in DWLAgent so don't try to\n";
      exit(9867);
    }
  //std::cerr<<"mappings = "<<mappings.size()<<"\n";
}

/**
 * take in the required information to make one end to end mapping
 * @param sourceNameIn agent name and policy name
 * @param tergetNameIn agent name and policy name
 * @param where to old mapping is
 */
void DWLAgent::loadMapping(std::string sourceNameIn, std::string targetNameIn, std::string address)
{
  if (this->usingTransferLearning)
    {
      bool isPresent = false;
      std::vector<std::shared_ptr < TransferMapping>>::iterator mappingIterator = mappings.begin();
      while (mappingIterator != mappings.end())
        {
          if (((*mappingIterator)->getSourceName() == sourceNameIn) && ((*mappingIterator)->getTargetName() == targetNameIn))
            {//is there so load
              isPresent = true;
              (*mappingIterator)->loadMapping(address);
              mappingIterator = mappings.end();
            }
          else
            {
              mappingIterator++;
            }
        }
      if (!isPresent)
        {//if not there add it
          std::cerr << "dwl aganet loadMapping Warning mapping you are trying to load is not there, create it first with Add mapping\n";
        }


    }
  else
    {

      std::cerr << "not using transfer learning in DWLAgent so don't try to\n";
      exit(9867);
    }
  //std::cerr<<"mappings = "<<mappings.size()<<"\n";
}

/**
 * take in the required information to make one end to end mapping
 * @param sourceNameIn agent name and policy name
 * @param tergetNameIn agent name and policy name
 * @param sourceIn the q table
 * @param targetIn the q table
 */
void DWLAgent::addMapping(std::string sourceNameIn, std::string targetNameIn, std::shared_ptr<WTable> sourceIn, std::shared_ptr<WTable> targetIn)
{//std::cerr<<"dwl agent adding a w mapping";
  if (this->usingTransferLearning)
    {
      bool isPresent = false;
      std::vector<std::shared_ptr < TransferMapping>>::iterator mappingIterator = wMappings.begin();
      while (mappingIterator != wMappings.end())
        {
          if (((*mappingIterator)->getSourceName() == sourceNameIn) && ((*mappingIterator)->getTargetName() == targetNameIn))
            {//is there
              isPresent = true;
              mappingIterator = wMappings.end();
            }
          else
            {
              mappingIterator++;
            }
        }
      if (!isPresent)
        {//if not there add it
          std::shared_ptr<TransferMapping> map = std::make_shared< TransferMapping>(); //make it and set the things
          map->setWSource(sourceIn);
          map->setWTarget(targetIn);
          map->setTargetName(targetNameIn);
          map->setSourceName(sourceNameIn);
          map->populateWMappingBySearch(); //TODO replace this with a populateMappingNotRandomly
          this->wMappings.push_back(map);
        }
      else
        {
          std::cerr << "dwl agent add w mappping warning adding an already presnet mapping\n";
        }


    }
  else
    {

      std::cerr << "not using transfer learning in DWLAgent so don't try to\n";
      exit(9867);
    }
  //std::cerr<<"  wmappings = "<<wMappings.size()<<"\n";
}

/**
 * get the transfers for all of the policies reciving from this one
 * @param policyName who to send from
 * @return what to send of form destination <state+action, value>
 */
std::vector<std::pair<std::string, std::pair<std::string, double> > > DWLAgent::transferToAllFromOne(std::string policyName)
{//UNOFFICIlly depricated

  std::vector<std::pair<std::string, std::pair<std::string, double> > > output;

  /*if (this->usingTransferLearning)
                                                                                                                  {
                                                                                                                  std::vector<TransferMapping*>::iterator mappingIterator = mappings.begin();
                                                                                                                  while (mappingIterator != mappings.end())
                                                                                                                  {//for all
                                                                                                                  TransferMapping* test = (*mappingIterator);
                                                                                                                  if (test->getSourceName() == policyName)
                                                                                                                  {//if there is a match
                                                                                                                  std::pair<std::string, std::pair<std::string, double> > toAdd;
                                                                                                                  toAdd.first = test->getTargetName();
                                                                                                                  toAdd.second = TransferMapping::makePairForMapper(test->mapFromStateToTarget(TransferMapping::makePairForMapper(this->findInterestingPair(policyName)))); //set the pair to pass translated to the neighbours space
                                                                                                                  output.push_back(toAdd);
                                                                                                                  mappingIterator++;//mappingIterator=mappings.erase(mappingIterator);//does ++
                                                                                                                  }
                                                                                                                  else
                                                                                                                  {
                                                                                                                  mappingIterator++;
                                                                                                                  }

                                                                                                                  }

                                                                                                                  }
                                                                                                                  else
                                                                                                                  {
                                                                                                                  std::cerr << "not using transfer learning in DWLAgent so don't try to\n";
                                                                                                                  exit(9867);
                                                                                                                  }*/
  return output;
}

/**
 * get the transfers for all of the policies reciving from this one on one agent also get intresting info
 * @param agentName who to send to must be full and be used in form agent+policy
 * @return what to send of form destination <state+action, value>
 */
std::string DWLAgent::transferToOneFromAll(std::string agentName)
{
  //std::cerr << "DWLAgent transfer from all to one "<<agentName<<"\n";
  std::vector < std::pair<std::string, std::pair<std::string, double> > >vectorVersion;
  std::vector < std::pair<std::string, std::pair<std::string, double> > >wVector;
  if (this->usingTransferLearning)
    {

      this->findAllInterestingPairs(vectorVersion);
      this->findAllInterestingWs(wVector);

    }
  else
    {
      std::cerr << "not using transfer learning in DWLAgent so don't try to\n" << std::endl;
      exit(9867);
    }
  std::string output = "";
  std::vector < std::pair<std::string, std::pair<std::string, double> > >::iterator outputIterator = vectorVersion.begin();
  while (outputIterator != vectorVersion.end())
    {

      std::pair<std::string, std::pair<std::string, double> > test = *outputIterator;
      if (test.first.find(agentName + "+") != std::string::npos)
        {//if is there
          std::stringstream ss;
          ss << test.second.second;
          output += "<transfer>";
          output += "<";
          output += test.first;
          output += ">";
          output += "<";
          output += test.second.first;
          output += "><";
          output += ss.str();
          output += ">\n";
        }
      outputIterator++;
    }
  vectorVersion.clear();
  //now add the w transfers to the list
  std::vector < std::pair<std::string, std::pair<std::string, double> > >::iterator wIterator = wVector.begin();
  while (wIterator != wVector.end())
    {
      std::pair<std::string, std::pair<std::string, double> > test = *wIterator;
      std::stringstream ss;
      ss << test.second.second;
      output += "<wtransfer>";
      output += "<";
      output += test.first;
      output += ">";
      output += "<";
      output += test.second.first;
      output += "><";
      output += ss.str();
      output += ">\n";
      wIterator++;
    }
  wVector.clear();
  //std::cerr << "output t all to all " << output << std::endl;

  return output;
}

/**
 * get the transfers for all of the policies reciving from this one also get intresting info
 * @param policyName who to send from
 * @return what to send of form destination <state+action, value>
 */
std::string DWLAgent::transferToAllFromAll()
{
  //std::cerr << "DWLAgent transfer from and too all\n";
  std::vector < std::pair<std::string, std::pair<std::string, double> > >vectorVersion;
  std::vector < std::pair<std::string, std::pair<std::string, double> > >wVector;
  if (this->usingTransferLearning)
    {

      this->findAllInterestingPairs(vectorVersion);
      this->findAllInterestingWs(wVector);

    }
  else
    {
      std::cerr << "not using transfer learning in DWLAgent so don't try to\n" << std::endl;
      exit(9867);
    }
  std::string output = "";
  std::vector < std::pair<std::string, std::pair<std::string, double> > >::iterator outputIterator = vectorVersion.begin();
  while (outputIterator != vectorVersion.end())
    {
      std::pair<std::string, std::pair<std::string, double> > test = *outputIterator;
      std::stringstream ss;
      ss << test.second.second;
      output += "<transfer>";
      output += "<";
      output += test.first;
      output += ">";
      output += "<";
      output += test.second.first;
      output += "><";
      output += ss.str();
      output += ">\n";
      outputIterator++;
    }
  vectorVersion.clear();
  //now add the w transfers to the list
  std::vector < std::pair<std::string, std::pair<std::string, double> > >::iterator wIterator = wVector.begin();
  while (wIterator != wVector.end())
    {
      std::pair<std::string, std::pair<std::string, double> > test = *wIterator;
      std::stringstream ss;
      ss << test.second.second;
      output += "<wtransfer>";
      output += "<";
      output += test.first;
      output += ">";
      output += "<";
      output += test.second.first;
      output += "><";
      output += ss.str();
      output += ">\n";
      wIterator++;
    }
  wVector.clear();
  if (output.size() > 1)
    {
      //std::cerr << "output t all to all " << output << std::endl;
    }
  else
    {
      // std::cerr << "output t all to all is size 0 " << std::endl;
    }

  return output;
}

void DWLAgent::findAllInterestingPairs(std::vector < std::pair<std::string, std::pair<std::string, double> > >& input)
{
  // std::cout << "DWLAgent::findAllInterestingPairs start\n";
  if (this->usingTransferLearning)
    {
      //std::cerr << "a\n";
      std::vector<std::shared_ptr < Policy>>::iterator localIterator = localPolicies.begin();
      while (localIterator != localPolicies.end())
        {//for all locals
          // std::cerr << "b\n";
          std::string policyName = (*localIterator)->getPolicyName();
          std::vector<std::shared_ptr < TransferMapping>>::iterator mappingIterator = mappings.begin();
          while (mappingIterator != mappings.end())
            {//for all
              //  std::cerr << "c\n";
              std::shared_ptr<TransferMapping> test = (*mappingIterator);
              //std::cerr << "mapping's source name " << test->getSourceName() << " actual name= " << policyName << "\n";
              if (test->getSourceName() == policyName)
                {//if there is a match
                  //  std::cerr << "d\n";
                  std::pair<std::string, std::pair<std::string, double> > toAdd;
                  toAdd.first = test->getTargetName();
                  std::vector<std::pair<std::string, double> > vect;

                  if (false && (std::static_pointer_cast<WLearningProcess>(*localIterator))->getBoltzmannTemperature() <= 10)
                    {//if small botz be gready
                      vect = test->choosePairsBasedOnVotes(2, 1, (std::static_pointer_cast<WLearningProcess>(*localIterator))->getQTable()); //TransferMapping::makePairForMapper(test->mapFromStateToTarget(TransferMapping::makePairForMapper(this->findInterestingPair(policyName)))); //set the pair to pass translated to the neighbours space
                    }
                  else
                    {//if big botz dont be gready
                      //vect = test->choosePairsBasedOnVotes(2, 0, (std::static_pointer_cast<WLearningProcess>( ((*localIterator)))->getQTable());
                      vect = TransferMapping::makePairForMapper(test->mapFromStateToTarget(TransferMapping::makePairForMapper(this->findInterestingPair(policyName)))); //set the pair to pass translated to the neighbours space
                    }
                  std::vector<std::pair<std::string, double> >::iterator vectIterator = vect.begin();
                  //std::cerr << "findAllInterestingPairs -policy name= " << policyName << "\n";
                  while (vect.end() != vectIterator)
                    {//add all the pairs
                      //  std::cerr << "e\n";
                      if (((*vectIterator).first.compare("NO MATCH") == 0) || ((*vectIterator).first.compare("NOMATCH") == 0))
                        {//if no match do nothing
                          std::cerr << "in the no match thing\n";
                          exit(99);
                        }
                      else
                        {
                          //std::cerr << "adding an interesting pair\n";
                          toAdd.second = (*vectIterator);
                          input.push_back(toAdd);
                        }
                      vectIterator++;
                    }
                  vect.clear();
                  mappingIterator++; //mappingIterator=mappings.erase(mappingIterator);//does ++
                }
              else
                {
                  mappingIterator++;
                }

            }
          localIterator++;
        }
    }
  else
    {

      std::cerr << "not using transfer learning in DWLAgent so don't try to\n" << std::endl;
      exit(9867);
    }

  //std::cerr << "find all intresting pairs end " << input.size() << "\r\n";
}

void DWLAgent::findAllInterestingWs(std::vector < std::pair<std::string, std::pair<std::string, double> > >& input)
{//std::cerr<<"enter findAllInterestingWs\n";
  if (this->usingTransferLearning)
    {//std::cerr<<"a +++++++++++\n";
      std::vector<std::shared_ptr < Policy>>::iterator localIterator = localPolicies.begin();
      while (localIterator != localPolicies.end())
        {//for all locals
          //std::cerr<<"b ++++++++++++\n";
          std::string policyName = (*localIterator)->getPolicyName();
          std::vector<std::shared_ptr < TransferMapping>>::iterator mappingIterator = wMappings.begin();
          //std::cerr<<wMappings.size()<<" size of mapping\n";
          while (mappingIterator != wMappings.end())
            {//for all
              //std::cerr<<"c ++++++++++++++\n";
              std::shared_ptr<TransferMapping> test = (*mappingIterator);
              if (test->getSourceName() == policyName)
                {//if there is a match
                  //std::cerr<<"d +++++++++++++\n";
                  std::pair<std::string, std::pair<std::string, double> > toAdd;
                  toAdd.first = test->getTargetName();
                  std::vector<std::pair<std::string, double> > vect = test->mapWFromStateToTarget(this->findInterestingW(policyName)); //set the pair to pass translated to the neighbours space
                  std::vector<std::pair<std::string, double> >::iterator vectIterator = vect.begin();
                  //std::cerr<<"findAllInterestingWs -policy anme= "<<policyName<<"\n";
                  while (vect.end() != vectIterator)
                    {//add all the pairs
                      //   std::cerr<<"e +++++++++++++\n";
                      if ((*vectIterator).first.compare("NO MATCH") == 0)
                        {//if no match do nothing
                          //        std::cerr<<"f +++++++++++++++\n";
                        }
                      else
                        {
                          // std::cerr<<"adding an interesting pair\n";
                          toAdd.second = (*vectIterator);
                          input.push_back(toAdd);
                        }
                      vectIterator++;
                    }
                  vect.clear();
                  mappingIterator++; //mappingIterator=mappings.erase(mappingIterator);//does ++
                }
              else
                {
                  mappingIterator++;
                }

            }
          localIterator++;
        }
    }
  else
    {

      std::cerr << "not using transfer learning in DWLAgent so don't try to\n" << std::endl;
      exit(9867);
    }
  //std::cerr << "find all intresting ws end "<<input.size()<<"\r\n";
}

/**
 * find something worth telling my neighbour about
 * @return a pair (state  value)
 */
std::vector<std::pair<std::string, double> > DWLAgent::findInterestingW(std::string policyName)
{//std::cerr<<"findInterestingw ++++++++"<<std::endl;
  std::vector<std::pair<std::string, double> >output;
  if (this->usingTransferLearning)
    {
      std::vector<std::shared_ptr < Policy>>::iterator localIterator = localPolicies.begin();
      while (localIterator != localPolicies.end())
        {//for all
          //std::cerr<<"1++++++++++++++"<<std::endl;
          std::shared_ptr<Policy> test = (*localIterator);
          //std::cerr<<"compare-="<<test->getPolicyName()<<"  "<<policyName<<"\n";
          if (test->getPolicyName() == policyName)//TODO check the name is agent+policy here not agent+agent+policy or just policy
            {//if there is a match
              //	std::cerr<<"2++++++++++++++++"<<std::endl;
              std::shared_ptr<WTable> table = (std::static_pointer_cast<WLearningProcess>(test))->getWTable();
              std::vector<std::string> states = table->getStatesOfHighest(); //table->getStateOfHighestW();//table->getStateOfSomething();////dont use chang in ivanas as qtable nneds tobe chabged betwen policy types//table->getStateOfGreatestChang(lastQTable);
              std::vector<std::string>::iterator statesIterator = states.begin();
              //std::cerr<<"Size states= "<<states.size()<<std::endl;
              while (states.end() != statesIterator)
                {//go through all returned states and get the pairs
                  //	std::cerr<<"3++++++++++++++"<<std::endl;
                  if ((*statesIterator) == "")
                    {//if not a state do nothing
                    }
                  else
                    {
                      //	std::cerr<<"4++++++++++"<<std::endl;
                      //if not using state of highest turn on
                      output.push_back(table->getWTableEntry((*statesIterator))); //TODO this is probably not interesting enough
                      //if trying to competitive transfer
                      //std::pair<std::string,double>temp;
                      //temp.first=(*statesIterator);
                      //temp.second=-999;
                      //output.push_back(temp);//this makes the state bad for other agent
                    }
                  statesIterator++;
                }

              states.clear();

            }
          localIterator++;
        }

    }
  else
    {
      std::cerr << "DWLAgent findInterestingW not using transfer learning in DWLAgent so don't try to\n";
      exit(9867);
    }
  if (output.size() == 0)
    {

      std::cerr << "DWLAgent findInterestingW didn't add anything as could't find " << policyName << " \n";
    }
  //std::cerr<<"found interesting w"<<output.begin()->first<<" "<<output.begin()->second<<" \n";
  return output;
}

/**
 * find something worth telling my neighbour about
 * @return a pair (state (action value))
 */
std::vector<std::pair<std::string, std::pair<std::string, double> > > DWLAgent::findInterestingPair(std::string policyName)
{
  //std::cerr << "findInterestingPair ++++++++" << std::endl;
  std::vector<std::pair<std::string, std::pair<std::string, double> > >output;
  if (this->usingTransferLearning)
    {
      std::vector<std::shared_ptr < Policy>>::iterator localIterator = localPolicies.begin();
      while (localIterator != localPolicies.end())
        {//for all
          std::shared_ptr<Policy> test = (*localIterator);
          if (test->getPolicyName() == policyName)//TODO check the name is agent+policy here not agent+agent+policy or just policy
            {//if there is a match
              std::shared_ptr<QTable> table = (std::static_pointer_cast<WLearningProcess>(test))->getQTable();
              //std::cerr << "1\n";
              std::vector<std::pair<std::string, std::string> > states; //some space
              SelectionType selectMethod = test->getConfig().selection;
              int amount = test->getConfig().amountToTransfer;
              switch (selectMethod)
                {//select the selection
                  case targetedBatchBased:
                    states = table->getStatesOfTargetedBatchBased(amount);
                    break;
                  case greatestChange:
                    states = table->getStateOfGreatestChang(amount);
                    break;
                  case mostVisit:
                    states = table->getStateOfMostVisit(amount);
                    break;
                  case randomSet:
                    states = table->getStateOfRandom(amount);
                    break;
                  case convergedMostVisit:
                    states = table->getStateOfMostVisitConverged(amount);
                    break;
                  case manyVisit:
                    states = table->getStateOfManyVisit(amount);
                    break;
                  case best:
                    states = table->getBestStates(amount);
                    break;
                  case DEFAULT_SELECTION:
                    std::cerr << "DWLAgetn selection got the default.  Make sure you set something\n";
                    exit(8356);
                    break;
                }
              std::vector<std::pair<std::string, std::string> >::iterator statesIterator = states.begin();
              while (states.end() != statesIterator)
                {//go through all returned states and get the pairs
                  if ((*statesIterator).first == "")
                    {//if not a state do nothing
                    }
                  else
                    {
                      output.push_back(table->getQTableEntry((*statesIterator).first, (*statesIterator).second)); //TODO this is probably not interesting enough
                    }
                  statesIterator++;
                }

              states.clear();

              break;
            }
          else
            {
              localIterator++;
            }
        }
      if (localIterator == localPolicies.end())
        {
          std::cerr << "DWLAgent findInterestingPair didn't add anything as could't find " << policyName << " \n";
        }
    }
  else
    {

      std::cerr << "DWLAgent findInterestingPair not using transfer learning in DWLAgent so don't try to\n";
      exit(9867);
    }
  //std::cerr << "found interesting pair " << output.begin()->first << " " << output.begin()->second.first << "  " << output.begin()->second.second << " \n";
  return output;
}

/**
 * find something to tell my neighbour about wether it is worth it or not
 * @return a pair (state (action value))
 */
std::pair<std::string, std::pair<std::string, double> > DWLAgent::findUninterestingPair(std::string policyName)
{
  std::pair<std::string, std::pair<std::string, double> > output;
  if (this->usingTransferLearning)
    {
      std::vector<std::shared_ptr < Policy>>::iterator localIterator = localPolicies.begin();
      while (localIterator != localPolicies.end())
        {//for all
          std::shared_ptr<Policy> test = (*localIterator);
          if (test->getPolicyName() == policyName)//TODO check the name is agent+policy here not agent+agent+policy or just policy
            {//if there is a match
              std::shared_ptr<QTable> table = (std::static_pointer_cast<WLearningProcess>(test))->getQTable();

              table->changeRandomState();
              output = table->getQTableEntry(table->getRandomState(), table->getBestAction(table->getRandomState()).first);
              break;
            }
          else
            {
              localIterator++;
            }
        }
      if (localIterator == localPolicies.end())
        {
          std::cerr << "DWLAgent findInterestingPair didn't add anything as could't find " << policyName << " \n";
        }
    }
  else
    {

      std::cerr << "DWLAgent findInterestingPair not using transfer learning in DWLAgent so don't try to\n";
      exit(9867);
    }

  //std::cerr<<"found interesting pair"<<output.first<<" "<<output.second.first<<"  "<<output.second.second<<" \n";
  return output;
}

/**
 *
 *return total reward ever
 */
double DWLAgent::getHistoricReward()
{

  return historicReward;
}

/**
 * add the string to the classes member
 * @param input
 */
void DWLAgent::readTransferedWInfoIn(std::vector<std::pair<std::string, std::pair<std::string, double> > > input)
{//std::cerr<<"readTransferedWInfoIn size "<<input.size()<<"\n";
  if (this->usingTransferLearning)
    {
      std::vector<std::pair<std::string, std::pair<std::string, double> > >::iterator inputIterator = input.begin();
      while (inputIterator != input.end())
        {//for all pairs
          std::pair<std::string, std::pair<std::string, double> > test = *inputIterator;
          std::vector<std::shared_ptr < Policy>>::iterator localIterator = localPolicies.begin();
          while (localIterator != localPolicies.end())
            {
              std::shared_ptr<Policy> local = *localIterator;
              //std::cerr<<"readTransferedWInfoIn Compare |"<<test.first <<"|  |"<< local->getPolicyName()<<"|\n";
              if (test.first == local->getPolicyName())//TODO as before check anme form
                {//this update belongs to this policy
                  //handle it
                  std::string state = test.second.first;
                  //std::cerr<<"w state recived= "<<state<<" with value "<<test.second.second<<"\n";
                  double oldValue = (std::static_pointer_cast<WLearningProcess>(local))->getWTable()->getWValue(state);
                  double newValue = test.second.second;
                  double outputValue = oldValue / 2 + (newValue) / 2; //newValue/3+(2*oldValue)/3;//oldValue/3+(2*newValue)/3;//newValue;////wmerge

                  (std::static_pointer_cast<WLearningProcess>(local))->setWValue(state, outputValue);
                  localIterator = localPolicies.end(); //end loop
                }
              else
                {
                  localIterator++;
                }
            }
          inputIterator++;
        }
    }
  else
    {

      std::cerr << "not using transfer learning in DWLAgent so don't try to\n";
      exit(9867);
    }
}

/**
 * add the string to the classes member
 * @param input
 */
void DWLAgent::readTransferedInfoIn(std::vector<std::pair<std::string, std::pair<std::string, double> > > input)
{
  //std::cerr << "readTransferedInfoIn (vect) size " << input.size() << "\n";
  if (this->usingTransferLearning)
    {
      std::vector<std::pair<std::string, std::pair<std::string, double> > >::iterator inputIterator = input.begin();
      while (inputIterator != input.end())
        {//for all pairs
          std::pair<std::string, std::pair<std::string, double> > test = *inputIterator;
          std::vector<std::shared_ptr < Policy>>::iterator localIterator = localPolicies.begin();
          while (localIterator != localPolicies.end())
            {
              std::shared_ptr<Policy> local = *localIterator;
              //std::cerr << "readTransferedInfoIn Compare |" << test.first << "|  |" << local->getPolicyName() << "|\n";
              if (test.first == local->getPolicyName())//TODO as before check anme form
                {//this update belongs to this policy
                  //handle it
                  //std::cerr << test.second.first << " =state " << "\n";
                  std::string state = test.second.first;
                  std::string action = state.substr(state.find(":") + 1, (state.length() - (state.find(":") + 1)));
                  state = state.substr(0, state.find(":"));

                  double oldValue = (std::static_pointer_cast<WLearningProcess>(local))->getQTable()->getQValue(state, action);
                  double newValue = test.second.second;
                  double outputValue = newValue;
                  bool goodTransfer = false; //what to feed back
                  //std::cerr << "values - old " << oldValue << " new " << newValue << "\n";
                  //now check my config to see how I should merge
                  MergeType mergeMethod = local->getConfig().merge;
                  if (mergeMethod == MergeType::adaptive)
                    {//if linier conbimation
                      int paramAdaptive = local->getConfig().mergeParam;
                      int visitsToState = (std::static_pointer_cast<WLearningProcess>(local))->getVisitCount(state, action);
                      double oldFactor = std::min(1.0, (((double) visitsToState) / paramAdaptive));
                      double newFactor = 1.0 - oldFactor;
                      outputValue = oldValue * oldFactor + newValue*newFactor;
                      if (newFactor > 0)
                        {
                          goodTransfer = true;
                        }
                      else
                        {
                          goodTransfer = false;
                        }
                    }
                  else if (mergeMethod == MergeType::adaptiveProbabilistic)
                    {//if we dont want the averageing effect
                      double param = local->getConfig().mergeParam / 100;
                      double randDraw = ((double) rand() / (RAND_MAX));
                      if (param < randDraw)
                        {//reject transfer
                          outputValue = oldValue;
                          goodTransfer = true; //cant really say that
                        }
                      else
                        {
                          outputValue = newValue;
                          goodTransfer = false; //cant really say this
                        }
                    }
                  else if (mergeMethod == MergeType::onlySource)
                    {
                      outputValue = oldValue;
                      goodTransfer = false;
                    }
                  else if (mergeMethod == MergeType::onlyTarget)
                    {
                      outputValue = newValue;
                      goodTransfer = true;
                    }
                  else if (mergeMethod == MergeType::shadowCopy)
                    {//make a shadow copy incase thee isn't one then use the recieved value in it

                      if (this->shadowTranferCount == -1)
                        {//someone else has triggered us to evaluate a transfer so make a space and start counting
                          (std::static_pointer_cast<WLearningProcess>(local))->printStateSpace("before shadowing", ".stats");
                          (std::static_pointer_cast<WLearningProcess>(local))->workOnShadowQTable(0);
                          shadowTranferCount = 0; //set to start counting timesteps used
                        }
                      if (shadowTranferCount > local->getConfig().currentShadowEvaluationTime)
                        {//if we have tried this transfer enough accept or rect
                          //WARNING counter per agent no per policy
                          //(std::static_pointer_cast<WLearningProcess>( local)->printStateSpace("before", ".stats");
                          (std::static_pointer_cast<WLearningProcess>(local))->workOnShadowQTable(2);
                          //(std::static_pointer_cast<WLearningProcess>( local)->printStateSpace("after", ".stats");
                          shadowTranferCount = -1; //set have no shadow
                        }
                      if (this->shadowTranferCount == 0)
                        {//only one transfer can go into shadow discard otheres
                          outputValue = newValue;
                          goodTransfer = true;
                        }
                      else
                        {
                          outputValue = oldValue;
                          goodTransfer = false;
                        }
                    }
                  else
                    {
                      std::cerr << "DWLAgent merge data got the default.  Make sure it is initalised somewhere\n";
                      exit(2827);
                    }
                  (std::static_pointer_cast<WLearningProcess>(local))->setQValue(state, action, outputValue);
                  //std::cerr << "merging " << state << " " << action << " to " << outputValue << std::endl;
                  //now set feedback for the mapping
                  std::stringstream ss;
                  ss << state << ":" << action;
                  (std::static_pointer_cast<WLearningProcess>(local))->provideTransferFeedback(ss.str(), goodTransfer);
                  localIterator = localPolicies.end(); //end loop
                  break;
                }
              else
                {
                  localIterator++;
                }
            }

          inputIterator++;
        }
    }
  else
    {

      std::cerr << "not using transfer learning in DWLAgent so don't try to" << std::endl;
      exit(9867);
    }

}

/**
 * make the sting into a vector and give it to add
 * @param input
 */
void DWLAgent::readTransferedInfoIn(std::string input)
{
  //std::cout << "readTransferedInfoIn(string)\n";
  if (usingTransferLearning == true)
    {
      //std::cerr << "about to readTransferedInfoIn(string)\n";
      //std::cerr << input << " =input\n";
      std::string copy = input;
      input.erase(std::remove_if(input.begin(), input.end(), isspace), input.end());
      std::vector<std::pair<std::string, std::pair<std::string, double> > > output;
      std::vector<std::pair<std::string, std::pair<std::string, double> > > wOutput;
      std::string name, stateAction, value, readTransfer = "<transfer>", wTransfer = "<wtransfer>";
      //input=input.substr(15);//lenght of read transfer tag
      int count = 0;
      for (int a = 0; a < input.size(); a++)
        {
          if (input[a] == '<')
            {
              count++;
            }
        }
      //std::cerr<<"number of < = "<<count<<"\n";
      while (count >= 3)//while enough <> for a thing
        {//while at least one tag left
          //std::cerr << "here\n";
          //input = input.substr((input.find(readTransfer) + readTransfer.length()), (input.length()-(input.find(readTransfer) + readTransfer.length())));
          int messageLenght = input.find(">", 0);
          messageLenght = input.find(">", messageLenght + 1);
          messageLenght = input.find(">", messageLenght + 1);
          messageLenght = input.find(">", messageLenght + 1);
          std::string message = input.substr(0, messageLenght);
          //std::cerr<<message<<"=message "<< messageLenght<<"\n";
          //std::cerr<<input<<"=input\n";
          std::string type = message.substr(0, (message.find(">", 0) + 1));
          //std::cerr<<type<<"=type\n";
          message = message.replace(0, type.size(), "");
          //std::cerr<<message<<"=message "<< messageLenght<<"\n";
          if (type == readTransfer)
            {
              name = message.substr(1, message.find(">") - 1);
              message = message.substr(message.find(">") + 1);
              stateAction = message.substr(1, message.find(">") - 1);
              message = message.substr(message.find(">") + 1);
              value = message.substr(1, message.find(">") - 1);

              if (message.length() > 1)
                {
                  message = message.substr(message.find(">") + 1);
                }
              //std::cerr << "reg trans " << name << " " << stateAction << "  " << atof(value.c_str()) << "  " << "\n";
              if (((stateAction.size() < 2) || stateAction.find("NOMATCH") != std::string::npos) || (stateAction.find("NO MATCH") != std::string::npos))
                {//recived an non-translatable pair
                  std::cerr << "recived a non-transferable pair, so discarding " << name << " " << stateAction << "  " << atof(value.c_str()) << "\n";
                }
              else
                {//if it was a corect pair do stuff
                  std::pair<std::string, std::pair<std::string, double> > toAdd;
                  toAdd.first = name;
                  toAdd.second.first = stateAction;
                  toAdd.second.second = atof(value.c_str());
                  output.push_back(toAdd);
                  count = 0;
                }

            }
          else if (type == wTransfer)
            {
              name = message.substr(1, message.find(">") - 1);
              message = message.substr(message.find(">") + 1);
              stateAction = message.substr(1, message.find(">") - 1);
              message = message.substr(message.find(">") + 1);
              value = message.substr(1, message.find(">") - 1);

              if (message.length() > 1)
                {
                  message = message.substr(message.find(">") + 1);
                }
              std::cerr << "w trans " << name << " " << stateAction << "  " << value << "  " << "-w \n";
              std::pair<std::string, std::pair<std::string, double> > toAdd;
              toAdd.first = name;
              toAdd.second.first = stateAction;
              toAdd.second.second = atof(value.c_str());
              wOutput.push_back(toAdd);
              count = 0;

            }
          else
            {
              //std::cerr << "DWLAgent+readTransferedInfoIn not a <transfer> or <wTransfer> instead " << type << "\n";
            }

          input = input.replace(0, messageLenght + 1, ""); //delet the one we just had plus>
          //td::cerr << input << "=input\n";
          count = 0;
          for (int a = 0; a < input.size(); a++)
            {
              if (input[a] == '<')
                {
                  count++;
                }
            }

        }
      //std::cerr << output.size() << " = size\n";
      if (output.size() > 0)
        {
          this->readTransferedInfoIn(output);
        }
      //output.clear();//shouldnt need

      if (wOutput.size() > 0)
        {
          this->readTransferedWInfoIn(wOutput);
        }
      //wOutput.clear();
      //std::cerr << "finished reading tl in " << std::endl;
    }
  else
    {

      std::cerr << "not using TL dont try\n";
      exit(3525);
    }
}

int DWLAgent::getNumberOfMappings()
{

  return mappings.size();
}

/**
 *turn on or off lerning
 */
void DWLAgent::manageLearning(bool learnQ, bool learnW)
{
  learningQ = learnQ;
  learningW = learnW;
  std::vector<std::shared_ptr < Policy>>::iterator localIterator = localPolicies.begin();
  while (localIterator != localPolicies.end())
    {//put locals into sugestion vector and see best
      std::shared_ptr<Policy> test = *localIterator;
      (std::static_pointer_cast<WLearningProcess>(test))->setLearningQ(learnQ);
      (std::static_pointer_cast<WLearningProcess>(test))->setLearningW(learnW);
      localIterator++;
    }
  std::vector<std::shared_ptr < Policy>>::iterator remoteIterator = remotePolicies.begin();
  while (remoteIterator != remotePolicies.end())
    {//put locals into sugestion vector and see best

      std::shared_ptr<Policy> test = *remoteIterator;
      (std::static_pointer_cast<WLearningProcess>(test))->setLearningQ(learnQ);
      (std::static_pointer_cast<WLearningProcess>(test))->setLearningW(learnW);
      remoteIterator++;
    }
}

void DWLAgent::changeActionSelectionTemperature(int input)
{
  std::vector<std::shared_ptr < Policy>>::iterator localIterator = localPolicies.begin();
  while (localIterator != localPolicies.end())
    {//put locals into sugestion vector and see best

      std::shared_ptr<Policy> test = *localIterator;
      (std::static_pointer_cast<WLearningProcess>(test))->setBoltzmannTemperature(input);
      (std::static_pointer_cast<WLearningProcess>(test))->setTaylorTemperature(input);
      localIterator++;
    }//TODO add remotes too
}

void DWLAgent::changeAlphaGamma(double alpha, double gamma)
{
  std::vector<std::shared_ptr < Policy>>::iterator localIterator = localPolicies.begin();
  while (localIterator != localPolicies.end())
    {//put locals into sugestion vector and see best

      std::shared_ptr<Policy> test = *localIterator;
      (std::static_pointer_cast<WLearningProcess>(test))->setQAlpha(alpha);
      (std::static_pointer_cast<WLearningProcess>(test))->setQGamma(gamma);
      localIterator++;
    }//TODO add remotes too
}

/**
 * egready must be turned on for this to have any effect it is off by default
 * @param input the e value 0-1
 */
void DWLAgent::changeEGreadyE(double input)
{
  if (input > 1 || input < 0)
    {//do nothing if wrong e
      std::cerr << "Wrong e in DWLAgent change EGready e\n";
    }
  else
    {
      std::vector<std::shared_ptr < Policy>>::iterator localIterator = localPolicies.begin();
      while (localIterator != localPolicies.end())
        {//put locals into sugestion vector and see best

          std::shared_ptr<Policy> test = *localIterator;
          (std::static_pointer_cast<WLearningProcess>(test))->setEGreadyE(input);
          localIterator++;
        }//TODO add remotes too
    }
}

/**
 * choose how to select actions, only one at a time please
 * @param boltzmann ture is wanted on
 * @param eGready true is wanted on
 */
void DWLAgent::chooseActionSelectionMethod(bool boltzmann, bool eGready, bool taylor)
{
  if ((boltzmann + eGready + taylor) > 1)
    {//do nothing if none or both action selections
      std::cerr << "can t use more than 1 action selection\n";
    }
  else
    {
      std::vector<std::shared_ptr < Policy>>::iterator localIterator = localPolicies.begin();
      while (localIterator != localPolicies.end())
        {//all locase need to be changed

          std::shared_ptr<Policy> test = *localIterator;
          (std::static_pointer_cast<WLearningProcess>(test))->setUseBoltzmann(boltzmann);
          (std::static_pointer_cast<WLearningProcess>(test))->setUseEGready(eGready);
          (std::static_pointer_cast<WLearningProcess>(test))->setUseTaylor(taylor);
          localIterator++;
        }//TODO add remotes too
    }
}

std::vector<std::shared_ptr<QTable>> DWLAgent::getLocalQTables()
{
  std::vector<std::shared_ptr < QTable>> output;
  std::vector<std::shared_ptr < Policy>>::iterator localIterator = localPolicies.begin();
  while (localIterator != localPolicies.end())
    {//all locase need to be changed

      std::shared_ptr<Policy> test = *localIterator;
      output.push_back((std::static_pointer_cast<WLearningProcess>(test))->getQTable());
      localIterator++;
    }
  return output;
}

/**
 * if renewing an agent for explitation 0 the reward and the history of it
 */
void DWLAgent::clearReward()
{

  rewardLog.clear();
  rewardLog.shrink_to_fit();
  totalReward = 0;
  historicReward = 0;
}

/**
 *write reward to file
 * input is the bit after name
 */
void DWLAgent::printReward(std::string input, std::string tag)
{
  //std::cerr << "start print mappings" << std::endl;
  std::string filename = input + "-reward.txt." + tag + ".stats";
  //std::cerr << "writing " << filename << "\n";
  std::ofstream outputfile(filename.c_str());

  if (outputfile.is_open())
    {//std::cerr<<rewardLog.size() <<"= log size\n";
      std::vector<std::string>::iterator rewardIterator = rewardLog.begin();
      while (rewardIterator != rewardLog.end())
        {
          //std::cerr << (*rewardIterator) << "\n";
          outputfile << (*rewardIterator) << ",";
          rewardIterator++;
        }
      outputfile << "\r\n"; //eof
      outputfile.close();
    }
  else
    {

      std::cerr << "\nQtable->writeStateActionToFile Unable to open file\n";
      exit(89);
    }
  //std::cerr << "Print reward done!\n";
}

/**
print the mappings
mode =0 back and forwards, 1=source first -1 = target first*/
void DWLAgent::printMappings(std::string filename, int mode, std::string tag)
{

  std::vector<std::shared_ptr < TransferMapping>>::iterator mappingIterator = mappings.begin();
  while (mappingIterator != mappings.end())
    {//fort all mappings
      if (mode <= 0)
        {//target first
          (*mappingIterator)->printMappingTargetFirst(filename, tag);
        }
      if (mode >= 0)
        {//source first
          (*mappingIterator)->printMappingSourceFirst(filename, tag);
        }
      mappingIterator++;
    }
  // std::cerr << "end print mappings\n";
}

/**
 * go through all mappings and update them based on feed back
 * @param sourceStateActionToUpdate
 * @param targetStateToUpdate
 * @param good
 */
void DWLAgent::updateLearnedMappingFromTarget()
{
  std::vector<std::shared_ptr < TransferMapping>>::iterator mappingIterator = mappings.begin();
  while (mappingIterator != mappings.end())
    {//fort all mappings
      //get this mappings target
      std::vector<std::shared_ptr < Policy>>::iterator localIterator = localPolicies.begin();
      while (localIterator != localPolicies.end())//TODO check only local
        {//all locase need to be changed
          std::shared_ptr<Policy> test = *localIterator;
          if (test->getPolicyName() == (*mappingIterator)->getTargetName())//TODO check correct name here
            {//found correct policy
              //get its state that it recieved and didnt/did like
              std::vector < std::pair < std::string, bool> > feedback = (std::static_pointer_cast<WLearningProcess>(test))->getTransferFeedback();
              //give it to the mapping to respond to
              std::vector < std::pair < std::string, bool> >::iterator feedbackIterator = feedback.begin();
              while (feedbackIterator != feedback.end())
                {
                  (

                          *mappingIterator)->updateLearnedMappingFromTarget((*feedbackIterator).first, (*feedbackIterator).second);
                  feedbackIterator++;
                }
              break;

            }
          localIterator++;
        }

      //feed back to mapping


      mappingIterator++;
    }
}

std::string DWLAgent::sendFeedback()
{
  std::string output;
  std::vector<std::shared_ptr < Policy>>::iterator localIterator = localPolicies.begin();
  while (localIterator != localPolicies.end())//TODO check only local
    {//for all local policies
      std::vector < std::pair < std::string, bool> > feedback = (std::static_pointer_cast<WLearningProcess>(*localIterator))->getTransferFeedback();
      //std::cerr << "sending " << feedback.size() << " items of feedback" << std::endl;
      std::vector < std::pair < std::string, bool> >::iterator feedbackIterator = feedback.begin();
      while (feedbackIterator != feedback.end())
        {//for all feedback make a string version

          output += "<feedback>";
          output += "<";
          output += (*localIterator)->getPolicyName();
          output += ">";
          output += "<";
          output += (*feedbackIterator).first;
          output += "><";
          std::stringstream ss;
          ss << (*feedbackIterator).second;
          output += ss.str();
          output += ">\n";
          feedbackIterator++;

        }
      localIterator++;
    }

  return output;
}

/**
 * turn the feedback into an effect on mapping
 * @param
 */
void DWLAgent::recieveFeedback(std::string input)
{
  if (input.find("feedback") == std::string::npos)
    {
      std::cerr << "DWLAgent+recieveFeedback not a feedback instead " << input.substr(0, input.find_first_of(">")) << " Did you populate a learned mapping\n";
    }
  else
    {
      int count = 0;
      for (int a = 0; a < input.size(); a++)
        {
          if (input[a] == '<')
            {
              count++;
            }
        }

      while (count >= 3)//while enough <> for a thing
        {//while at least one tag left
          int messageLenght = input.find(">", 0);
          messageLenght = input.find(">", messageLenght + 1);
          messageLenght = input.find(">", messageLenght + 1);
          messageLenght = input.find(">", messageLenght + 1);
          std::string message = input.substr(0, messageLenght);
          //std::cerr << message << "=message " << messageLenght << "\n";
          //std::cerr << input << "=input\n";
          std::string type = message.substr(0, (message.find(">", 0) + 1));
          //std::cerr << type << "=type\n";
          message = message.replace(0, type.size(), "");
          // std::cerr << message << "=message " << messageLenght << "\n";

          if (type == "<feedback>")
            {
              std::string target = message.substr(1, message.find(">") - 1);
              message = message.substr(message.find(">") + 1);
              std::string stateAction = message.substr(1, message.find(">") - 1);
              message = message.substr(message.find(">") + 1);
              std::string value = message.substr(1, message.find(">") - 1);

              if (message.length() > 1)
                {
                  message = message.substr(message.find(">") + 1);
                }
              //std::cerr << "recieved from transfer partner " << target << " " << stateAction << "  " << value << "  " << "\n";
              //now we have msg parse it and apply it
              std::vector<std::shared_ptr < TransferMapping>>::iterator mappingIterator = mappings.begin();
              while (mappingIterator != mappings.end())
                {//fort all mappings
                  //std::cerr << "dwlagent recieveFeedback|" << (*mappingIterator)->getTargetName() << "| and the target= |" << target << "|" << std::endl;
                  if ((*mappingIterator)->getTargetName() == target)
                    {//see it the update is from this mappings target
                      //std::cerr << "found mapping" << std::endl;
                      //found our mapping
                      (*mappingIterator)->updateLearnedMappingFromTarget(stateAction, (bool)atoi(value.c_str()));
                      break;
                    }
                  mappingIterator++;
                }
            }
          input = input.replace(0, messageLenght + 1, ""); //delet the one we just had plus>
          //std::cerr<<input<<"=input\n";
          count = 0;
          for (int a = 0; a < input.size(); a++)
            {
              if (input[a] == '<')
                {

                  count++;
                }
            }

        }
    }

}

std::vector<std::string> DWLAgent::getLocalPolicyNames()
{
  std::vector<std::string> output;
  std::vector<std::shared_ptr < Policy>>::iterator localIterator = localPolicies.begin();
  while (localIterator != localPolicies.end())//TODO check only local
    {//for all local policies
      output.push_back((*localIterator)->getPolicyName());
      localIterator++;
    }
  return output;
}