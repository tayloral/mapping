/*
 * File:   QTable.cpp
 * Author: Adam
 *
 * Created on October 8, 2012, 10:11 AM
 */

#include <map>
#include <string>
#include <vector>
#include "QTable.h"
#include "Cusum.h"
#include <iostream>
#include <cmath>
#include <stdlib.h>
#include <fstream>
#include <cstring>
#include <limits>
#include <algorithm>
#include <math.h>

QTable::QTable()
{
  randomState = "";
  masterCusum = std::make_shared<Cusum>();
  masterCusum->SetAlpha(0.0027); //init it just incase I dont
  masterCusum->SetBeta(0.01);
  masterCusum->SetDelta(1);
  rollingAverageReward = 0;
  numberOfRewardSamples = 0;
  this->mostRecentStatesTraceTransferable = false;
  usingShadowTable = false;
}

void QTable::setGamma(double gamma)
{
  if (gamma < 0 || gamma > 1)
    {
      std::cerr << "in Qtable->setgamma out of range\n";
      exit(99);
    }
  this->gamma = gamma;
}

double QTable::getGamma() const
{
  return gamma;
}

void QTable::setAlpha(double alpha)
{
  if (alpha < 0 || alpha > 1)
    {
      std::cerr << "in Qtable->setalpha out of range\n";
      exit(99);
    }
  this->alpha = alpha;
}

double QTable::getAlpha() const
{
  return alpha;
}

std::multimap<std::string, std::pair<std::string, double> > QTable::getQTable() const
{
  return qTable;
}

QTable::~QTable()
{
  qTable.clear();
  visitCount.clear();
  stateDiverges.clear();
  converged.clear();
  qTablePrevious.clear();
  std::multimap<std::string, std::pair<std::string, double> > emptyMultimapSSD;
  std::multimap < std::string, std::pair < std::string, bool> > emptyMultimapSSB;
  std::multimap<std::string, std::pair<std::string, int> > emptyMultimapSSI;
  std::multimap<std::string, std::pair<std::string, std::shared_ptr < Cusum>> > emptyMultimapSSC;
  qTable.swap(emptyMultimapSSD);
  visitCount.swap(emptyMultimapSSI);
  stateDiverges.swap(emptyMultimapSSC);
  converged.swap(emptyMultimapSSB);
  qTablePrevious.swap(emptyMultimapSSD);
}

/**
 * put in the new state
 * @param stateName its name
 * @param actionName its name
 * @param value how it should start
 */
void QTable::addStateAction(std::string stateName, std::string actionName, double value)
{
  //std::cerr << "adding "<<stateName<<" : "<<actionName << "\n";
  if (qTable.size() == 0 || randomState.length() < 5)
    {
      randomState = stateName;
    }
  //see if this pair is there already
  std::pair< std::multimap< std::string, std::pair< std::string, double > >::iterator, std::multimap< std::string, std::pair< std::string, double > >::iterator > stateActionsRange = qTable.equal_range(stateName); //get first and last of the actions in that state
  std::multimap< std::string, std::pair< std::string, double> >::iterator it;
  bool valueSet = false;
  if ((stateActionsRange.first) != qTable.end())//&& (stateActionsRange.second) != qTable.end())//the second condition is not nessicary as find can ligitimatly give the end
    {
      for (it = (stateActionsRange.first); it != (stateActionsRange.second); it++)
        {//go through all actions in this state and see if there is a match
          if ((*it).second.first.compare(actionName) == 0)
            {//if they match update the q
              (*it).second.second = value;
              //std::cerr << "in Qtable->addstateActin found state there already= " << stateName << " and action= " << actionName << ", added vakue anyway\n";
              valueSet = true;
              break; //it = stateActionsRange.second;
            }

        }
    }
  if (valueSet == false)
    {//if there was no state action by this name already
      std::pair<std::string, double> sub = std::pair<std::string, double>(actionName, value);
      std::pair<std::string, std::pair<std::string, double> > main = std::pair<std::string, std::pair<std::string, double> >(stateName, sub);
      //ADDED for transfer learning action visit count
      std::pair<std::string, int> sub2 = std::pair<std::string, int>(actionName, 0);
      std::pair<std::string, std::pair<std::string, int> > main2 = std::pair<std::string, std::pair<std::string, int> >(stateName, sub2);
      visitCount.insert(main2);
      std::pair < std::string, bool> sub3 = std::pair < std::string, bool>(actionName, false);
      std::pair < std::string, std::pair < std::string, bool> > main3 = std::pair < std::string, std::pair < std::string, bool> >(stateName, sub3);
      converged.insert(main3);
      std::pair < std::string, std::shared_ptr < Cusum>> sub4 = std::pair < std::string, std::shared_ptr < Cusum >> (actionName, masterCusum->deepCopy()); //give this its own version of cusum
      std::pair < std::string, std::pair < std::string, std::shared_ptr<Cusum> >> main4 = std::pair < std::string, std::pair < std::string, std::shared_ptr < Cusum>> >(stateName, sub4);
      stateDiverges.insert(main4);
      //END
      qTable.insert(main);
    }

  this->qTablePrevious.clear();
  qTablePrevious.insert(qTable.begin(), qTable.end());
}

/**
 * destroys the model that is already there
 * @param input
 */
void QTable::newMasterCusum(std::shared_ptr<Cusum> input)
{
  //delete masterCusum; //no more old one
  masterCusum = input; //shiny and new
  //now populate this to states
  //update cusum

  std::multimap < std::string, std::pair < std::string, std::shared_ptr<Cusum> > >::iterator cusumRange = stateDiverges.begin(); //get first and last of the actions in that state
  while (cusumRange != stateDiverges.end())
    {//go through all actions in this state and replace cusum
      std::pair < std::string, std::pair < std::string, std::shared_ptr<Cusum> > >toUpdate = (*cusumRange);
      //  delete toUpdate.second.second;
      toUpdate.second.second = masterCusum->deepCopy(); //now the new one
      cusumRange++;
    }

}

void QTable::changeRandomState()
{
  std::multimap<std::string, std::pair<std::string, double> >::iterator qIterator = qTable.begin();
  int a = rand() % qTable.size(), b = 0;
  while (a > b)
    {
      qIterator++;
      b++;
    }
  randomState = (*qIterator).first;
}

/**
 * change the value
 * @param stateName
 * @param actionName
 * @param input what to change it to
 */
void QTable::setQValue(std::string stateName, std::string actionName, double input)
{
  std::pair< std::multimap< std::string, std::pair< std::string, double > >::iterator, std::multimap< std::string, std::pair< std::string, double > >::iterator > stateActionsRange = qTable.equal_range(stateName); //get first and last of the actions in that state
  std::multimap< std::string, std::pair< std::string, double> >::iterator it;
  bool valueSet = false;
  if ((stateActionsRange.first) != qTable.end())//&& (stateActionsRange.second) != qTable.end())//the second condition is not nessicary as find can ligitimatly give the end
    {
      for (it = (stateActionsRange.first); it != (stateActionsRange.second); it++)
        {//go through all actions in this state and see if there is a match
          if ((*it).second.first.compare(actionName) == 0)
            {//if they match update the q
              (*it).second.second = input;

              valueSet = true;
              break; //it = stateActionsRange.second;
            }

        }
    }
  if (valueSet == false)
    {
      std::cerr << "in Qtable->setQValue failed to find state= " << stateName << " and action= " << actionName << "\n";
      exit(11);
    }
}

double QTable::getQValue(std::string stateName, std::string actionName)
{
  std::pair<std::multimap<std::string, std::pair<std::string, double> >::iterator, std::multimap<std::string, std::pair<std::string, double> >::iterator> stateActionsRange = qTable.equal_range(stateName); //get first and last of the actions in that state
  std::multimap<std::string, std::pair<std::string, double> >::iterator it;
  for (it = stateActionsRange.first; it != stateActionsRange.second; it++)
    {//go through all actions in this state and see if there is a match
      if ((*it).second.first.compare(actionName) == 0)
        {//if they match return the q
          return (*it).second.second;
        }
    }
  this->writeStateActionToFile("error", "error");
  std::cerr << "in Qtable->getQValue failed to find state= " << stateName << " and action= " << actionName << "\n";
  exit(10);
  return -99999;
}

/*
 *return a not random state
 */
std::string QTable::getRandomState()
{
  return randomState;
}

/**
 * get what can be done in one state
 * @param stateName
 * @return the actions and their values
 */
std::vector<std::pair<std::string, double> > QTable::getActionsFromState(std::string stateName)
{
  std::vector<std::pair<std::string, double> > output;
  std::pair<std::multimap<std::string, std::pair<std::string, double> >::iterator, std::multimap<std::string, std::pair<std::string, double> >::iterator> stateActionsRange = qTable.equal_range(stateName); //get first and last of the actions in that state
  std::multimap<std::string, std::pair<std::string, double> >::iterator it;
  for (it = stateActionsRange.first; it != stateActionsRange.second; it++)
    {//go through all actions and add them
      output.push_back(std::pair<std::string, double>((*it).second.first, (*it).second.second));
    }
  if (output.size() == 0)
    {
      writeStateActionToFile("error", "error");
      std::cerr << "Warning QTable returned a state with zero actions.  It was " << stateName << ".\n";
    }
  //std::cerr<<"number of actions in qtable= "<<output.size()<<"\n";
  return output;
}

/**
 * write a file with the q table in it
 * @param filename
 */
void QTable::writeStateActionToFile(std::string filenameIn, std::string tag)
{
  std::string filename;
  if (tag == "error" || tag == "")
    {//if erro no tag
      filename = filenameIn + "-q.txt.stats";
    }
  else if (tag == "plain")
    {//if erro no tag
      filename = filenameIn + "-q.txt";
    }
  else
    {//has a tag
      filename = filenameIn + "-q.txt." + tag + ".stats";
    }

  //std::cerr << "writing " << filename << "\n";
  std::ofstream outputfile(filename.c_str());
  //std::cerr<<filename<<"\n";
  if (outputfile.is_open())
    {
      std::multimap<std::string, std::pair<std::string, double> >::iterator qTableIterator = qTable.begin();
      while (qTableIterator != qTable.end())
        {
          outputfile << (*qTableIterator).first << "^^^^" << (*qTableIterator).second.first << "~~~~" << (*qTableIterator).second.second << "\r\n";
          qTableIterator++;
        }
      outputfile << ";;;;;;;;;;"; //eof
      outputfile.close();
    }
  else
    {
      std::cerr << "\nQtable->writeStateActionToFile Unable to open file\n";
      exit(89);
    }
  //std::cerr << "done!" << "\n";
}

/**
 * for all states set vist count to zero and converged to false.  this allows them to relearn/ get transfers more agressivelty.
 * @param onlyChangedStates true will only do so for changed states, false will do so for all reporting outOfControl from cusum
 */
void QTable::resetVisitCount(bool onlyChangedStates)
{
  std::multimap < std::string, std::pair < std::string, std::shared_ptr<Cusum> > >::iterator cusumRange = stateDiverges.begin(); //get first and last of the actions in that state
  while (cusumRange != stateDiverges.end())
    {//go through all actions in this state and check cusum
      std::pair < std::string, std::pair < std::string, std::shared_ptr<Cusum> > >toUpdate = (*cusumRange);
      bool toChange = true;
      if (onlyChangedStates)
        {//if selective
          if (toUpdate.second.second->getLastOutput() == 0)
            {// dont change
              toChange = false;
            }
        }
      if (toChange)
        {//actually change
          //remove cusums model
          toUpdate.second.second->resetModel();
          //get correct state from visit
          std::multimap< std::string, std::pair< std::string, int> >::iterator it1;
          std::pair< std::multimap< std::string, std::pair< std::string, int > >::iterator, std::multimap< std::string, std::pair< std::string, int > >::iterator > visitCountRange = visitCount.equal_range(toUpdate.first); //get first and last of the actions in that state
          for (it1 = (visitCountRange.first); it1 != (visitCountRange.second); it1++)
            {//go through all actions in this state and see if there is a match
              if ((*it1).second.first.compare(toUpdate.second.first) == 0)
                {//matched
                  (*it1).second.second = 0; //0 visit count
                }
            }
          //get correct from converged
          std::multimap < std::string, std::pair < std::string, bool> >::iterator it2;
          std::pair < std::multimap < std::string, std::pair < std::string, bool > >::iterator, std::multimap < std::string, std::pair < std::string, bool > >::iterator > convergedRange = converged.equal_range(toUpdate.first); //get first and last of the actions in that state
          for (it2 = (convergedRange.first); it2 != (convergedRange.second); it2++)
            {//go through all actions in this state and see if there is a match
              if ((*it2).second.first.compare(toUpdate.second.first) == 0)
                {//matched not converged
                  (*it2).second.second = false;
                }
            }

        }
      cusumRange++;
    }
}

/**
 * perform the q learning update based on the current alpha and gamma
 * @param previousStateName the state we were in
 * @param actionName the action taken
 * @param currentStateName the state we were in
 * @param value the reward recived for above
 */
void QTable::qLearningUpdate(std::string previousStateName, std::string actionName, std::string currentStateName, double reward)
{
  //make a copy of the last q table for diffing in transfer
  this->qTablePrevious.clear();
  qTablePrevious.insert(qTable.begin(), qTable.end());
  //add to the trace queue for transfer
  mostRecentStatesTrace.push_back(std::pair<std::string, std::string>(currentStateName, actionName));
  if (numberOfRewardSamples > 0)
    {//calculate the approximate average Wellford's Method
      numberOfRewardSamples++;
      //m1=x1...mk=mk-1+(xk-mk-1)/k
      double oldMean = rollingAverageReward;
      this->rollingAverageReward += (reward - this->rollingAverageReward) / numberOfRewardSamples;
      //Sk = Sk-1 + (xk – Mk-1)*(xk – Mk)
      this->rollingVarienceReward = (reward - oldMean)*(reward - rollingAverageReward) / (numberOfRewardSamples - 1);
    }
  else
    {
      rollingVarienceReward = 0;
      this->rollingAverageReward += reward;
      numberOfRewardSamples++;
    }

  if (numberOfRewardSamples > 20 && reward > (rollingAverageReward + std::sqrt(rollingVarienceReward) * 1))
    {//if not to soon and reward is good set transferable
      //std::cout << "Triggered transfer Reward is: " << reward << " and average: " << rollingAverageReward << " varience is: " << rollingVarienceReward << "\n";
      this->mostRecentStatesTraceTransferable = true;
    }

  // std::cerr << " in q update " << previousStateName << "  " << actionName << "  " << currentStateName << "  " << reward << "\n";
  //find state to update
  std::pair<std::multimap<std::string, std::pair<std::string, double> >::iterator, std::multimap<std::string, std::pair<std::string, double> >::iterator> stateActionsRange = qTable.equal_range(previousStateName); //get first and last of the actions in that state
  std::multimap<std::string, std::pair<std::string, double> >::iterator it;
  bool foundMatch = false;
  it = stateActionsRange.first;

  while (it != stateActionsRange.second)
    {//go through all actions in this state and see if there is a match
      if ((*it).second.first.compare(actionName) == 0)
        {//if they match do the update
          // std::cerr << "Qlearning - " << (*it).first << " + " << (*it).second.first << "'s value " << (*it).second.second;
          //q new =                        q old +                         alpha (reward +gamma (max next action)                                                    - q old)
          //ADDED FOR EXP
          double oldValue = (*it).second.second;
          //end
          (*it).second.second = (*it).second.second + alpha * (reward + gamma * (this->getBestAction(currentStateName).second) - (*it).second.second);

          //ADDED FOR EXP
          double newValue = (*it).second.second;
          //end
          //std::cerr << " to " << (*it).second.second << " alpha= " << alpha << " gamma= " << gamma << " reward= " << reward << " qmax+1= " << (this->getBestAction(currentStateName).second) << "\n";

          it = stateActionsRange.second; //end loop if found
          foundMatch = true;
          //ADDED FOR EXP
          std::multimap< std::string, std::pair< std::string, int> >::iterator it1;
          std::pair< std::multimap< std::string, std::pair< std::string, int > >::iterator, std::multimap< std::string, std::pair< std::string, int > >::iterator > visitCountRange = visitCount.equal_range(previousStateName); //get first and last of the actions in that state
          for (it1 = (visitCountRange.first); it1 != (visitCountRange.second); it1++)
            {//go through all actions in this state and see if there is a match
              if ((*it1).second.first.compare(actionName) == 0)
                {//matched
                  (*it1).second.second++; //add to visit count
                }
            }
          //END
          //ADDED FOR EXP
          if (abs(oldValue - newValue) < std::max(1.0, newValue / 100))//formerly in this but will prevent no reward states converging && reward > 3)
            {//if converged
              std::multimap < std::string, std::pair < std::string, bool> >::iterator it2;
              std::pair < std::multimap < std::string, std::pair < std::string, bool > >::iterator, std::multimap < std::string, std::pair < std::string, bool > >::iterator > convergedRange = converged.equal_range(previousStateName); //get first and last of the actions in that state
              for (it2 = (convergedRange.first); it2 != (convergedRange.second); it2++)
                {//go through all actions in this state and see if there is a match
                  if ((*it2).second.first.compare(actionName) == 0)
                    {//matched
                      (*it2).second.second = true;
                    }
                }
            }
          //update cusum
          std::multimap < std::string, std::pair < std::string, std::shared_ptr < Cusum>> >::iterator it3;
          std::pair < std::multimap < std::string, std::pair < std::string, std::shared_ptr<Cusum> > >::iterator, std::multimap < std::string, std::pair < std::string, std::shared_ptr<Cusum> > >::iterator > cusumRange = stateDiverges.equal_range(previousStateName); //get first and last of the actions in that state
          for (it3 = (cusumRange.first); it3 != (cusumRange.second); it3++)
            {//go through all actions in this state and see if there is a match
              if ((*it3).second.first.compare(actionName) == 0)
                {//matched
                  (*it3).second.second->calculateEnvironmnetState(reward);
                  //std::cerr << "Cusum for state " << previousStateName << " and action " << actionName << " is " << (*it3).second.second->calculateEnvironmnetState(reward) << "\n"; //see is the reward for this state has deviated too much from the model
                  //std::cerr << "with mean= " << (*it3).second.second->GetMean() << " and stddev= " << (*it3).second.second->GetStdDev() << "\n";
                }
            }
          //END
          //std::cerr<<" learning!!!\n";
        }
      else
        {
          it++;
        }
    }
  if (foundMatch == false)
    {//if there was no match
      std::cerr << "in Qtable->qLearningUpdate failed to find state= " << previousStateName << " and action= " << actionName << "\n";
      exit(10);
    }

}

/**
 * return the best action in the passed state
 * @param stateName
 * @return
 */
std::pair<std::string, double> QTable::getBestAction(std::string stateName)
{
  std::pair<std::string, double> output;
  std::pair<std::multimap<std::string, std::pair<std::string, double> >::iterator, std::multimap<std::string, std::pair<std::string, double> >::iterator> stateActionsRange = qTable.equal_range(stateName); //get first and last of the actions in that state
  std::multimap<std::string, std::pair<std::string, double> >::iterator it;
  if (stateActionsRange.first != qTable.end())
    {
      for (it = stateActionsRange.first; it != stateActionsRange.second; it++)
        {//go through all actions and add them
          if (it == stateActionsRange.first)
            {//if the first one then it is best
              output = (*it).second;
            }
          else
            {
              if (output.second < (*it).second.second)
                {//this one is better
                  output = (*it).second;
                }
            }
        }
    }
  else
    {//badness
      std::cerr << "couldn't find " << stateName << " to get actions from\n";
      exit(72834729);
    }
  return output;
}

/**
 * read from the output of write
 * @param filenameIn
 */
void QTable::readStateActionFromFile(std::string filenameIn)
{
  //std::cerr << "in readStateActionFromFile\n";
  std::string line;
  filenameIn += ".txt";
  std::ifstream myfile(filenameIn.c_str());
  if (myfile.is_open())
    {
      getline(myfile, line);
      while (myfile.good())
        {//while there is stuff to get, get it split it and put it into model
          if (line != ";;;;;;;;;;")
            {//stop it trying to segment eof
              std::string backup = line;
              std::string state = line.substr(0, line.find("^^^^"));
              if (state.length() > 5 && randomState == "")
                {
                  randomState = state; //std::cerr<<"setting random to "<<randomState<<"\n";
                }
              line = backup;
              std::string action = line.substr(line.find("^^^^") + 4);
              action = action.substr(0, action.find("~~~~"));
              line = backup;
              std::string value = line.substr(line.find("~~~~") + 4);
              this->addStateAction(state, action, atof(value.c_str())); //put it in
              getline(myfile, line);
            }
          else
            {//if at my end of file note
              myfile.close();
            }
        }
      myfile.close();
    }
  else
    {
      std::cerr << "QTable  readStateActionFromFile Unable to open file:" << filenameIn << "\n";
      //exit(89);
    }
  //std::cerr<<"end readStateActionFromFile\n";
}

/**
 * get a q table state and action
 * @param state
 * @param action
 * @return
 */
std::pair<std::string, std::pair<std::string, double> > QTable::getQTableEntry(std::string state, std::string action)
{
  //find state to update
  std::pair<std::multimap<std::string, std::pair<std::string, double> >::iterator, std::multimap<std::string, std::pair<std::string, double> >::iterator> stateActionsRange = qTable.equal_range(state); //get first and last of the actions in that state
  std::multimap<std::string, std::pair<std::string, double> >::iterator it;
  bool foundMatch = false;
  it = stateActionsRange.first;

  while (it != stateActionsRange.second)
    {//go through all actions in this state and see if there is a match
      if ((*it).second.first.compare(action) == 0)
        {//if they match
          return (*it);
        }
      else
        {
          it++;
        }
    }
  if (foundMatch == false)
    {//if there was no match
      std::cerr << "in Qtable->getQTableEntry failed to find state= " << state << " and action= " << action << "\n";
      exit(10);
    }
  return *stateActionsRange.first;

}

/**
 * set the q value
 * @param input
 */
void QTable::setQValue(std::pair<std::string, std::pair<std::string, double> > input)
{
  this->setQValue(input.first, input.second.first, input.second.second);
}

/**
 * how many states report change in their cusum
 * @return <positive,negative>
 */
std::pair<int, int> QTable::stateActionsReportingChangeNumber()
{
  std::pair<int, int> output(0, 0);
  std::multimap < std::string, std::pair < std::string, std::shared_ptr<Cusum> > >::iterator cusumRange = stateDiverges.begin(); //get first and last of the actions in that state
  while (cusumRange != stateDiverges.end())
    {//go through all actions in this state and replace cusum
      std::pair < std::string, std::pair < std::string, std::shared_ptr<Cusum> > >toUpdate = (*cusumRange);
      int lastForThisState = toUpdate.second.second->getLastOutput();
      if (lastForThisState == -1)
        {
          output.second++;
        }
      else if (lastForThisState == 1)
        {
          output.first++;
        }
      else
        {
          if (toUpdate.second.second->getWasOutOfRange() == true)
            {//if was out of range before but not last add to both
              output.second++;
              output.first++;
            }
        }
      cusumRange++;
    }
  return output;

}

/**
 * look at visit counts for all states and see how many are a significant percentage of that
 * @return
 */
int QTable::approximateNumberOfRegularVisitedStates()
{
  //get the total
  double total = 0;
  int significantPercentage = 10; //use this for now should lead to a max of 20 states to trigger change (needs to be somewhat high as exploration can skew)
  int numberOfStates = 0;
  std::multimap<std::string, std::pair<std::string, int> >::iterator visitIterator = visitCount.begin();
  while (visitIterator != visitCount.end())
    {
      total += (*visitIterator).second.second;
      visitIterator++;
    }
  total = total / 100 * significantPercentage;
  //now rescan and count
  visitIterator = visitCount.begin();
  while (visitIterator != visitCount.end())
    {
      if ((*visitIterator).second.second > total)
        {//found one
          numberOfStates++;
        }
      visitIterator++;
    }
  return numberOfStates;
}

/**
 * how many states report they are converged
 * @return <positive,negative>
 */
int QTable::numberOfStateActionsConverged()
{
  int output = 0;
  std::multimap < std::string, std::pair < std::string, bool > >::iterator convergedIterator = converged.begin(); //get first and last of the actions in that state
  while (convergedIterator != converged.end())
    {//go through all actions in this state and replace cusum
      std::pair < std::string, std::pair < std::string, bool > >toUpdate = (*convergedIterator);
      bool forThisState = toUpdate.second.second;
      if (forThisState == true)
        {
          output++;
        }

      convergedIterator++;
    }
  return output;

}

/**
 * how many states report change in their cusum
 * @return <positive,negative>
 */
std::pair<double, double> QTable::stateActionsReportingChangeAverageMagnatude()
{
  std::pair<double, double> output(0, 0);
  int low = 0;
  int high = 0;
  std::multimap < std::string, std::pair < std::string, std::shared_ptr<Cusum> > >::iterator cusumRange = stateDiverges.begin(); //get first and last of the actions in that state
  while (cusumRange != stateDiverges.end())
    {//go through all actions in this state and replace cusum
      std::pair < std::string, std::pair < std::string, std::shared_ptr<Cusum> > >toUpdate = (*cusumRange);
      int lastForThisState = toUpdate.second.second->getLastOutput();
      if (lastForThisState == -1)
        {
          output.second += abs(toUpdate.second.second->GetSlo());
          low++;
        }
      else if (lastForThisState == 1)
        {
          output.first += abs(toUpdate.second.second->GetShi());
          high++;
        }
      cusumRange++;
    }


  if (high != 0)
    {
      output.first = output.first / high;
    }
  if (low != 0)
    {
      output.second = output.second / low;
    }
  return output;

}

int QTable::numberOfStateActions()
{
  return qTable.size();
}

/**
 *find which state has the greatest differance between argument and this
 *@param the thing to compare to
 */
std::vector<std::pair<std::string, std::string> > QTable::getStateOfGreatestChang(int amountToReturn)
{ //std::cerr<<"state of greatest change"<<std::endl;
  if (amountToReturn > qTable.size())
    {
      amountToReturn = qTable.size();
    }
  std::vector< std::pair<std::pair<std::string, std::string>, double> > bestSoFar;
  std::multimap<std::string, std::pair<std::string, double> >::iterator qTableIterator = this->qTable.begin();
  while (qTableIterator != qTable.end())
    {//for all qs//use search as the two tables are not nessicarilly identical
      double diff = std::numeric_limits<double>::quiet_NaN(); //= abs(qTableIterator->second.second - qTablePrevious->getQValue(qTableIterator->first, qTableIterator->second.first));
      //get the q value in previous
      std::pair<std::multimap<std::string, std::pair<std::string, double> >::iterator, std::multimap<std::string, std::pair<std::string, double> >::iterator> stateActionsRange = qTablePrevious.equal_range(qTableIterator->first); //get first and last of the actions in that state
      std::multimap<std::string, std::pair<std::string, double> >::iterator it;
      for (it = stateActionsRange.first; it != stateActionsRange.second;)
        {//go through all actions in this state and see if there is a match
          //std::cerr<<(*it).second.first<<"  "<<qTableIterator->second.first<<"\n";
          if ((*it).second.first.compare(qTableIterator->second.first) == 0)
            {//if they match return the q
              diff = abs(qTableIterator->second.second - (*it).second.second);
              it = stateActionsRange.second;
              //std::cerr<<diff<<" match\n";
            }
          else
            {
              it++;
            }
        }
      if (diff != diff)
        {//is nan
          std::cerr << "couldn't match state action in greatest chang\n";
          exit(883);
        }
      //std::cerr<<diff<<std::endl;
      if (bestSoFar.size() < amountToReturn)
        {//if small just add
          std::pair<std::pair<std::string, std::string>, double> toAdd;
          toAdd.first.first = qTableIterator->first;
          toAdd.first.second = qTableIterator->second.first;
          toAdd.second = diff;
          bestSoFar.push_back(toAdd);
          bestSoFar = this->sort(bestSoFar);
        }
      else if (bestSoFar.size() > 0 && diff > bestSoFar.back().second)
        {//if better
          bestSoFar.pop_back(); //remove back one
          std::pair<std::pair<std::string, std::string>, double> toAdd;
          toAdd.first.first = qTableIterator->first;
          toAdd.first.second = qTableIterator->second.first;
          toAdd.second = diff;
          bestSoFar.push_back(toAdd);
          bestSoFar = this->sort(bestSoFar);
        }
      qTableIterator++;
    }
  //std::cerr<<bestSoFar<<"\n";
  std::vector<std::pair<std::string, std::string> > out;
  std::vector< std::pair<std::pair<std::string, std::string>, double> >::iterator bestIterator = bestSoFar.begin();
  while (bestIterator != bestSoFar.end())
    {
      out.push_back((*bestIterator).first);
      bestIterator++;
    }
  return out;

}

/**
 *true if no switch required
 */
std::vector< std::pair<std::pair<std::string, std::string>, double> > QTable::sort(std::vector< std::pair<std::pair<std::string, std::string>, double> > input)
{
  //std::cerr << "in swap in size= " << input.size() << "\n";
  if (input.size() < 2)
    {//if cant swap just return
      return input;
    }
  bool swapped = true;
  while (swapped)
    {
      swapped = false;
      for (int a = input.size() - 1; a > 0; a--)
        {//for last index to first
          if (input[a].second > input[a - 1].second)
            {//if we need to swap one
              std::pair<std::pair<std::string, std::string>, double > temp = input[a];
              input[a] = input[a - 1];
              input[a - 1] = temp;
              swapped = true;
            }
        }
    }
  return input;
}

/**
 *find which state has the greatest differance between argument and this
 *@param the thing to compare to
 */
std::vector<std::pair<std::string, std::string> > QTable::getStateOfMostVisit(int amountToReturn)
{
  if (amountToReturn > qTable.size())
    {
      amountToReturn = qTable.size();
    }
  std::vector< std::pair<std::pair<std::string, std::string>, double> > bestSoFar;
  std::multimap<std::string, std::pair<std::string, int> >::iterator qTableIterator = this->visitCount.begin();
  while (qTableIterator != visitCount.end())
    {//for all qs//use search as the two tables are not nessicarilly identical
      //std::cerr<<diff<<std::endl;
      if (bestSoFar.size() < amountToReturn)
        {//if small just add
          std::pair<std::pair<std::string, std::string>, double> toAdd;
          toAdd.first.first = qTableIterator->first;
          toAdd.first.second = qTableIterator->second.first;
          toAdd.second = qTableIterator->second.second;
          bestSoFar.push_back(toAdd);
          bestSoFar = this->sort(bestSoFar);
        }
      if (bestSoFar.size() > 0 && qTableIterator->second.second > bestSoFar.back().second)
        {//if better
          bestSoFar.pop_back(); //remove back one
          std::pair<std::pair<std::string, std::string>, double> toAdd;
          toAdd.first.first = qTableIterator->first;
          toAdd.first.second = qTableIterator->second.first;
          toAdd.second = qTableIterator->second.second;
          bestSoFar.push_back(toAdd);
          bestSoFar = this->sort(bestSoFar);
        }
      qTableIterator++;
    }
  //std::cerr<<bestSoFar<<"\n";
  std::vector<std::pair<std::string, std::string> > out;
  std::vector< std::pair<std::pair<std::string, std::string>, double> >::iterator bestIterator = bestSoFar.begin();
  while (bestIterator != bestSoFar.end())
    {
      out.push_back((*bestIterator).first);
      bestIterator++;
    }
  return out;
}

/**
 *find which state has the greatest differance between argument and this
 *@param the thing to compare to
 */
std::vector<std::pair<std::string, std::string> > QTable::getStateOfMostVisitConverged(int amountToReturn)
{
  if (amountToReturn > qTable.size())
    {
      amountToReturn = qTable.size();
    }
  std::vector< std::pair<std::pair<std::string, std::string>, double> > bestSoFar;
  std::multimap<std::string, std::pair<std::string, int> >::iterator qTableIterator = this->visitCount.begin();
  while (qTableIterator != visitCount.end())
    {//for all qs//use search as the two tables are not nessicarilly identical
      //std::cerr<<diff<<std::endl;
      if (bestSoFar.size() < amountToReturn)
        {//if small just add
          std::pair < std::multimap < std::string, std::pair < std::string, bool> >::iterator, std::multimap < std::string, std::pair < std::string, bool> >::iterator> range = converged.equal_range(qTableIterator->first);
          std::multimap < std::string, std::pair < std::string, bool> >::iterator it = range.first;
          while (it != range.second)
            {//for all in a  state
              if ((*it).second.first == qTableIterator->second.first)
                {//if actions match
                  break;
                }
              else
                {
                  it++;
                }
            }
          if ((*it).second.second == true)//for converged only turn on

            {//is converged
              std::pair<std::pair<std::string, std::string>, double> toAdd;
              toAdd.first.first = qTableIterator->first;
              toAdd.first.second = qTableIterator->second.first;
              toAdd.second = qTableIterator->second.second;
              bestSoFar.push_back(toAdd);
              bestSoFar = this->sort(bestSoFar);
            }
        }
      if (bestSoFar.size() < amountToReturn)
        {//add anyway
          std::pair<std::pair<std::string, std::string>, double> toAdd;
          toAdd.first.first = qTableIterator->first;
          toAdd.first.second = qTableIterator->second.first;
          toAdd.second = qTableIterator->second.second;
          bestSoFar.push_back(toAdd);
          bestSoFar = this->sort(bestSoFar);
        }
      else if (bestSoFar.size() > 0 && qTableIterator->second.second > bestSoFar.back().second)
        {//if better
          std::pair < std::multimap < std::string, std::pair < std::string, bool> >::iterator, std::multimap < std::string, std::pair < std::string, bool> >::iterator> range = converged.equal_range(qTableIterator->first);
          std::multimap < std::string, std::pair < std::string, bool> >::iterator it = range.first;
          while (it != range.second)
            {//for all in a  state
              if ((*it).second.first == qTableIterator->second.first)
                {//if actions match
                  break;
                }
              else
                {
                  it++;
                }
            }
          if ((*it).second.second == true)
            {//is converged
              bestSoFar.pop_back(); //remove back one
              std::pair<std::pair<std::string, std::string>, double> toAdd;
              toAdd.first.first = qTableIterator->first;
              toAdd.first.second = qTableIterator->second.first;
              toAdd.second = qTableIterator->second.second;
              bestSoFar.push_back(toAdd);
              bestSoFar = this->sort(bestSoFar);
            }
        }
      qTableIterator++;
    }
  std::vector<std::pair<std::string, std::string> > out;
  std::vector< std::pair<std::pair<std::string, std::string>, double> >::iterator bestIterator = bestSoFar.begin();
  while (bestIterator != bestSoFar.end())
    {
      out.push_back((*bestIterator).first);
      bestIterator++;
    }
  return out;
}

/**
 *find which state has the greatest differance between argument and this
 *@param the thing to compare to
 */
std::vector<std::pair<std::string, std::string> > QTable::getStatesOfTargetedBatchBased(int amountToReturn)
{
  std::vector<std::pair<std::string, std::string> > out;
  if (this->mostRecentStatesTraceTransferable)
    {
      //std::cout << "getStatesOfTargetedBatchBased something was tranferable\n";
      if (amountToReturn > qTable.size())
        {
          amountToReturn = qTable.size();
        }
      //now check the sequence to find a good n and pack output vector
      int n = 5; //hows that for a good n?
      if (mostRecentStatesTrace.size() < n)
        {
          n = mostRecentStatesTrace.size();
        }
      std::vector<std::pair<std::string, std::string> >::iterator mostRecentStatesTraceIterator = mostRecentStatesTrace.begin();
      for (int a = 0; a < n; a++)
        {//grab some ellements
          out.push_back(std::pair<std::string, std::string>(mostRecentStatesTraceIterator->first, mostRecentStatesTraceIterator->second));
          mostRecentStatesTraceIterator++;
        }
      this->mostRecentStatesTraceTransferable = false;
      this->mostRecentStatesTrace.clear();
      this->mostRecentStatesTrace.shrink_to_fit();
    }
  else
    {//not marked as having a happy ending

    }
  //std::cout << "end getStatesOfTargetedBatchBased\n";

  return out;
}

/**
 *find which state has the greatest differance between argument and this
 *@param the thing to compare to
 */
std::vector<std::pair<std::string, std::string> > QTable::getStateOfManyVisit(int amountToReturn)
{
  if (amountToReturn > qTable.size())
    {
      amountToReturn = qTable.size();
    }
  int visitMin = 10;
  std::vector< std::pair<std::pair<std::string, std::string>, double> > bestSoFar;
  std::multimap<std::string, std::pair<std::string, int> >::iterator qTableIterator = this->visitCount.begin();
  while (qTableIterator != visitCount.end())
    {//for all qs//use search as the two tables are not nessicarilly identical
      //std::cerr<<diff<<std::endl;
      if (bestSoFar.size() < amountToReturn && qTableIterator->second.second > visitMin)
        {//if small just add
          std::pair<std::pair<std::string, std::string>, double> toAdd;
          toAdd.first.first = qTableIterator->first;
          toAdd.first.second = qTableIterator->second.first;
          toAdd.second = qTableIterator->second.second;
          bestSoFar.push_back(toAdd);
          bestSoFar = this->sort(bestSoFar);
        }
      if (bestSoFar.size() > 0 && qTableIterator->second.second > bestSoFar.back().second && qTableIterator->second.second > visitMin)
        {//if better
          bestSoFar.pop_back(); //remove back one
          std::pair<std::pair<std::string, std::string>, double> toAdd;
          toAdd.first.first = qTableIterator->first;
          toAdd.first.second = qTableIterator->second.first;
          toAdd.second = qTableIterator->second.second;
          bestSoFar.push_back(toAdd);
          bestSoFar = this->sort(bestSoFar);
        }
      qTableIterator++;
    }
  //std::cerr<<bestSoFar<<"\n";
  std::vector<std::pair<std::string, std::string> > out;
  std::vector< std::pair<std::pair<std::string, std::string>, double> >::iterator bestIterator = bestSoFar.begin();
  while (bestIterator != bestSoFar.end())
    {
      out.push_back((*bestIterator).first);
      bestIterator++;
    }
  return out;
}

/**
 *find which state has the greatest differance between argument and this
 *@param the thing to compare to
 */
std::vector<std::pair<std::string, std::string> > QTable::getBestStates(int amountToReturn)
{//https://www.youtube.com/watch?v=vXMWNhCmLUg
  if (amountToReturn > qTable.size())
    {
      amountToReturn = qTable.size();
    }
  std::vector< std::pair<std::pair<std::string, std::string>, double> > bestSoFar;
  std::multimap<std::string, std::pair<std::string, double> >::iterator qTableIterator = this->qTable.begin();
  while (qTableIterator != qTable.end())
    {//for all qs//use search as the two tables are not nessicarilly identical
      if (bestSoFar.size() < amountToReturn)
        {//if small just add
          std::pair<std::pair<std::string, std::string>, double> toAdd;
          toAdd.first.first = qTableIterator->first;
          toAdd.first.second = qTableIterator->second.first;
          toAdd.second = qTableIterator->second.second;
          bestSoFar.push_back(toAdd);
          bestSoFar = this->sort(bestSoFar);
          //std::cerr << "fill adding " << qTableIterator->first << " and " << qTableIterator->second.second << "\n";
        }
      if (bestSoFar.size() > 0 && qTableIterator->second.second > bestSoFar.back().second)
        {//if better
          bestSoFar.pop_back(); //remove back one
          std::pair<std::pair<std::string, std::string>, double> toAdd;
          toAdd.first.first = qTableIterator->first;
          toAdd.first.second = qTableIterator->second.first;
          toAdd.second = qTableIterator->second.second;
          bestSoFar.push_back(toAdd);
          bestSoFar = this->sort(bestSoFar);
          //std::cerr << "better adding " << qTableIterator->first << ", " << qTableIterator->second.first << " and " << qTableIterator->second.second << "\n";
        }
      qTableIterator++;
    }
  //std::cerr<<bestSoFar.size()<<"=best size\n";
  std::vector<std::pair<std::string, std::string> > out;
  std::vector< std::pair<std::pair<std::string, std::string>, double> >::iterator bestIterator = bestSoFar.begin();
  while (bestIterator != bestSoFar.end())
    {
      out.push_back((*bestIterator).first);
      bestIterator++;
    }
  return out;
}
//END

void QTable::addFromQTable(std::multimap<std::string, std::pair<std::string, double> > in)
{//std::cerr<<"about to add form Qtable\n"<<std::endl;
  std::multimap<std::string, std::pair<std::string, double> >::iterator inputIterator = in.begin();
  while (inputIterator != in.end())
    {
      this->qTable.insert(*inputIterator);
      inputIterator++;
    }
  //std::cerr<<"added form Qtable\n"<<std::endl;
}

int QTable::getVisitCount(std::string stateName, std::string actionName)
{
  std::pair<std::multimap<std::string, std::pair<std::string, int> >::iterator, std::multimap<std::string, std::pair<std::string, int> >::iterator> stateActionsRange = visitCount.equal_range(stateName); //get first and last of the actions in that state
  std::multimap<std::string, std::pair<std::string, int> >::iterator it;
  for (it = stateActionsRange.first; it != stateActionsRange.second; it++)
    {//go through all actions in this state and see if there is a match
      if ((*it).second.first.compare(actionName) == 0)
        {//if they match return the q
          return (*it).second.second;
        }
    }
  this->writeStateActionToFile("error", "error");
  std::cerr << "in Qtable->getVisitcount failed to find state= " << stateName << " and action= " << actionName << "\n";
  exit(10);
  return -99999;

}

/**
 * return some random states
 * @return
 */
std::vector<std::pair<std::string, std::string> > QTable::getStateOfRandom(int amountToReturn)
{
  int arraySize = amountToReturn;
  std::vector<std::pair<std::string, std::string> > out;
  for (int a = 0; a < arraySize; a++)
    {
      std::multimap<std::string, std::pair<std::string, double> >::iterator tableIterator = qTable.begin();
      int number = (rand() % qTable.size());
      for (int b = 0; b < number; b++)
        {
          tableIterator++;
        }
      std::pair<std::string, std::string> toAdd;
      toAdd.first = tableIterator->first;
      toAdd.second = tableIterator->second.first;
      out.push_back(toAdd);

    }

  return out;
}

void QTable::setMasterCusum(std::shared_ptr<Cusum> masterCusum)
{
  this->masterCusum = masterCusum;
}

std::shared_ptr<Cusum> QTable::getMasterCusum() const
{
  return masterCusum;
}

void QTable::setStateDiverges(std::multimap<std::string, std::pair<std::string, std::shared_ptr<Cusum>> > stateDiverges)
{
  this->stateDiverges = stateDiverges;
}

std::multimap<std::string, std::pair<std::string, std::shared_ptr<Cusum>> > QTable::getStateDiverges() const
{
  return stateDiverges;
}

void QTable::createAndSetShadowCopy()
{
  if (usingShadowTable)
    {//if we already have one dont repopulate

    }
  else
    {//make a shadow copy and create stats for it
      shadowQTable.insert(qTable.begin(), qTable.end()); //add all
      this->shadowAverageReward = this->rollingAverageReward;
      this->shadowVarienceReward = this->rollingVarienceReward;
      this->shadowNumberOfRewardSamples = this->numberOfRewardSamples;
      this->rollingAverageReward = 0;
      this->rollingVarienceReward = 0;
      this->numberOfRewardSamples = 0;
      //std::cout << "Made shadow\n";
      // std::cout << "rol ave= " << rollingAverageReward << " roll var= " << rollingVarienceReward << " roll samples= " << numberOfRewardSamples << " shadl ave= " << shadowAverageReward << " shad var= " << shadowVarienceReward << " shadow samples= " << shadowNumberOfRewardSamples << "\n";

    }
  this->usingShadowTable = true;
}

/**
 * get rid of shadow copy
 * @param acceptShadowAsCorrect true is accept it false reject it
 */
void QTable::destroyShadowCopy(bool acceptShadowAsCorrect)
{
  //std::cout << "QTable::destroyShadowCopy " << acceptShadowAsCorrect << " \n";
  if (usingShadowTable)
    {//prevent acidental clearing of real qTable
      if (acceptShadowAsCorrect == false)
        {//go back to origional table
          std::cout << "rejecting transfer\n";
          qTable.clear();
          qTable.insert(shadowQTable.begin(), shadowQTable.end()); //add all
          shadowQTable.clear();
          this->rollingAverageReward = this->shadowAverageReward;
          this->rollingVarienceReward = this->shadowVarienceReward;
          this->numberOfRewardSamples = this->shadowNumberOfRewardSamples;

        }
      else
        {//who needs the old one
          std::cout << "accepting transfer\n";
          shadowQTable.clear();
          this->shadowAverageReward = 0;
          this->shadowVarienceReward = 0;
          this->shadowNumberOfRewardSamples = 0;
        }
      this->usingShadowTable = false;
    }

}

/**
 * test (welch's) the reward stats and see if we are better off than before
 * @return
 */
bool QTable::isCurrentCopyBetterThanShadow()
{ //1= current copy 2= shadow
  //s is stddev we have variences stddev^2
  //s(x1-x2)=sqrt((s1^2/n1)+(s2^2/n2))
  double sDiff = std::sqrt((rollingVarienceReward / numberOfRewardSamples)+(shadowVarienceReward / shadowNumberOfRewardSamples));
  //t=x1-x2/s(x1-x2)
  double t = (rollingAverageReward - shadowAverageReward) / sDiff; //basically the normailised differnece
  //std::cout << "rol ave= " << rollingAverageReward << " roll var= " << rollingVarienceReward << " roll samples= " << numberOfRewardSamples << " shadl ave= " << shadowAverageReward << " shad var= " << shadowVarienceReward << " shadow samples= " << shadowNumberOfRewardSamples << " sdiff= " << sDiff << " t= " << t << "\n";

  if (std::abs(t) < 0.5)
    {//if too close to call
      //std::cout << "QTable::isCurrentCopyBetterThanShadow too close to call\n";
      return true; //as less processing
    }
  else if (t > 0)
    {//current is better
      // std::cout << "QTable::isCurrentCopyBetterThanShadow new better\n";
      return true;
    }
  else
    {//go back to where you once belonged
      //std::cout << "QTable::isCurrentCopyBetterThanShadow old better\n";
      return false;
    }
}