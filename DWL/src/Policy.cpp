/* 
 * File:   Policy.cpp
 * Author: Adam
 * 
 * Created on July 11, 2012, 9:24 AM
 */

#include "Policy.h"

Policy::Policy(std::string name)
{
    this->policyName = name;
    this->config.environment.fastChange = false;
    this->config.environment.iChanged = false;
    this->config.environment.iLearned = false;
    this->config.environment.neighbourChange = false;
    this->config.environment.sharpChnageHappened = false;
    this->config.environment.slowChange = false;
    this->config.environment.processedILearned = false;
    this->config.environment.processedSharpChnageHappened = false;
    this->config.selection = SelectionType::DEFAULT_SELECTION;
    this->config.merge = MergeType::DEFAULT_MERGE;
    this->config.amountToTransfer = 4;
    this->config.mergeParam = 5;
    learnedThreshold = 50; //as a % of state space
    environmentChangeThreshold = 30; //as a % of state space
    sharpChangeStart = 180; //cusum for if a sharp change 
    fastChangeStart = 40; //cusum for if a fast  change 
}

int Policy::getLearnedThreshold()
{
    return learnedThreshold;
}

void Policy::setLearnedThreshold(int learnedThreshold)
{
    this->learnedThreshold = learnedThreshold;
}

void Policy::setPolicyName(std::string policyName)
{
    this->policyName = policyName;
}

std::string Policy::getPolicyName() const
{
    return policyName;
}

void Policy::setConfig(TransferConfiguration config)
{
    this->config = config;
}

TransferConfiguration Policy::getConfig() const
{
    return config;
}

Policy::~Policy()
{

}

/**
 set slow to passed and fast to off if isSlow**/
void Policy::setSlowChange(bool input)
{
    this->config.environment.slowChange = input;
    if (input == true)
    {//if is slow cant be fast
        this->config.environment.fastChange = false;
    }
}

/**
 set fast to passed and slow to off if isSlow**/
void Policy::setFastChange(bool input)
{
    this->config.environment.fastChange = input;
    if (input == true)
    {//if is slow cant be fast
        this->config.environment.slowChange = false;
    }
}

void Policy::setSharpChangeHappened(bool input)
{
    this->config.environment.sharpChnageHappened = input;
}

void Policy::setNeighbourChanged(bool input)
{
    this->config.environment.neighbourChange = input;
}

void Policy::setILearned(bool input)
{
    this->config.environment.iLearned;
}

void Policy::setIChanged(bool input)
{
    this->config.environment.iChanged;
}

void Policy::setSelectionType(SelectionType method, int amountToShare)
{
    this->config.selection = method;
    this->config.amountToTransfer = amountToShare;
}

void Policy::setMergeType(MergeType method, int adptiveTime)
{
    this->config.merge = method;
    this->config.mergeParam = adptiveTime;
}

void Policy::setFastChangeStart(double fastChangeStart)
{
    this->fastChangeStart = fastChangeStart;
}

double Policy::getFastChangeStart() const
{
    return fastChangeStart;
}

void Policy::setSharpChangeStart(double sharpChangeStart)
{
    this->sharpChangeStart = sharpChangeStart;
}

double Policy::getSharpChangeStart() const
{
    return sharpChangeStart;
}

void Policy::setEnvironmentChangeThreshold(int environmentChangeThreshold)
{
    this->environmentChangeThreshold = environmentChangeThreshold;
}

int Policy::getEnvironmentChangeThreshold() const
{
    return environmentChangeThreshold;
}